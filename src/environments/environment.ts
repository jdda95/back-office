// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  notificationLink: 'https://empresarios24-7.com/set/notifications.php',
  firebase: {
    apiKey: 'AIzaSyCxPs6lo5yrwT5sq1yGiE6stjCliqZH6qI',
    authDomain: 'network-247.firebaseapp.com',
    databaseURL: 'https://network-247.firebaseio.com',
    projectId: 'network-247',
    storageBucket: 'network-247.appspot.com',
    messagingSenderId: '400962712959',
    appId: '1:400962712959:web:64df2bbcb992af5c68964a',
    measurementId: 'G-DXNQGYVGJL',
  },
};

// ************************************************************************************
// ---------------------- cambiar cuando se migre a producción -------------------------
// ---------------------- usar el comando firebase use backoffice-3c204 ----------------
// ---------------------- el emulador apuntara al nuevo proyecto para correr -----------
// ---------------------- y usará el firebase emulator correctamente -------------------
// ************************************************************************************
// export const environment = {
//   production: false,
//   notificationLink: 'https://empresarios24-7.com/set/notifications.php',
//   firebase: {
//     host: 'localhost:8081',
//     apiKey: 'AIzaSyCxPs6lo5yrwT5sq1yGiE6stjCliqZH6qI',
//     authDomain: 'backoffice-3c204.firebaseapp.com',
//     databaseURL: 'http://localhost:4000?ns=network-247',
//     projectId: 'backoffice-3c204',
//     storageBucket: 'backoffice-3c204.appspot.com',
//   },
// };

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
