import { Component, OnDestroy, OnInit } from '@angular/core';
import { ThemeConstantService } from '../shared/services/theme-constant.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
//Services
import { AuthenticationService } from '../shared/services/authentication.service';
import { AssociateCRUDService } from '../shared/services/associate-crud.service';
import { MembershipsService } from '../shared/services/memberships.service';
import { RankService } from '../shared/services/rank.service';
import { WalletService } from '../shared/services/wallet.service';
//Models
import { Associate } from '../shared/models/associate.model';
import { Wallet } from '../shared/models/wallet.model';
import { Membership } from '../shared/models/membership.model';
import { TreeNodeInterface } from '../shared/interfaces/tree-node.type';
import { Rank } from '../shared/models/rank.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: [
    `
      .d-flex.align-items-center h6 {
        white-space: nowrap;
        width: 100px;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    `,
  ],
})
export class DashboardComponent implements OnInit, OnDestroy {
  themeColors = this.colorConfig.get().colors;
  blue = this.themeColors.blue;
  volcano = this.themeColors.volcano;
  blueLight = this.themeColors.blueLight;
  cyan = this.themeColors.cyan;
  cyanLight = this.themeColors.cyanLight;
  gold = this.themeColors.gold;
  purple = this.themeColors.purple;
  purpleLight = this.themeColors.purpleLight;
  red = this.themeColors.red;
  orange = this.themeColors.orange;
  // set associate data
  currentUser: string;
  associate: Associate = new Associate();
  associate$: Observable<Associate>;
  wallets: Wallet[] = [];
  wallets$: Observable<Wallet[]>;
  memberships: Membership[] = [];
  // set next rank and goal
  ranksList: Rank[] = [];
  nextRank = { currentchi: 0, name: '', goal: 0, pos: 0 };
  progressBar: number = 0;
  //
  mySubscription: any;

  constructor(
    private colorConfig: ThemeConstantService,
    private authServ: AuthenticationService,
    private router: Router,
    private assoCRUDServ: AssociateCRUDService,
    private membServ: MembershipsService,
    private rankServ: RankService,
    private activeRoute: ActivatedRoute,
    public walletServ: WalletService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  listOfMapData: TreeNodeInterface[] = [];

  mapOfExpandedData: { [key: string]: TreeNodeInterface[] } = {};

  collapse(
    array: TreeNodeInterface[],
    data: TreeNodeInterface,
    $event: boolean
  ): void {
    if (!$event) {
      if (data.children) {
        data.children.forEach((d) => {
          // tslint:disable-next-line: no-non-null-assertion
          const target = array.find((a) => a.key === d.key)!;
          target.expand = false;
          this.collapse(array, target, false);
        });
      } else {
        return;
      }
    }
  }

  convertTreeToList(root: TreeNodeInterface): TreeNodeInterface[] {
    const stack: TreeNodeInterface[] = [];
    const array: TreeNodeInterface[] = [];
    const hashMap = {};
    stack.push({ ...root, level: 0, expand: false });
    while (stack.length !== 0) {
      // tslint:disable-next-line: no-non-null-assertion
      const node = stack.pop()!;
      this.visitNode(node, hashMap, array);
      if (node.children) {
        for (let i = node.children.length - 1; i >= 0; i--) {
          // tslint:disable-next-line: no-non-null-assertion
          stack.push({
            ...node.children[i],
            level: node.level! + 1,
            expand: false,
            parent: node,
          });
        }
      }
    }
    return array;
  }

  visitNode(
    node: TreeNodeInterface,
    hashMap: { [key: string]: boolean },
    array: TreeNodeInterface[]
  ): void {
    if (!hashMap[node.key]) {
      hashMap[node.key] = true;
      array.push(node);
    }
  }

  ngOnInit(): void {
    this.listOfMapData = this.listOfMapData.filter((d) => d.assoID === '');

    this.associate$ = this.assoCRUDServ.getAssociate$();
    this.associate$.subscribe((newData) => {
      this.associate = newData;
      this.assoIsSet(this.associate);
      this.membServ
        .getMemberships()
        .snapshotChanges()
        .subscribe((membData) => {
          membData.forEach((memb) => {
            if (this.associate.membershipID === memb.payload.doc.id) {
              this.associate.membershipName = memb.payload.doc.data()['name'];
            }
          });
        });
    });

    this.wallets$ = this.assoCRUDServ.getAssociateWallets$();
    this.assoCRUDServ.getAssociateWallets$().subscribe(
      (data) => {
        let totalMoney = 0;
        data.forEach((wallet) => {
          if (wallet.type !== 1) {
            totalMoney = totalMoney + wallet.current_balance;
          }
          this.walletServ.totalMoney = totalMoney;
        });
      },
      (err) => console.error(err)
    );
    this.wallets$.subscribe((newData) => {
      this.wallets = newData;
      this.retrieveWallets(this.associate.fireId);
    });
    // evaluate if session exist
    this.authServ.hasUserB().then((user) => {
      if (user !== null) {
        this.currentUser = user.uid;
        // retrieve wallets user
        this.retrieveWallets(this.currentUser);
        // retrieve memeberships
        this.retrieveMemberships();
      } else {
        this.router.navigate(['auth/login']);
      }
    });
  }

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
    this.listOfMapData = this.listOfMapData.filter((d) => d.key === 99999);
  }

  assoIsSet(newAssoData: Associate) {
    this.associate = newAssoData;

    if (newAssoData.rankID !== undefined) {
      this.retrieveRanks(newAssoData.rankID);
    }
    if (newAssoData.referrals_list !== undefined) {
      this.retrieveReferralList(newAssoData.referrals_list);
    }
  }
  // retrieveAssociate() {
  //   if (this.assoCRUDServ.associate == undefined) {

  //     this.assoCRUDServ.getCurrentAssociateData(this.currentUser);
  //   } else {

  //     // chek how save list localy
  //     // this.associate = this.assoCRUDServ.associate;
  //     // this.assoIsSet(this.associate);
  //   }
  // }
  retrieveMemberships() {
    let membID;
    // retrieve memberships
    this.membServ
      .getMemberships()
      .snapshotChanges()
      .subscribe((membData) => {
        membData.forEach((memb) => {
          let cuMemb: Membership = new Membership();
          Object.keys(memb.payload.doc.data()).forEach((key) => {
            cuMemb[key] = memb.payload.doc.data()[key];
          });
          this.memberships.push(cuMemb);
        });
      });
  }
  retrieveWallets(userID: string) {
    if (this.assoCRUDServ.wallets.length <= 0) {
      this.assoCRUDServ.getAssociateWallets(userID);
    } else {
      this.wallets = this.assoCRUDServ.wallets;
    }
  }
  retrieveReferralList(listID: string, fatherCounter?: string) {
    this.assoCRUDServ
      .getAssociateRefList(listID)
      .get()
      .subscribe((listAsso) => {
        let counter = 1;
        listAsso.forEach((asso) => {
          let cuReferral = {} as TreeNodeInterface;
          // Define current key for tree
          if (fatherCounter !== undefined) {
            let transform = fatherCounter + counter.toString();

            cuReferral.key = parseInt(transform);
          } else {
            cuReferral.key = counter;
          }
          // set node from current data
          Object.keys(asso.data()).forEach((key) => {
            cuReferral[key] = asso.data()[key];
          });

          // search childs data
          if (asso.data()['haschilds'] === 1) {
            cuReferral['children'] = [] as TreeNodeInterface[];
            let index: string;
            if (fatherCounter !== undefined) {
              index = fatherCounter + counter.toString();
            } else {
              index = counter.toString();
            }

            this.retrieveReferralList(asso.data()['referrals_list'], index);
          }
          // insert child
          if (fatherCounter !== undefined) {
            let temparray = this.findById(
              this.listOfMapData,
              parseInt(fatherCounter)
            );
            temparray['children'] = [...temparray['children'], cuReferral];
          } else {
            // if active user add to counter
            let rankAssoList = this.ranksList.find(
              (nxtRank) => cuReferral.rank_ID === nxtRank.fireID
            );
            let myRank = this.ranksList.find(
              (nxtRank) => nxtRank.fireID === this.associate.rankID
            );
            if (cuReferral.status === true && rankAssoList.pos >= myRank.pos) {
              this.nextRank.currentchi++;
              this.progressBar = Math.floor(
                (this.nextRank.currentchi * 100) / this.nextRank.goal
              );
            }
            this.listOfMapData = [...this.listOfMapData, cuReferral];
            // evaluate if already has object duplicate and eliminate
            const seen = new Set();
            this.listOfMapData = this.listOfMapData.filter((el) => {
              const duplicate = seen.has(el.assoID);
              seen.add(el.assoID);
              return !duplicate;
            });
          }
          counter++;
        });
        this.listOfMapData.forEach((item) => {
          this.mapOfExpandedData[item.key] = this.convertTreeToList(item);
        });
      });
  }
  retrieveRanks(assoRank: string) {
    let counter = 0;
    this.rankServ
      .getRanks()
      .snapshotChanges()
      .subscribe((data) => {
        data.forEach((rank) => {
          let cuRank: Rank = new Rank();

          cuRank.fireID = rank.payload.doc.id;
          Object.keys(rank.payload.doc.data()).forEach((key) => {
            cuRank[key] = rank.payload.doc.data()[key];
          });
          this.ranksList.push(cuRank);
          if (counter == data.length - 1) {
            let myrank = this.ranksList.find(
              (rank) => rank.fireID === assoRank
            );
            let nextRank = this.ranksList.find(
              (nxtRank) => myrank.next_rk === nxtRank.fireID
            );
            this.nextRank.name = nextRank.name;
            this.nextRank.goal = nextRank.min_referrals;
          }
          counter++;
        });
      });
  }
  // search recursively treeData
  findById(data, id) {
    function iter(a) {
      if (a.key === id) {
        result = a;
        return true;
      }
      return Array.isArray(a.children) && a.children.some(iter);
    }
    var result;
    data.some(iter);
    return result;
  }
}
