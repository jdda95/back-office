import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { VideoCardComponent } from './video-card/video-card.component';


@NgModule({
  declarations: [
    VideoCardComponent
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule
  ],
  exports: [
    VideoCardComponent
  ]
})
export class ComponentsModule { }
