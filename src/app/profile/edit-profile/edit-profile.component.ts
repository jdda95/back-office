import { Component, OnInit } from '@angular/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import {
  EmailValidator,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';
import { Observable, Observer } from 'rxjs';
// Services
import { AssociateCRUDService } from '../../shared/services/associate-crud.service';
import { AngularFireStorage } from '@angular/fire/storage';
// Models
import { Associate } from '../../shared/models/associate.model';
import { finalize } from 'rxjs/operators';
import { PhoneNumber } from 'libphonenumber-js/min';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styles: [],
})
export class EditProfileComponent implements OnInit {
  changePWForm: FormGroup;
  associate: Associate;
  avatarUrl = '';
  downloadURL$: Observable<string>;
  loading = '';

  constructor(
    private fb: FormBuilder,
    private message: NzMessageService,
    private assoCRUDServ: AssociateCRUDService,
    private router: Router,
    private fireBase: AngularFireStorage
  ) { }

  ngOnInit(): void {
    this.evaluateData();
    this.changePWForm = this.fb.group({
      address: [null, [Validators.required]],
      phone: [null, [Validators.required]],
    });
  }

  evaluateData() {
    if (this.assoCRUDServ.associate !== undefined) {
      this.associate = this.assoCRUDServ.associate;
      this.avatarUrl = this.associate.profilePic;
    } else {
      this.router.navigate(['/dashboard']);
    }
  }

  beforeUpload = (file: File) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng =
        file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        this.message.error('Solo puedes subir archivos JPG o PNG!');
        observer.complete();
        return;
      }
      const isLt1M = file.size / 1024 / 1024 < 2;
      if (!isLt1M) {
        this.message.error('La imagen debe pesar menos de 1MB!');
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt1M);
      observer.complete();
    });
  };

  handleChange(info: { file: NzUploadFile }): void {
    const file = info.file.originFileObj;
    const filePath = `AvatarImages/${this.associate.fireId}`;
    const fileRef = this.fireBase.ref(filePath);
    const task = this.fireBase.upload(filePath, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL$ = fileRef.getDownloadURL();
          this.downloadURL$.subscribe((url) => {
            if (url) {
              this.avatarUrl = url;
              this.associate.profilePic = url;
              this.assoCRUDServ.updateAssociateData(this.associate.fireId, {
                profilePic: url,
              });
              this.assoCRUDServ.updateSimpleAssociateData(this.associate.in_ref_list, {
                profilePic: url,
              }).then(res => {
                console.log('actualización simple satisfactoria');
              }).catch(err => {
                console.log(`error actualizando los datos ${err}`);
              });
              this.loading = '';
            }
          });
        })
      )
      .subscribe((url) => {
        if (url) {
          this.loading = 'Cargando...';
        }
      });
  }

  submitForm(): void {
    let newData = {};
    Object.keys(this.changePWForm.controls).forEach((key) => {
      if (
        this.changePWForm.controls[key].value !== null &&
        this.changePWForm.controls[key].value !== ''
      ) {
        newData[key] = this.changePWForm.controls[key].value;
      }
    });

    this.assoCRUDServ
      .updateAssociateData(this.associate.fireId, newData)
      .then((res) => {
        Object.keys(this.changePWForm.controls).forEach((key) => {
          if (
            this.changePWForm.controls[key].value !== null &&
            this.changePWForm.controls[key].value !== ''
          ) {
            this.associate[key] = this.changePWForm.controls[key].value;
          }
        });
      })
      .catch((err) => {
        console.error('from update email or address', err);
      });
  }
}
