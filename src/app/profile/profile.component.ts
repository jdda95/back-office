import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { UploadFile } from 'ng-zorro-antd/upload';
// Services
import { AssociateCRUDService } from '../shared/services/associate-crud.service';
// Models
import { Associate } from '../shared/models/associate.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: [],
})
export class ProfileComponent implements OnInit {
  associate: Associate;

  constructor(
    private assoCRUDServ: AssociateCRUDService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.evaluateData();
  }

  evaluateData() {
    if (this.assoCRUDServ.associate !== undefined) {
      this.associate = this.assoCRUDServ.associate;
    } else {
      this.router.navigate(['auth/login']);
    }
  }
}
