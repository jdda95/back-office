export class Rank {
  fireID?: string;
  min_referrals: number;
  name: string;
  next_rk: string;
  percent_com: number;
  pos: number;
}
