import { Desearizable } from '../interfaces/desearizable';

export class Associate implements Desearizable {
  parentID?: string;
  fireId: string;
  fullName: string;
  dni?: string;
  nickName: string;
  profilePic: string;
  coverPic: string;
  rankID: string;
  rankName: string;
  phone: string;
  email?: string;
  address: string;
  countryID: string;
  cityID: string;
  alieID: string;
  walletLinks: string;
  wallet247: string;
  walletMembership: string;
  walletResidual: string;
  walletDevelopment: string;
  walletHeadquaters?: string;
  referrals_list: string;
  membershipID: string;
  membershipName?: string;
  status: boolean;
  date?: Date;
  password: any;
  in_ref_list?: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
