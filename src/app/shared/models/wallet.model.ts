export class Wallet {
    name: string;
    type: number; 
    current_balance: number;
    max_balance:number;
    owner:string;
    movementsList: Array<any>;
    date:Date;
    fireID?:string;
}
