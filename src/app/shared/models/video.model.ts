export class Video {
  fireID?: string;
  name: string;
  link: string;
  image: string;
  position: string;
  description: string;
}
