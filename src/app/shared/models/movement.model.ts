export class Movement {
    fireID?: string;
    date: Date;
    amount: number;
    status: number;
    tokenID: string;
    user1: string;
    user1email: string;
    user2email: string;
    user2?: string;
    walletID1: string;
    walletID2?: string;
    type: number;
}
