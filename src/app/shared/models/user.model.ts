export interface Users {
    name: string;
    phone: number;
    position: string;
    key: number;
    avatar: string;
    country: string;
    username: string;
    isActive: boolean;
}
