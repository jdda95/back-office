export class AdminCrud {
  email: string;
  percent?: number;
  position?: string;
  profilePic?: string;
  rankName: string;
}
