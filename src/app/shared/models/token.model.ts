export class Token {
    fireID?: string;
    date: Date;
    userID: string;
    type: number;
    status: boolean;
}
