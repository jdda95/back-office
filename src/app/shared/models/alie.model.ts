export class Alie {
    firebaseID?: string;
    name: string;
    rate: number;
    bill: number;
    countryID: string;
    countryName?: string;
    status: boolean;
}
