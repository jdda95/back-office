import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { AssociateCRUDService } from '../services/associate-crud.service';

@Injectable({
  providedIn: 'root',
})
export class ResolveUser implements Resolve<Observable<any>> {
  constructor(private assoCRUDServ: AssociateCRUDService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return this.assoCRUDServ.getAssociate$().subscribe((data) => {return data;});
  }
}
