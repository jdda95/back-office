import { Pipe, PipeTransform } from '@angular/core';
import { City } from "../models/city.model";
import { Country } from "../models/country.model";

@Pipe({
  name: 'filtercityselectors'
})
export class FilterselectorsPipe implements PipeTransform {

  transform(items: City[], filter:Country ): any {
    if(!items || !filter){
      return items;
    }
    return items.filter(item => item.fatherID.indexOf(filter.id) !== -1);
  }

}
