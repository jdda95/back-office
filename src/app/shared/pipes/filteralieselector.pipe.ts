import { Pipe, PipeTransform } from '@angular/core';
import { Alie } from '../models/alie.model';
import { Country } from "../models/country.model";

@Pipe({
  name: 'filteralieselector'
})
export class FilteralieselectorPipe implements PipeTransform {

  transform(items: Alie[], filter:Country ): any {
    if(!items || !filter){
      return items;
    }
    return items.filter(item => item.countryID.indexOf(filter.id) !== -1);
  }

}
