import { Component } from '@angular/core';
import { ThemeConstantService } from '../../services/theme-constant.service';
// Services
import { AssociateCRUDService } from '../../services/associate-crud.service';
import { AdminCrudService } from '../../services/admin-crud.service';
import { AuthenticationService } from '../../services/authentication.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
// Models
import { Associate } from '../../models/associate.model';
import { AdminCrud } from '../../models/admin-crud.model';
import { Observable } from 'rxjs';
import { keyframes } from '@angular/animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  searchVisible: boolean = false;
  quickViewVisible: boolean = false;
  isFolded: boolean;
  isExpand: boolean;
  associate: Associate;
  administrator: AdminCrud;
  associate$: Observable<Associate>;
  administrator$: Observable<AdminCrud>;
  profile = false;
  currentUser: string;
  currentMail: string;

  constructor(
    private themeService: ThemeConstantService,
    private assoCRUDServ: AssociateCRUDService,
    private admCRUDServ: AdminCrudService,
    private authServ: AuthenticationService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {
    if (
      this.router.url.includes('op') ||
      this.router.url.includes('adm') ||
      this.router.url.includes('rel')
    ) {
      this.profile = true;
    }
  }

  ngOnInit(): void {
    let aux = true;
    this.authServ.hasUser().forEach((user) => {
      if (aux) {
        if (user !== null) {
          this.currentUser = user.uid;
          this.currentMail = user.email;
          this.evaluateData();
        } else {
          console.log('empty user');
        }
        aux = false;
      }
    });
    // this.authServ.hasUserB().then((res) => {
    //   console.log(`b-side`);
    //   if (res !== null) {
    //     console.log(res);
    //     this.currentUser = res.uid;
    //     this.currentMail = res.email;
    //     this.evaluateData();
    //   } else {
    //     console.log('empty user');
    //   }
    // });
    this.themeService.isMenuFoldedChanges.subscribe(
      (isFolded) => (this.isFolded = isFolded)
    );
    this.themeService.isExpandChanges.subscribe(
      (isExpand) => (this.isExpand = isExpand)
    );

    if (this.profile == false) {
      this.associate$ = this.assoCRUDServ.getAssociate$();
      this.associate$.subscribe((newData) => {
        this.associate = newData;
      });
    } else {
      this.administrator$ = this.admCRUDServ.getAdmin$();
      this.administrator$.subscribe((newData) => {
        this.administrator = newData;
      });
    }
  }

  toggleFold() {
    this.isFolded = !this.isFolded;
    this.themeService.toggleFold(this.isFolded);
  }

  toggleExpand() {
    this.isFolded = false;
    this.isExpand = !this.isExpand;
    this.themeService.toggleExpand(this.isExpand);
    this.themeService.toggleFold(this.isFolded);
  }

  quickViewToggle(): void {
    this.quickViewVisible = !this.quickViewVisible;
  }

  evaluateData() {
    if (this.profile == false) {
      // if (this.assoCRUDServ.associate != undefined) {
      //   this.associate = this.assoCRUDServ.associate;
      // } else {
      this.assoCRUDServ.getCurrentAssociateData(this.currentUser);
      // }
    } else {
      // if (this.admCRUDServ.admin !== undefined) {
      //   this.associate = undefined;
      //   this.administrator = this.admCRUDServ.admin;
      // } else {
      this.admCRUDServ.getAdministratorData(this.currentMail);
      // }
    }
  }
  logOut() {
    this.authServ.logout().then(() => {
      this.assoCRUDServ.associate = undefined;
      this.router.navigate(['/auth/login']);
    });
  }
}
