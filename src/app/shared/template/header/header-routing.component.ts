import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent} from './header.component'

const routes: Routes = [
  {
    path: ' ',
    component: HeaderComponent,
    data: {
      title: 'header ',
      headerDisplay: 'none',
    },
  },
  {
    path: ':profile',
    component: HeaderComponent,
    data: {
      title: 'header ',
      headerDisplay: 'none',
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HeaderRoutingComponent{}
