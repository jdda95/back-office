import { Component } from '@angular/core';
import {
  FINANCIALROUTES,
  SUPERADMINROUTES,
  PUBLICRELATIONSHIPROUTES,
  NATIONALROUTES,
  ASSOCIATEROUTES,
} from './side-nav-routes.config';
import { ThemeConstantService } from '../../services/theme-constant.service';
// import * as firebase from 'firebase/app';
import { AuthenticationService } from '../../services/authentication.service';
import { MenuNavService } from '../../services/menu-nav.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './side-nav.component.html',
})
export class SideNavComponent {
  public menuItems: any[];
  isFolded: boolean;
  isSideNavDark: boolean;

  constructor(
    private themeService: ThemeConstantService,
    private authServ: AuthenticationService,
    private menuServ: MenuNavService
  ) {}

  ngOnInit(): void {
    this.themeService.isMenuFoldedChanges.subscribe(
      (isFolded) => (this.isFolded = isFolded)
    );
    this.themeService.isSideNavDarkChanges.subscribe(
      (isDark) => (this.isSideNavDark = isDark)
    );
    let aux = true;
    this.authServ.hasUser().forEach((user) => {
      if (aux) {
        if (user !== null) {
          user.getIdTokenResult().then((idTokenResult) => {
            switch (idTokenResult.claims.role) {
              case 1:
                // console.log(`Operativo header`);
                if (!this.menuServ.setNow) {
                  SUPERADMINROUTES.push({
                    path: '/billeteras/12',
                    title: 'Billetera Operativo',
                    iconType: 'fontawesome',
                    iconTheme: 'fas',
                    icon: 'fa-wallet',
                    submenu: [],
                  });
                }
                this.menuItems = SUPERADMINROUTES.filter(
                  (menuItem) => menuItem
                );

                this.menuServ.setNow = true;

                break;
              case 2:
                // console.log(`Administrativo header`);
                if (!this.menuServ.setNow) {
                  FINANCIALROUTES.push({
                    path: '/billeteras/13',
                    title: 'Billetera Administrativo',
                    iconType: 'fontawesome',
                    iconTheme: 'fas',
                    icon: 'fa-wallet',
                    submenu: [],
                  });
                }
                this.menuItems = FINANCIALROUTES.filter((menuItem) => menuItem);

                this.menuServ.setNow = true;
                break;
              case 3:
                // console.log(`Relaciones públicas header`);
                if (!this.menuServ.setNow) {
                  PUBLICRELATIONSHIPROUTES.push({
                    path: '/billeteras/14',
                    title: 'Billetera Relaciones Públicas',
                    iconType: 'fontawesome',
                    iconTheme: 'fas',
                    icon: 'fa-wallet',
                    submenu: [],
                  });
                }
                this.menuItems = PUBLICRELATIONSHIPROUTES.filter(
                  (menuItem) => menuItem
                );

                this.menuServ.setNow = true;
                break;
              case 4:
                // console.log(`Nacional header`);
                if (!this.menuServ.setNow) {
                  NATIONALROUTES.forEach((el) => {
                    if (el.title === 'Billeteras') {
                      el.submenu.push({
                        path: '/billeteras/6',
                        title: 'Billetera Nacional',
                        iconType: '',
                        iconTheme: '',
                        icon: '',
                        submenu: [],
                      });
                    }
                  });
                }
                this.menuItems = NATIONALROUTES.filter((menuItem) => menuItem);

                this.menuServ.setNow = true;
                break;
              case 5:
                // console.log(`Zonal header`);
                if (!this.menuServ.setNow) {
                  NATIONALROUTES.forEach((el) => {
                    if (el.title === 'Billeteras') {
                      el.submenu.push({
                        path: '/billeteras/9',
                        title: 'Billetera Zonal',
                        iconType: '',
                        iconTheme: '',
                        icon: '',
                        submenu: [],
                      });
                    }
                  });
                }
                this.menuItems = NATIONALROUTES.filter((menuItem) => menuItem);

                this.menuServ.setNow = true;
                break;
              default:
                // console.log(`none claims side nav`);
                this.menuItems = ASSOCIATEROUTES.filter((menuItem) => menuItem);

                break;
            }
          });
        }
      } else {
        // console.log('empty user');
      }
      aux = false;
    });
  }
}
