import { Injectable } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";
import { Membership } from '../models/membership.model';

@Injectable({
  providedIn: 'root'
})
export class MembershipsService {

  constructor(private firaBase: AngularFirestore) { }

  membershipsList = this.firaBase.collection('memberships',ref => ref.orderBy('price'));

  getMemberships() {
    return this.membershipsList;
  }

  setMemeberships(membID,dataObj){
    return this.membershipsList.doc(membID).update(dataObj);
  }

  updateUserMemebership(userID: string, membershipID: string) {
    return this.firaBase.doc('associates/' + userID).update({ membershipID: membershipID });
  }
}
