import { Injectable } from '@angular/core';
//Services
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import * as firebase from 'firebase/app';
//Models
import { Associate } from '../../shared/models/associate.model';
import { Wallet } from '../../shared/models/wallet.model';

@Injectable({
  providedIn: 'root',
})
export class AssociateCRUDService {
  associate: Associate;
  private assocaite$ = new Subject<Associate>();

  wallets: Wallet[] = [];
  private wallets$ = new Subject<Wallet[]>();

  constructor(private fireBase: AngularFirestore) { }

  getAssociateList(countryID?: string, cityID?: string, status?: number) {
    return this.fireBase.collection('associates', (ref) => ref.orderBy('date', 'desc'));
  }

  getAssociateData(associateid: string) {
    return this.fireBase.collection('associates').doc(associateid);
  }

  getCurrentAssociateData(associateid: string) {
    return this.fireBase
      .collection('associates')
      .doc(associateid)
      .snapshotChanges()
      .subscribe((res) => {
        this.associate = new Associate();
        if (res.payload.data() !== undefined) {
          Object.keys(res.payload.data()).forEach((key) => {
            this.associate[key] = res.payload.data()[key];
          });
          // console.log(`call next update service`);
          this.assocaite$.next(this.associate);
        } else {
          console.error('Error getting users Data');
        }
      });
  }

  getAssociate$(): Observable<Associate> {
    return this.assocaite$.asObservable();
  }
  updateAssociateData(
    fireId: string,
    updateData: Object,
    updateSimpleData?: Object
  ) {
    const myBatch = this.fireBase.firestore.batch();
    const userToUpdate = this.fireBase.doc(`associates/${fireId}`).ref;
    myBatch.update(userToUpdate, updateData);
    if (updateSimpleData !== null && updateSimpleData !== undefined && updateSimpleData !== '' && updateSimpleData['ref'] !== '' && updateSimpleData['ref'] !== undefined) {
      myBatch.update(this.fireBase.doc(updateSimpleData['ref']).ref, {
        status: updateSimpleData['status'],
      });
    }
    return myBatch
      .commit()
      .then(() => {
        return this.fireBase
          .collection('associates')
          .doc(fireId)
          .update(updateData);
      })
      .catch((err) => {
        console.error(`Error updating complex and simple asso data`);
      });
  }
  updateSimpleAssociateData(
    referralList: string,
    updateData: Object
  ) {
    return this.fireBase.doc(referralList).update(updateData);
  }

  getAssociateWallets(associateID: string) {
    this.wallets = this.wallets.filter((d) => d.name == '');
    return this.fireBase
      .collection('wallets', (ref) => ref.where('owner', '==', associateID))
      .snapshotChanges()
      .subscribe((walletsData) => {
        walletsData.forEach((cuWallet) => {
          let wallet = new Wallet();
          Object.keys(cuWallet.payload.doc.data()).forEach((key) => {
            wallet.fireID = cuWallet.payload.doc.id;
            wallet[key] = cuWallet.payload.doc.data()[key];
          });
          // evaluate if already has object duplicate and eliminate
          this.wallets.push(wallet);
          const seen = new Set();
          this.wallets = this.wallets.filter((el) => {
            const duplicate = seen.has(el.name);
            seen.add(el.name);
            return !duplicate;
          });
          this.wallets$.next(this.wallets);
        });
      });
  }

  getAssociateWallets$(): Observable<Wallet[]> {
    return this.wallets$.asObservable();
  }

  getAssociateRefList(listID: string) {
    return this.fireBase
      .collection('referrals_list')
      .doc(listID)
      .collection('my_referr');
  }

  getAssociateForMail(assoMail: string) {
    return this.fireBase
      .collection('associates', (ref) => ref.where('email', '==', assoMail))
      .snapshotChanges()
      .subscribe((users) => {
        users.forEach((user) => {
          return user.payload.doc.id;
        });
      });
  }
}
