import { Injectable } from '@angular/core';
// Service
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class GeneralInfoService {

  constructor(private fireBase:AngularFirestore) { }

  infoCollection = this.fireBase.collection('information');
  
  getInfo(){
    return this.infoCollection;
  }

  setInfo(docID,dataObj){
    return this.infoCollection.doc(docID).update(dataObj);
  }
}
