import { Injectable } from '@angular/core';
//Firebase
import { AngularFirestore } from "@angular/fire/firestore";
// Model
import { Alie } from "../models/alie.model";

@Injectable({
  providedIn: 'root'
})
export class AliesService {

  constructor(private fireBase: AngularFirestore) { }

  aliesList = this.fireBase.collection('alies');
  walletsCollection = this.fireBase.collection('wallets');

  getAlies() {
    return this.aliesList;
  }
  setAlies(newAlies: Alie[]) {
    const myBatch = this.fireBase.firestore.batch();
    //new clone object on javascript
    newAlies.forEach(alie => {
      let newAlie = { ...alie };
      let newalieId = this.fireBase.createId();
      const newAlieb = this.aliesList.doc(newalieId);
      myBatch.set(newAlieb.ref, newAlie);
      // // create wallet of director
      // const newWalletDoc = this.walletsCollection.doc(this.fireBase.createId());
      // let wAlido = {
      //   owner: newalieId,
      //   type: 1,
      //   name: `Aliado ${alie.name}`,
      //   max_balance: 0,
      //   current_balance: 0,
      //   movementsList: '',
      //   date: new Date()
      // }
    });
    return myBatch.commit();
  }

  deleteAlie(aliesID) {
    return this.aliesList.doc(aliesID).delete();
  }

  updateAlly(allyId, fields) {
    return this.aliesList.doc(allyId).update(fields);
  }
}
