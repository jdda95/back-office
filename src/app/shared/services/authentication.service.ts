import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

//Services
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
//Models
import { Associate } from '../models/associate.model';

@Injectable()
export class AuthenticationService {
  // private currentUserSubject: BehaviorSubject<User>;
  // public currentUser: Observable<User>;
  // only for localhost
  private user: Subject<Associate>;

  associateCollection = this.fireBase.collection('associates');
  adminCollection = this.fireBase.collection('administrators');

  constructor(
    private http: HttpClient,
    private af: AngularFireAuth,
    private fireBase: AngularFirestore
  ) {}

  getFirebaseApp(name) {
    let foundApp = firebase.apps.find((app) => app.name === name);
    return foundApp
      ? foundApp
      : firebase.initializeApp(firebase.app().options, 'auth-associate');
  }
  createUser(associate: Associate, status: boolean) {
    let authApp = this.getFirebaseApp('auth-associate');
    let authAuth = firebase.auth(authApp);
    authAuth.setPersistence(firebase.auth.Auth.Persistence.NONE);
    return authAuth
      .createUserWithEmailAndPassword(associate['email'], associate['password'])
      .then((res) => {
        delete associate['password'];
        delete associate['repassword'];
        associate['fireId'] = res.user.uid;
        associate['countryID'] = associate['country']['id'];
        delete associate['country'];
        associate['rankID'] = 'rk_asso';
        associate['rankName'] = 'Afiliado';
        associate['status'] = status;
        associate['coverPic'] = 'assets/images/avatars/generic.png';
        associate['profilePic'] = 'assets/images/avatars/generic.png';
        associate['membershipID'] = '';
        associate['noPay'] = 0;
        // set role
        return associate;
      });
    // return this.af
    //   .createUserWithEmailAndPassword(associate['email'], associate['password'])
    //   .then((res) => {
    //     delete associate['password'];
    //     delete associate['repassword'];
    //     associate['fireId'] = res.user.uid;
    //     associate['countryID'] = associate['country']['id'];
    //     delete associate['country'];
    //     associate['rankID'] = 'rk_asso';
    //     associate['rankName'] = 'Afiliado';
    //     associate['status'] = false;
    //     associate['coverPic'] = 'assets/images/avatars/generic.png';
    //     associate['profilePic'] = 'assets/images/avatars/generic.png';
    //     associate['membershipID'] = '';
    //     associate['noPay'] = 0;
    //     // set role
    //     return associate;
    //   });
  }

  createAssociate(associate: Associate) {
    let data = { ...associate };
    return this.associateCollection.doc(data['fireId']).set(data);
  }

  createAdminUser(admin: Object) {
    return this.af
      .createUserWithEmailAndPassword(admin['email'], admin['password'])
      .then((res) => {
        delete admin['password'];
        delete admin['confirm'];
        return this.adminCollection.doc(`${res.user.uid}`).set(admin);
      })
      .catch((err) => {
        console.log(`error creating admin user ${err}`);
      });
  }

  // public get currentUserValue(): User {
  //     return this.currentUserSubject.value;
  // }

  login(dataform: Object) {
    // firebase.auth().useEmulator("http://localhost:9099/");
    return this.af
      .setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      .then(() => {
        return this.af.signInWithEmailAndPassword(
          dataform['email'],
          dataform['password']
        );
      });
  }

  logout() {
    // firebase.auth().useEmulator("http://localhost:9099/");
    return this.af.signOut();
  }

  hasUser() {
    // firebase.auth().useEmulator("http://localhost:9099/");
    // return this.af.currentUser;
    return this.af.authState;
  }
  hasUserB() {
    return this.af.currentUser;
  }

  // only for local host
  getAssociateTemp$(): Observable<Associate> {
    return this.user.asObservable();
  }
}
