import { Injectable } from '@angular/core';
//Services
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
//Models
import { AdminCrud } from '../models/admin-crud.model';

@Injectable({
  providedIn: 'root',
})
export class AdminCrudService {
  admin: AdminCrud;
  private admin$ = new Subject<AdminCrud>();

  constructor(private fireBase: AngularFirestore) {}

  getAdministratorsList() {
    return this.fireBase.collection('administrators').snapshotChanges();
  }
  getMarketingDir() {
    return this.fireBase.doc('/ranks_dir/cFi4HSMwobtUChFso00n').get();
  }

  getAdministratorData(admMail: string) {
    return this.fireBase
      .collection('administrators', (ref) => ref.where('email', '==', admMail))
      .snapshotChanges()
      .subscribe((adminData) => {
        adminData.forEach((cuAdm) => {
          let newAdm = new AdminCrud();
          Object.keys(cuAdm.payload.doc.data()).forEach((key) => {
            newAdm[key] = cuAdm.payload.doc.data()[key];
          });
          this.admin = newAdm;
          this.admin$.next(newAdm);
        });
      });
  }

  getAdmin$(): Observable<AdminCrud> {
    return this.admin$.asObservable();
  }
}
