import { Injectable } from '@angular/core';
//Services
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { firestore } from 'firebase/app';
// Model
import { Movement } from '../../shared/models/movement.model';
import { query } from 'chartist';

@Injectable({
  providedIn: 'root',
})
export class TokeAndMovementsService {
  tokenColl = this.fireBase.collection('reg_tok');
  movementColl = this.fireBase.collection('req_movements');
  walletsColl = this.fireBase.collection('wallets');

  constructor(private fireBase: AngularFirestore) {}

  createToken(token) {
    return this.tokenColl.doc(token['fireID']).set({ ...token });
  }

  evaluateToken(token, typeReq) {
    let diffInMs: number = 0;
    let fireID: string = '';
    let typeToken: number = 0;
    let tokenStatus: boolean = false;

    return this.tokenColl
      .doc(`${token['token']}`)
      .get()
      .forEach((data) => {
        let setDate = new Date(data.get('date') * 1000);
        let currentDate = new Date();
        diffInMs = currentDate.getMinutes() - setDate.getMinutes();
        fireID = data.get('userID');
        typeToken = data.get('type');
        tokenStatus = data.get('status');
      })
      .then(async () => {
        // evaluate if token is expired
        if (
          diffInMs < 30 &&
          token['user1'] === fireID &&
          tokenStatus === true &&
          typeToken == typeReq
        ) {
          // preparate movement to insert
          let newMove = new Movement();
          newMove.date = new Date();
          newMove.amount = token['mount'];
          newMove.status = typeToken == 2 ? 3 : 1;
          newMove.tokenID = token['token'];
          newMove.user1 = token['user1'];
          newMove.user1email = token['user1email'];
          newMove.user2 = token['user2'] !== undefined ? token['user2'] : '';
          newMove.user2email =
            token['user2email'] !== undefined ? token['user2email'] : '';
          newMove.walletID1 = token['walletID1'];
          newMove.walletID2 =
            token['walletID2'] !== undefined ? token['walletID2'] : '';
          newMove.type = typeToken;
          let okRules = '';
          await this.registerRequest(newMove)
            .then(() => {
              okRules = 'Solicitud realizada satisfactoriamente';
            })
            .catch((err) => {
              console.log(err);
              okRules =
                'Error generando la soli favor intentelo más tarde nuevamente';
            });
          return okRules;
        } else {
          if (tokenStatus) {
            await this.disableToken(token['token']);
          }
          return 'Error en la solicitud el token puede estar vencido o ya fue usado en otra operación, solicite uno nuevamente';
        }
      })
      .catch((err) => {
        return `erro-> ${err}`;
      });
  }

  async registerRequest(movement) {
    if (movement['type'] === 1) {
      const myBatch = this.fireBase.firestore.batch();
      let moveID = this.fireBase.createId();
      const creatMove = this.movementColl.doc(moveID).ref;
      myBatch.set(creatMove, { ...movement });

      const disableToken = this.tokenColl.doc(movement['tokenID']).ref;
      myBatch.update(disableToken, { status: false });
      return myBatch.commit();
    } else if (movement['type'] === 2) {
      const myBatch = this.fireBase.firestore.batch();

      const creatMove = this.movementColl.doc(this.fireBase.createId()).ref;
      myBatch.set(creatMove, { ...movement });

      const reduceBalance = this.walletsColl.doc(movement['walletID1']).ref;
      const decreaseVal = firestore.FieldValue.increment(-movement['amount']);
      myBatch.update(reduceBalance, { current_balance: decreaseVal });

      const increaseBalance = this.walletsColl.doc(movement['walletID2']).ref;
      const increaseVal = firestore.FieldValue.increment(movement['amount']);
      myBatch.update(increaseBalance, { current_balance: increaseVal });

      const disableToken = this.tokenColl.doc(movement['tokenID']).ref;
      myBatch.update(disableToken, { status: false });
      return myBatch.commit();
    }
  }

  async disableToken(tokenID) {
    this.tokenColl
      .doc(tokenID)
      .update({ status: false })
      .catch((err) => {
        console.log(`error disabling code ${err}`);
      });
  }

  async getMovementList(walletID) {
    let resMovements: Movement[] = [];
    let sended = this.fireBase
      .collection('req_movements', (ref) =>
        ref.orderBy('date', 'desc').where('walletID1', '==', walletID)
      )
      .get();
    await sended.forEach((movements) => {
      movements.docs.forEach((movement) => {
        let cuMove = new Movement();
        cuMove.fireID = movement.id;
        Object.keys(movement.data()).forEach((key) => {
          cuMove[key] = movement.data()[key];
        });
        resMovements.push(cuMove);
      });
    });
    let received = this.fireBase
      .collection('req_movements', (ref) =>
        ref.orderBy('date').where('walletID2', '==', walletID)
      )
      .get();
    await received.forEach((movements) => {
      movements.docs.forEach((movement) => {
        let cuMove = new Movement();
        cuMove.fireID = movement.id;
        Object.keys(movement.data()).forEach((key) => {
          cuMove[key] = movement.data()[key];
        });
        resMovements.push(cuMove);
      });
    });
    return resMovements;
  }

  getAllMovements() {
    return this.movementColl.get();
  }

  getAllMovementsFilter(field, filter) {
    return this.fireBase
      .collection('req_movements', (ref) =>
        ref.orderBy('date', 'desc').where(field, '==', filter)
      )
      .get();
  }

  getMovementForId(moveID) {
    return this.fireBase.doc(`req_movements/${moveID}`).get();
  }

  async changeStatusMovement(moveData) {
    let result = {};
    const myBatch = this.fireBase.firestore.batch();
    const updateMove = this.movementColl.doc(moveData['id']).ref;
    myBatch.update(updateMove, { status: moveData['newStatus'] });

    if (moveData['type'] == 1) {
      const reduceBalance = this.walletsColl.doc(moveData['walletID1']).ref;
      const decreaseVal = firestore.FieldValue.increment(-moveData['amount']);
      myBatch.update(reduceBalance, { current_balance: decreaseVal });
    }
    await myBatch
      .commit()
      .then((res) => {
        result = { msg: 'Actualización de estado satisfactorio', status: 1 };
      })
      .catch((err) => {
        result = {
          msg: 'Error en el proceso, intentelo más tarde nuevamente',
          status: 2,
          err: err,
        };
      });
    return result;
  }
}
