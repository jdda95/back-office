import {
  Injectable,
  ɵSWITCH_COMPILE_INJECTABLE__POST_R3__,
} from '@angular/core';
//FireBase Service
import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root',
})
export class WalletService {
  totalMoney = 0;
  walletsCollection = this.fireBase.collection('wallets');
  constructor(private fireBase: AngularFirestore) {}

  createAssociateWallets(wallets: Object[]) {
    const myBatch = this.fireBase.firestore.batch();
    //new clone object on javascript
    wallets.forEach((wallet) => {
      let newWallet = { ...wallet };
      const newWalletDoc = this.walletsCollection.doc(newWallet['docID']);
      delete newWallet['stringID'];
      delete newWallet['docID'];
      myBatch.set(newWalletDoc.ref, newWallet);
    });
    return myBatch.commit();
  }

  createAlieWallet() {}
}
