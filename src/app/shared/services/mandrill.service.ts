import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
const mandrillApiKey = 'jS7WASXX5FWq47bi1-wyFA';

@Injectable({
  providedIn: 'root',
})
export class MandrillService {
  constructor(private http: HttpClient) {}

  sendContactMessage(to, toname, mergevariables) {
    let body = {
      to,
      toname,
      mergevariables,
      subject: 'Gracias por contactarnos',
      template: 'contacto-desde-web',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
  sendDirectorNatNotification(to, toname, mergevariables) {
    let body = {
      to,
      toname,
      mergevariables,
      subject: 'Nuevo Aspirante',
      template: 'director-nacional',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
  sendLoginMail(to, toname, mergevariables) {
    let body = {
      to,
      toname,
      mergevariables,
      subject: 'Cuenta activada',
      template: 'notificaci-n-de-activaci-n',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
  sendFinanMail() {
    let body = {
      to: 'cfo@empresarios24-7.com',
      toname: 'Diana',
      mergevariables: {},
      subject: 'Nuevo afiliado',
      template: 'dir-administrativa',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
  sendMonetizacion(mergevariables: {
    USER: string;
    VALUE: string;
    WALLET: string;
  }) {
    let body = {
      to: 'cfo@empresarios24-7.com',
      toname: 'Diana',
      mergevariables,
      subject: 'Nueva solicitud de monetización',
      template: 'monetizaci-n',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
  sendTransaction(
    to: string,
    toname: string,
    mergevariables: { USERSEND: string; WALLET: string; VALUE: string }
  ) {
    let body = {
      to,
      toname,
      mergevariables,
      subject: 'Nueva solicitud de monetización',
      template: 'nueva-transferencia',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
  sendTokeEmail(to: string, toname: string, mergevariables: { token: string }) {
    let body = {
      to,
      toname,
      mergevariables,
      subject: 'Solicitud TOKEN',
      template: 'send-token',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
  moneyAccept(to: string, toname: string) {
    let body = {
      to,
      toname,
      mergevariables: {},
      subject: 'Solicitud Aceptada',
      template: 'money-success',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
  moneyDenied(to: string, toname: string) {
    let body = {
      to,
      toname,
      mergevariables: {},
      subject: 'Solicitud Rechazada',
      template: 'money-denied',
      mandrillApiKey,
    };
    let serverinfo: Observable<any>;
    serverinfo = this.http.post(environment.notificationLink, body);
    return serverinfo;
  }
}
