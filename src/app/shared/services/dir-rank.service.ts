import { Injectable } from '@angular/core';
//Firebase
import { AngularFirestore } from '@angular/fire/firestore';
import { NzMessageService } from 'ng-zorro-antd';
import { GeneralInfoService } from '../../shared/services/general-info.service';
// model

@Injectable({
  providedIn: 'root',
})
export class DirRankService {
  constructor(
    private fireBase: AngularFirestore,
    private nzMessageService: NzMessageService,
    private infoServ: GeneralInfoService
  ) {
    this.getGeneralInfo();
  }
  dirrkList = this.fireBase.collection('ranks_dir');
  walletsCollection = this.fireBase.collection('wallets');
  // set start value links wallets
  linksValue = 0;

  getGeneralInfo() {
    this.infoServ
      .getInfo()
      .get()
      .subscribe((data) => {
        data.docs.forEach((doc) => {
          this.linksValue = doc.data()['matrixBudget'];
        });
      });
  }

  getRanksList() {
    return this.dirrkList;
  }

  setDirector(formData: any) {
    // console.log(`country id  -> ${formData["country"]["id"]}`);
    // console.log(`zone id  -> ${formData["cityID"]}`);
    // console.log(`email to search  -> ${formData["email"]}`);
    // console.log(`director Type selected -> ${formData["directorType"]}`);

    // evaluate  country
    if (formData['cityID'] == '') {
      let subCountry = this.fireBase
        .collection('countries')
        .snapshotChanges()
        .subscribe((res) => {
          res.forEach((countrie) => {
            // evaluate country sended
            if (formData['country']['id'] == countrie.payload.doc.id) {
              // evaluate if has national and mk  director
              Object.keys(countrie.payload.doc.data()).forEach((key) => {
                switch (key) {
                  case `natDirectorMail`:
                    // creating national director
                    if (
                      countrie.payload.doc.data()[key] != '' &&
                      formData['directorType'] == 'nJB8qZe8gVcKmJYxj98g'
                    ) {
                      console.log(
                        `No puede asignar director nacional, ${
                          formData['country']['id']
                        } ya tiene uno ->${countrie.payload.doc.data()[key]}`
                      );
                      this.nzMessageService.info(
                        `No puede asignar director nacional, ya tiene uno.`
                      );
                      // --- ****************************** ---
                      // cambiar cuando se cambie a la base de datos final el id de comparación
                      // --- ****************************** ---
                    } else if (
                      formData['directorType'] == 'nJB8qZe8gVcKmJYxj98g' &&
                      countrie.payload.doc.data()[key] == ''
                    ) {
                      // console.log(`don't has national director -> ${countrie.payload.doc.data()[key]}`);
                      // evaluate if user exist to set director
                      let subAsso = this.fireBase
                        .collection('associates', (ref) =>
                          ref.where('email', '==', formData['email'])
                        )
                        .snapshotChanges()
                        .subscribe((resuser) => {
                          // evaluate if exist user with email
                          if (resuser.length > 0) {
                            resuser.forEach((user) => {
                              // evaluate if user is already director
                              if (
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('naldirectorOf') ||
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('zndirectorOf') ||
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('mkdirectorOf')
                              ) {
                                //  is national
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('naldirectorOf')
                                ) {
                                  this.nzMessageService.info(
                                    `No puede ser director nacional de ${
                                      formData['country']['id']
                                    } porque es director de -> ${
                                      user.payload.doc.data()['naldirectorOf']
                                    }`
                                  );
                                  console.log(
                                    `No puede ser director nacional de ${
                                      formData['country']['id']
                                    } porque es director de -> ${
                                      user.payload.doc.data()['naldirectorOf']
                                    }`
                                  );
                                }
                                // is zonal
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('zndirectorOf')
                                ) {
                                  this.nzMessageService.info(
                                    `No puede ser director nacional. Ya es director de zona.`
                                  );
                                  console.log(
                                    `No puede ser director nacional de ${
                                      formData['country']['id']
                                    } Ya es director de zona de -> ${
                                      user.payload.doc.data()['znldirectorOf']
                                    }`
                                  );
                                }
                                // is marketing
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('mkdirectorOf')
                                ) {
                                  this.nzMessageService.info(
                                    `No puede ser director nacional.Ya es director de marketing.`
                                  );
                                }
                              } else {
                                console.log(
                                  `Podría ser director nacional de ${formData['country']['id']}`
                                );
                                // update data of asso to new new national director
                                const myBatch = this.fireBase.firestore.batch();
                                const updAsso = this.fireBase.doc(
                                  `associates/${
                                    user.payload.doc.data()['fireId']
                                  }`
                                );
                                let walletNalID = this.fireBase.createId();
                                let walletMatID = this.fireBase.createId();
                                myBatch.update(updAsso.ref, {
                                  naldirectorOf: formData['country']['id'],
                                  walletNalDir: walletNalID,
                                  walletMatDir: walletMatID,
                                });
                                // create wallet of director
                                const newWalletDoc = this.walletsCollection.doc(
                                  walletNalID
                                );
                                let wNalDir = {
                                  owner: user.payload.doc.data()['fireId'],
                                  type: 6,
                                  name: 'Director Nacional',
                                  max_balance: 0,
                                  current_balance: 0,
                                  movementsList: '',
                                  date: new Date(),
                                };
                                // create wallet of matrix
                                const newWalletDocM = this.walletsCollection.doc(
                                  walletMatID
                                );
                                let wMatrix = {
                                  owner: user.payload.doc.data()['fireId'],
                                  type: 7,
                                  name: ' Matriz',
                                  max_balance: this.linksValue,
                                  current_balance: 0,
                                  movementsList: '',
                                  date: new Date(),
                                };
                                myBatch.set(newWalletDoc.ref, wNalDir);
                                myBatch.set(newWalletDocM.ref, wMatrix);
                                // update country with new director data
                                const updContry = this.fireBase.doc(
                                  `countries/${formData['country']['id']}`
                                );
                                myBatch.update(updContry.ref, {
                                  natDirectorUID: user.payload.doc.data()[
                                    'fireId'
                                  ],
                                  natDirectorMail: formData['email'],
                                });
                                //  send all tasks
                                subCountry.unsubscribe();
                                subAsso.unsubscribe();
                                myBatch
                                  .commit()
                                  .then(() => {
                                    this.nzMessageService.create(
                                      'success',
                                      `Director Nacional asignado`
                                    );
                                  })
                                  .catch((err) => {
                                    this.nzMessageService.create(
                                      'error',
                                      `Error actualizando director nacional.`
                                    );
                                  });
                              }
                            });
                          } else {
                            this.nzMessageService.create(
                              'error',
                              `El usuario no existe`
                            );
                          }
                        });
                    }
                    break;
                  case `mrkDirectorMail`:
                    // creating marketing director
                    /*
                    if (
                      countrie.payload.doc.data()[key] != '' &&
                      formData['directorType'] == 'cFi4HSMwobtUChFso00n'
                    ) {
                      console.log(
                        `can't assign marketing director, ${
                          formData['country']['id']
                        } ya tiene uno ->${countrie.payload.doc.data()[key]}`
                      );
                      this.nzMessageService.create(
                        'error',
                        `can't assign marketing director, ${
                          formData['country']['id']
                        } ya tiene uno ->${countrie.payload.doc.data()[key]}`
                      );
                      // --- ****************************** ---
                      // cambiar cuando se cambie a la base de datos final el id de comparación
                      // --- ****************************** ---
                    } else if (
                      formData['directorType'] == 'cFi4HSMwobtUChFso00n' &&
                      countrie.payload.doc.data()[key] == ''
                    ) {
                      // console.log(`don't has national director -> ${countrie.payload.doc.data()[key]}`);
                      // evaluate if user exist to set director
                      let subAsso = this.fireBase
                        .collection('associates', (ref) =>
                          ref.where('email', '==', formData['email'])
                        )
                        .snapshotChanges()
                        .subscribe((resuser) => {
                          // evaluate if exist user with email
                          if (resuser.length > 0) {
                            resuser.forEach((user) => {
                              // evaluate if user is already director
                              if (
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('naldirectorOf') ||
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('zndirectorOf') ||
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('mkdirectorOf')
                              ) {
                                //  is national
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('naldirectorOf')
                                ) {
                                  console.log(
                                    `No podría ser director de marketing de ${
                                      formData['country']['id']
                                    } porque es director de -> ${
                                      user.payload.doc.data()['naldirectorOf']
                                    }--`
                                  );
                                  this.nzMessageService.create(
                                    'error',
                                    `No podría ser director de marketing de ${
                                      formData['country']['id']
                                    } porque es director de -> ${
                                      user.payload.doc.data()['naldirectorOf']
                                    }--`
                                  );
                                }
                                // is zonal
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('zndirectorOf')
                                ) {
                                  console.log(
                                    `No podría ser director de marketing de ${
                                      formData['country']['id']
                                    } Ya es director de zona de -> ${
                                      user.payload.doc.data()['znldirectorOf']
                                    }`
                                  );
                                  this.nzMessageService.create(
                                    'error',
                                    `No podría ser director de marketing de ${
                                      formData['country']['id']
                                    } Ya es director de zona de -> ${
                                      user.payload.doc.data()['znldirectorOf']
                                    }`
                                  );
                                }
                                // is marketing
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('mkdirectorOf')
                                ) {
                                  console.log(
                                    `No podría ser director de marketing de ${
                                      formData['country']['id']
                                    } ya es director de marketing de -> ${
                                      user.payload.doc.data()['mkdirectorOf']
                                    }`
                                  );
                                  this.nzMessageService.create(
                                    'error',
                                    `No podría ser director de marketing de ${
                                      formData['country']['id']
                                    } ya es director de marketing de -> ${
                                      user.payload.doc.data()['mkdirectorOf']
                                    }`
                                  );
                                }
                              } else {
                                console.log(
                                  `Podría ser director de marketing de ${formData['country']['id']}`
                                );
                                this.nzMessageService.create(
                                  'error',
                                  `Podría ser director de marketing de ${formData['country']['id']}`
                                );
                                // update data of asso to new new national director
                                const myBatch = this.fireBase.firestore.batch();
                                const updAsso = this.fireBase.doc(
                                  `associates/${
                                    user.payload.doc.data()['fireId']
                                  }`
                                );
                                let walletID = this.fireBase.createId();
                                myBatch.update(updAsso.ref, {
                                  mkdirectorOf: formData['country']['id'],
                                  walletMkDir: walletID,
                                });
                                // create wallet of director
                                const newWalletDoc = this.walletsCollection.doc(
                                  walletID
                                );
                                let wMkDir = {
                                  owner: user.payload.doc.data()['fireId'],
                                  type: 8,
                                  name: 'Director Marketing',
                                  max_balance: 0,
                                  current_balance: 0,
                                  movementsList: '',
                                  date: new Date(),
                                };
                                myBatch.set(newWalletDoc.ref, wMkDir);
                                // update country with new director data
                                const updContry = this.fireBase.doc(
                                  `countries/${formData['country']['id']}`
                                );
                                myBatch.update(updContry.ref, {
                                  mrkDirectorUID: user.payload.doc.data()[
                                    'fireId'
                                  ],
                                  mrkDirectorMail: formData['email'],
                                });
                                //  send all tasks
                                subCountry.unsubscribe();
                                subAsso.unsubscribe();
                                myBatch
                                  .commit()
                                  .then(() => {
                                    this.nzMessageService.create(
                                      'success',
                                      `Director de marketing asignado.`
                                    );
                                  })
                                  .catch((err) => {
                                    this.nzMessageService.create(
                                      'error',
                                      `Error actualizando director marketing.`
                                    );
                                  });
                              }
                            });
                          } else {
                            this.nzMessageService.create(
                              'error',
                              `El usuario no existe`
                            );
                          }
                        });
                    }*/
                    break;
                  default:
                    break;
                }
              });
            }
          });
        });
    }

    // evaluate marketing
    // --- ****************************** ---
    // cambiar cuando se cambie a la base de datos final el id de comparación
    // --- ****************************** ---
    if (formData['country'] == '' && formData['cityID'] == '') {
      let ranks = this.fireBase
        .doc(`ranks_dir/cFi4HSMwobtUChFso00n`)
        .get()
        .subscribe((data) => {
          if (
            data.data()['mrkDirectorUID'] == '' &&
            data.data()['mrkDirectorMail'] == ''
          ) {
            console.log(`could be mrk director`);
            // evaluate if user exist to set director
            let subAsso = this.fireBase
              .collection('associates', (ref) =>
                ref.where('email', '==', formData['email'])
              )
              .snapshotChanges()
              .subscribe((resuser) => {
                // evaluate if exist user with email
                if (resuser.length > 0) {
                  resuser.forEach((user) => {
                    // evaluate if user is already director
                    if (
                      user.payload.doc.data().hasOwnProperty('naldirectorOf') ||
                      user.payload.doc.data().hasOwnProperty('zndirectorOf') ||
                      user.payload.doc.data().hasOwnProperty('mkdirectorOf')
                    ) {
                      //  is national
                      if (
                        user.payload.doc.data().hasOwnProperty('naldirectorOf')
                      ) {
                        console.log(
                          `No podría ser director de marketing de ${
                            formData['country']['id']
                          } porque es director de -> ${
                            user.payload.doc.data()['naldirectorOf']
                          }--`
                        );
                        this.nzMessageService.create(
                          'error',
                          `No podría ser director de marketing de ${
                            formData['country']['id']
                          } porque es director de -> ${
                            user.payload.doc.data()['naldirectorOf']
                          }--`
                        );
                      }
                      // is zonal
                      if (
                        user.payload.doc.data().hasOwnProperty('zndirectorOf')
                      ) {
                        console.log(
                          `No podría ser director de marketing de ${
                            formData['country']['id']
                          } Ya es director de zona de -> ${
                            user.payload.doc.data()['znldirectorOf']
                          }`
                        );
                        this.nzMessageService.create(
                          'error',
                          `No podría ser director de marketing de ${
                            formData['country']['id']
                          } Ya es director de zona de -> ${
                            user.payload.doc.data()['znldirectorOf']
                          }`
                        );
                      }
                      // is marketing
                      if (
                        user.payload.doc.data().hasOwnProperty('mkdirectorOf')
                      ) {
                        console.log(
                          `No podría ser director de marketing de ${
                            formData['country']['id']
                          } ya es director de marketing de -> ${
                            user.payload.doc.data()['mkdirectorOf']
                          }`
                        );
                        this.nzMessageService.create(
                          'error',
                          `No podría ser director de marketing de ${
                            formData['country']['id']
                          } ya es director de marketing de -> ${
                            user.payload.doc.data()['mkdirectorOf']
                          }`
                        );
                      }
                    } else {
                      console.log(`Podría ser director de marketing`);
                      this.nzMessageService.create(
                        'error',
                        `Podría ser director de marketing `
                      );
                      // update data of asso to new new mrk director
                      const myBatch = this.fireBase.firestore.batch();
                      const updAsso = this.fireBase.doc(
                        `associates/${user.payload.doc.data()['fireId']}`
                      );
                      let walletID = this.fireBase.createId();
                      myBatch.update(updAsso.ref, {
                        mkdirectorOf: 'company',
                        walletMkDir: walletID,
                      });
                      // create wallet of director
                      const newWalletDoc = this.walletsCollection.doc(walletID);
                      let wMkDir = {
                        owner: user.payload.doc.data()['fireId'],
                        type: 8,
                        name: 'Director Marketing',
                        max_balance: 0,
                        current_balance: 0,
                        movementsList: '',
                        date: new Date(),
                      };
                      // --- ****************************** ---
                      // cambiar cuando se cambie a la base de datos final el id de comparación
                      // --- ****************************** ---
                      myBatch.set(newWalletDoc.ref, wMkDir);
                      // update country with new director data
                      const updContry = this.fireBase.doc(
                        `ranks_dir/cFi4HSMwobtUChFso00n`
                      );
                      myBatch.update(updContry.ref, {
                        mrkDirectorUID: user.payload.doc.data()['fireId'],
                        mrkDirectorMail: formData['email'],
                      });
                      //  send all tasks
                      ranks.unsubscribe();
                      subAsso.unsubscribe();
                      myBatch
                        .commit()
                        .then(() => {
                          this.nzMessageService.create(
                            'success',
                            `Director de marketing asignado.`
                          );
                        })
                        .catch((err) => {
                          console.log(err);
                          this.nzMessageService.create(
                            'error',
                            `Error actualizando director marketing.`
                          );
                        });
                    }
                  });
                } else {
                  this.nzMessageService.create('error', `El usuario no existe`);
                }
              });
          } else {
            console.log(
              `already exist mrk director -> ${data.data()['mrkDirectorMail']}`
            );
          }
        });
    }
    // evaluate zones
    console.log(typeof formData['country']);
    if (formData['country'] !== '' && formData['cityID'] !== '') {
      let subCountry = this.fireBase
        .collection(`countries/${formData['country']['id']}/cities`)
        .snapshotChanges()
        .subscribe((res) => {
          res.forEach((zone) => {
            // evaluate zone sended
            if (formData['cityID'] == zone.payload.doc.id) {
              // evaluate if has zone director
              Object.keys(zone.payload.doc.data()).forEach((key) => {
                switch (key) {
                  case `directormail`:
                    if (zone.payload.doc.data()[key] !== '') {
                      console.log(
                        `No tiene director -> ${zone.payload.doc.data()[key]}`
                      );
                      this.nzMessageService.create(
                        'error',
                        `No tiene director -> ${zone.payload.doc.data()[key]}`
                      );
                      console.log(
                        `No puede asignar director de zona, ${formData['country']['id']} ya tiene uno en ${formData['cityID']}`
                      );
                      this.nzMessageService.create(
                        'error',
                        `No puede asignar director de zona, ${formData['country']['id']} ya tiene uno en ${formData['cityID']}`
                      );
                    } else {
                      console.log(
                        `No tiene director de zona -> ${
                          zone.payload.doc.data()[key]
                        }`
                      );
                      this.nzMessageService.create(
                        'error',
                        `No tiene director de zona -> ${
                          zone.payload.doc.data()[key]
                        }`
                      );

                      // evaluate if user exist to set director
                      let subAsso = this.fireBase
                        .collection('associates', (ref) =>
                          ref.where('email', '==', formData['email'])
                        )
                        .snapshotChanges()
                        .subscribe((resuser) => {
                          // evaluate if exist user with email
                          if (resuser.length > 0) {
                            resuser.forEach((user) => {
                              // evaluate if already is director
                              if (
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('naldirectorOf') ||
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('zndirectorOf') ||
                                user.payload.doc
                                  .data()
                                  .hasOwnProperty('mkdirectorOf')
                              ) {
                                //  is national
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('naldirectorOf')
                                ) {
                                  console.log(
                                    `couldn't be zone of ${
                                      formData['cityID']
                                    } porque es director de -> ${
                                      user.payload.doc.data()['naldirectorOf']
                                    }--`
                                  );
                                  this.nzMessageService.create(
                                    'error',
                                    `couldn't be zone of ${
                                      formData['cityID']
                                    } porque es director de -> ${
                                      user.payload.doc.data()['naldirectorOf']
                                    }--`
                                  );
                                }
                                // is zonal
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('zndirectorOf')
                                ) {
                                  console.log(
                                    `couldn't be zonal director of ${
                                      formData['cityID']
                                    } Ya es director de zona de -> ${
                                      user.payload.doc.data()['znldirectorOf']
                                    }`
                                  );
                                  this.nzMessageService.create(
                                    'error',
                                    `couldn't be zonal director of ${
                                      formData['cityID']
                                    } Ya es director de zona de -> ${
                                      user.payload.doc.data()['znldirectorOf']
                                    }`
                                  );
                                }
                                // is marketing
                                if (
                                  user.payload.doc
                                    .data()
                                    .hasOwnProperty('mkdirectorOf')
                                ) {
                                  console.log(
                                    `couldn't be zonal director of ${
                                      formData['cityID']
                                    } ya es director de marketing de -> ${
                                      user.payload.doc.data()['mkdirectorOf']
                                    }`
                                  );
                                  this.nzMessageService.create(
                                    'error',
                                    `couldn't be zonal director of ${
                                      formData['cityID']
                                    } ya es director de marketing de -> ${
                                      user.payload.doc.data()['mkdirectorOf']
                                    }`
                                  );
                                }
                              } else {
                                console.log(
                                  `could be zonal director of ${formData['country']['id']} zone ${formData['cityID']} `
                                );
                                this.nzMessageService.create(
                                  'error',
                                  `could be zonal director of ${formData['country']['id']} zone ${formData['cityID']} `
                                );
                                // update data of asso to new new national director
                                const myBatch = this.fireBase.firestore.batch();
                                const updAsso = this.fireBase.doc(
                                  `associates/${
                                    user.payload.doc.data()['fireId']
                                  }`
                                );
                                let walletID = this.fireBase.createId();
                                myBatch.update(updAsso.ref, {
                                  zndirectorOf: formData['cityID'],
                                  walletZnDir: walletID,
                                });
                                // create wallet of director
                                const newWalletDoc = this.walletsCollection.doc(
                                  walletID
                                );
                                let wZnDir = {
                                  owner: user.payload.doc.data()['fireId'],
                                  type: 9,
                                  name: 'Director Zonal',
                                  max_balance: 0,
                                  current_balance: 0,
                                  movementsList: '',
                                  date: new Date(),
                                };
                                myBatch.set(newWalletDoc.ref, wZnDir);
                                // update city with new director data
                                const updCity = this.fireBase.doc(
                                  `countries/${formData['country']['id']}/cities/${formData['cityID']}`
                                );
                                myBatch.update(updCity.ref, {
                                  directorUID: user.payload.doc.data()[
                                    'fireId'
                                  ],
                                  directormail: formData['email'],
                                });
                                //  send all tasks
                                subCountry.unsubscribe();
                                subAsso.unsubscribe();
                                myBatch
                                  .commit()
                                  .then(() => {
                                    // close subscription to countries
                                    this.nzMessageService.create(
                                      'success',
                                      `Director de zona asignado.`
                                    );
                                  })
                                  .catch((err) => {
                                    this.nzMessageService.create(
                                      'error',
                                      `Error actualizando director zonal.`
                                    );
                                  });
                              }
                            });
                          } else {
                            this.nzMessageService.create(
                              'error',
                              `El usuario no existe`
                            );
                          }
                        });
                    }
                    break;
                  default:
                    break;
                }
              });
            }
          });
        });
    }
  }
}
