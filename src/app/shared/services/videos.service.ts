import { Injectable } from '@angular/core';
// Services
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  constructor(private fireStServ: AngularFirestore) {

  }

  setVideo(objectVideo) {
    return this.fireStServ.collection('sch_vid').doc(this.fireStServ.createId()).set(objectVideo);
  }

  getListVideos(filter?) {
    if (filter) {
      return this.fireStServ.collection('sch_vid', ref => ref.where('position', '==', filter)).get();
    } else {
      return this.fireStServ.collection('sch_vid').get();
    }
  }

  deleteVideo(videoID) {
    return this.fireStServ.doc(`sch_vid/${videoID}`).delete();
  }
}
