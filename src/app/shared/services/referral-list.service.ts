import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
//Services
import { AssociateCRUDService } from '../services/associate-crud.service';

@Injectable({
  providedIn: 'root',
})
export class ReferralListService {
  constructor(
    private fireBase: AngularFirestore,
    private assoServ: AssociateCRUDService
  ) { }

  referListCollection = this.fireBase.collection('referrals_list');
  statusCreation$ = new Subject<number>();
  currentstatus = 0;
  setReferralList(newSimpleAssociateData: Object) {
    const getasso = this.assoServ
      .getAssociateData(newSimpleAssociateData['id_referral'])
      .get()
      .subscribe((res) => {
        let stringID = res.data()['referrals_list'];
        if (stringID !== undefined) {
          const myBatch = this.fireBase.firestore.batch();
          let newIdToInsert = this.fireBase.createId();
          const newSimpleAsso = this.referListCollection.doc(stringID).collection('my_referr').doc(newIdToInsert);
          myBatch.set(newSimpleAsso.ref, newSimpleAssociateData);
          const upDateComplexAsso = this.fireBase.doc(`associates/${newSimpleAssociateData['assoID']}`).ref;
          myBatch.update(upDateComplexAsso, { in_ref_list: newSimpleAsso.ref });
          myBatch.commit().then(res => {
            this.changeStatus(1);
            getasso.unsubscribe();
          }).catch(err => {
            console.error('Error added referral to list', err);
            this.changeStatus(2);
          });

          // this.referListCollection.doc(stringID).collection('my_referr').add(newSimpleAssociateData).then((res) => {
          //   console.log('lista agregada', res.path);
          //   this.changeStatus(1);
          //   getasso.unsubscribe();
          // })
          //   .catch((err) => {
          //     console.error('Error added referral to list', err);
          //     this.changeStatus(2);
          //   });
        }
      });
  }
  // update grandpas child field => haschilds to 1
  updateReferralList(listID: string, sonId: string) {
    this.referListCollection
      .doc(listID)
      .collection('my_referr', (ref) => ref.where('assoID', '==', sonId))
      .snapshotChanges()
      .subscribe((res) => {
        res.forEach((son) => {
          this.referListCollection
            .doc(listID)
            .collection('my_referr')
            .doc(son.payload.doc.id)
            .update({ haschilds: 1 })
            .then((res) => {
              this.changeStatus(3);
            })
            .catch((err) => {
              console.error('Error updating fathers list', err);
              this.changeStatus(2);
            });
        });
      });
  }

  changeStatus(status: number) {
    this.currentstatus = status;
    this.statusCreation$.next(this.currentstatus);
  }

  getStatusChanges$(): Observable<number> {
    return this.statusCreation$.asObservable();
  }
}
