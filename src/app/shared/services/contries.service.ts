import { Injectable } from '@angular/core';
//Firebase
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
// Models
import { City } from '../models/city.model';
import { Country } from '../models/country.model';

@Injectable({
  providedIn: 'root',
})
export class ContriesService {
  countries: Country[] = [];
  private countries$ = new Subject<Country[]>();
  cities: City[] = [];
  private cities$ = new Subject<City[]>();

  constructor(private fireBase: AngularFirestore) {}

  countriesList = this.fireBase.collection('countries');

  getContries() {
    return this.countriesList.snapshotChanges().subscribe((data) => {
      console.log(data);

      this.countries = this.countries.filter((a) => a.id == '');
      this.cities = this.cities.filter((a) => a.id == '');
      data.forEach((coun) => {
        // console.log(`country ${coun.payload.doc.data()['name']}`);
        let ccoun: Country = new Country();
        ccoun.id = coun.payload.doc.id;
        ccoun.name = coun.payload.doc.data()['name'];
        ccoun.natDirectorUID = coun.payload.doc.data()['natDirectorUID'];
        ccoun.natDirectorMail = coun.payload.doc.data()['natDirectorMail'];
        ccoun.mrkDirectorUID = coun.payload.doc.data()['mrkDirectorUID'];
        ccoun.mrkDirectorMail = coun.payload.doc.data()['mrkDirectorMail'];
        this.getCities(coun.payload.doc.id)
          .snapshotChanges()
          .subscribe((cidata) => {
            cidata.forEach((cit) => {
              let ccit: City = new City();
              ccit.fatherID = coun.payload.doc.id;
              ccit.id = cit.payload.doc.id;
              ccit.name = cit.payload.doc.data()['name'];
              ccit.directorUID = cit.payload.doc.data()['directorUID'];
              ccit.directormail = cit.payload.doc.data()['directormail'];
              // console.log(`name zone from service ${ccit.name} -> ${ccoun.name}`);
              this.cities.push(ccit);
              const seen = new Set();
              this.cities = this.cities.filter((el) => {
                const duplicate = seen.has(el.id);
                seen.add(el.name);
                return !duplicate;
              });
              // console.log(` cities length ${this.cities.length}`);
              this.cities$.next(this.cities);
            });
          });
        this.countries.push(ccoun);
        const seen = new Set();
        this.countries = this.countries.filter((el) => {
          const duplicate = seen.has(el.id);
          seen.add(el.name);
          return !duplicate;
        });
        this.countries$.next(this.countries);
      });
    });
  }

  getCountries$(): Observable<Country[]> {
    return this.countries$.asObservable();
  }

  getCities(countryID: string) {
    return this.fireBase
      .collection('countries')
      .doc(countryID)
      .collection('cities');
  }

  getCities$(): Observable<City[]> {
    return this.cities$.asObservable();
  }

  createCountry(newCountries: Country[]) {
    const myBatch = this.fireBase.firestore.batch();
    //  new clone object on JS
    newCountries.forEach((country) => {
      let newCountry = { ...country };
      const newCountryDoc = this.countriesList.doc(country.id);
      delete newCountry['id'];
      myBatch.set(newCountryDoc.ref, newCountry);
    });
    return myBatch.commit();
  }

  createCity(newCities: City[]) {
    const myBatch = this.fireBase.firestore.batch();
    newCities.forEach((city) => {
      //  new clone object on JS
      let newCity = { ...city };
      const newCityDoc = this.countriesList.doc(
        `${city.fatherID}/cities/${city.id}`
      );
      delete newCity['id'];
      myBatch.set(newCityDoc.ref, newCity);
    });
    return myBatch.commit();
  }
}
