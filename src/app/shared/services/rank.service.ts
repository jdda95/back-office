import { Injectable } from '@angular/core';
// Service
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';

@Injectable({
  providedIn: 'root',
})
export class RankService {
  constructor(private fireBase: AngularFirestore) {}

  getRanks() {
    return this.fireBase.collection('ranks', (ref) => ref.orderBy('pos'));
  }
  getRank(rankid) {
    return this.fireBase.collection('ranks').doc(rankid);
  }
}
