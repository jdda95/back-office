import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ThemeConstantService } from './services/theme-constant.service';
import { SearchPipe } from './pipes/search.pipe';
import { PhonePipe } from './pipes/phone.pipe';
import { FilterselectorsPipe } from './pipes/filterselectorscity.pipe';
import { FilteralieselectorPipe } from './pipes/filteralieselector.pipe';

@NgModule({
  exports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    NzIconModule,
    PerfectScrollbarModule,
    SearchPipe,
    PhonePipe,
    FilterselectorsPipe,
    FilteralieselectorPipe,
  ],
  imports: [RouterModule, CommonModule, NzIconModule, PerfectScrollbarModule],
  declarations: [
    SearchPipe,
    PhonePipe,
    FilterselectorsPipe,
    FilteralieselectorPipe,
  ],
  providers: [ThemeConstantService],
})
export class SharedModule {}
