import { Routes } from '@angular/router';
import { AdminGuard } from '../guards/admin.guard';
import { ResolveUser } from '../resolvers/user.resolver';

export const CommonLayout_ROUTES: Routes = [
  {
    path: 'dashboard',
    loadChildren: () =>
      import('../../dashboard/dashboard.module').then((m) => m.DashboardModule),
    canActivate: [AdminGuard],
    resolve: {
      user: ResolveUser,
    },
  },
  {
    path: 'dashboard-admin',
    loadChildren: () =>
      import('../../dashboard-admin/dashboard-admin.module').then((m) => m.DashboardAdminModule),
    canActivate: [AdminGuard],
    resolve: {
      user: ResolveUser,
    },
  },
  {
    path: 'billeteras',
    loadChildren: () =>
      import('../../wallets/wallets.module').then((m) => m.WalletsModule),
  },
  {
    path: 'perfil',
    loadChildren: () =>
      import('../../profile/profile.module').then((m) => m.ProfileModule),
  },
  {
    path: 'escuela-empresarios',
    loadChildren: () =>
      import('../../school/school.module').then((m) => m.SchoolModule),
  },
  {
    path: 'transacciones',
    loadChildren: () =>
      import('../../transactions/transactions.module').then(
        (m) => m.TransactionsModule
      ),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('../../admins-dashboard/admins-dashboard.module').then(
        (m) => m.AdminsDashboardModule
      ),
    canActivate: [AdminGuard],
  },
];
