export interface TreeNodeInterface {
  key?: number;
  date?: Date;
  assoID: string;
  name: string;
  nickname: string;
  phone: string;
  profilePic: string;
  rank_ID: string;
  referrals_list?: string;
  status: number | boolean;
  children?: TreeNodeInterface[];
  //Don't know what do
  expand: boolean;
  level?: number;
  parent?: TreeNodeInterface;
}
