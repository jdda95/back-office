import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { DashboardAdminRoutingModule } from "./dashboard-admin-routing.module";
import { DashboardAdminComponent } from './dashboard-admin.component';

import { ThemeConstantService } from '../shared/services/theme-constant.service';

/** Import any ng-zorro components as the module required except icon module */
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';

import { AuthenticationService } from "../shared/services/authentication.service";

const antdModule = [
    NzButtonModule,
    NzCardModule,
    NzTagModule,
    NzBadgeModule,
    NzProgressModule,
    NzAvatarModule,
    NzTableModule
];

@NgModule({
    imports: [
        SharedModule,
        DashboardAdminRoutingModule,
        ...antdModule
    ],
    exports: [],
    declarations: [
      DashboardAdminComponent
    ],
    providers: [
        ThemeConstantService,
        AuthenticationService
    ]
})
export class DashboardAdminModule { }

