import { Component, OnInit } from '@angular/core';
import { ThemeConstantService } from '../shared/services/theme-constant.service';
import { AuthenticationService } from '../shared/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
// services
import { AdminCrudService } from '../shared/services/admin-crud.service';
import { AssociateCRUDService } from '../shared/services/associate-crud.service';
import { AliesService } from '../shared/services/alies.service';
//  model
import { AdminCrud } from '../shared/models/admin-crud.model';
import { Wallet } from '../shared/models/wallet.model';
import { Alie } from '../shared/models/alie.model';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styles: [
    `
      h6.refer-name {
        white-space: nowrap;
        width: 100px;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    `,
  ],
})
export class DashboardAdminComponent implements OnInit {
  themeColors = this.colorConfig.get().colors;
  blue = this.themeColors.blue;
  volcano = this.themeColors.volcano;
  blueLight = this.themeColors.blueLight;
  cyan = this.themeColors.cyan;
  cyanLight = this.themeColors.cyanLight;
  gold = this.themeColors.gold;
  purple = this.themeColors.purple;
  purpleLight = this.themeColors.purpleLight;
  red = this.themeColors.red;
  orange = this.themeColors.orange;

  administrator$: Observable<AdminCrud>;
  administrator: AdminCrud;
  // wallet
  wallets: Wallet[] = [];
  wallets$: Observable<Wallet[]>;

  constructor(
    private activeRoute: ActivatedRoute,
    private colorConfig: ThemeConstantService,
    private authServ: AuthenticationService,
    private assoCRUDServ: AssociateCRUDService,
    private admCRUDServ: AdminCrudService,
    private router: Router,
    private allyServ: AliesService
  ) {
    // this.activeRoute.params.subscribe((params:Params)=> {
    //   console.log(`profile constr ${params.profile}`);
    // });
  }

  ngOnInit(): void {
    this.wallets = this.wallets.filter((wall) => wall.name == '');
    this.administrator$ = this.admCRUDServ.getAdmin$();
    this.administrator$.subscribe((newData) => {
      this.retrieveAdmin(newData);
    });
    this.wallets$ = this.assoCRUDServ.getAssociateWallets$();
    this.wallets$.subscribe((newData) => {
      this.wallets = newData;
      const seen = new Set();
      this.wallets = this.wallets.filter((el) => {
        const duplicate = seen.has(el.name);
        seen.add(el.name);
        return !duplicate;
      });
      this.getAlies();
      // this.retrieveWallets();
    });

    this.authServ.hasUserB().then((user) => {
      if (user !== null) {
        // console.log(`mail-> ${user.email} | uid-> ${user.uid}`);
        this.admCRUDServ.getAdministratorData(user.email);
        this.retrieveWallets(user.uid);
      } else {
        this.router.navigate(['auth/login']);
      }
    });
  }

  retrieveAdmin(newAdm: AdminCrud) {
    this.administrator = newAdm;
  }
  retrieveWallets(userID: string) {
    // console.log(this.assoCRUDServ.wallets.length);
    this.assoCRUDServ.getAssociateWallets(userID);
    // if (this.assoCRUDServ.wallets.length <= 0) {
    //   this.assoCRUDServ.getAssociateWallets(userID);
    // } else {
    //   this.wallets = this.assoCRUDServ.wallets;
    //   Object.values(this.wallets).forEach(value => {
    //     console.log(`values wallet ${value}`);
    //   });
    // }
  }

  getAlies() {
    this.allyServ
      .getAlies()
      .snapshotChanges()
      .subscribe((alies) => {
        alies.forEach((ally) => {
          let cuAlly: Wallet = new Wallet();
          cuAlly.owner = ally.payload.doc.id;
          cuAlly.current_balance = ally.payload.doc.data()['bill'];
          cuAlly.name = ally.payload.doc.data()['name'];
          this.wallets = [...this.wallets, cuAlly];
          // evaluate if already has this alie
          const seen = new Set();
          this.wallets = this.wallets.filter((el) => {
            const duplicate = seen.has(el.name);
            seen.add(el.name);
            return !duplicate;
          });
        });
      });
  }
}
