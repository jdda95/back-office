import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
// Services
import { AssociateCRUDService } from 'src/app/shared/services/associate-crud.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { MandrillService } from 'src/app/shared/services/mandrill.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { TokeAndMovementsService } from 'src/app/shared/services/toke-and-movements.service';
// Model
import { Wallet } from 'src/app/shared/models/wallet.model';
import { Token } from 'src/app/shared/models/token.model';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-request-payment',
  templateUrl: './request-payment.component.html',
  styles: [
    `
      i {
        cursor: pointer;
      }
    `,
  ],
})
export class RequestPaymentComponent implements OnInit {
  requestForm: FormGroup;
  tokenVisible = false;
  password: string;

  inputValue: string;
  filteredOptions: string[] = [];
  wallets$: Observable<Wallet[]>;
  userKind = 0;
  options: Wallet[] = [];
  maxVariable = 0;
  walletSelectedName: string;
  valuesChanges = false;

  addSubstract = 100;

  userID = '';
  userMail = '';

  constructor(
    private fb: FormBuilder,
    private mandrill: MandrillService,
    private assoCRUDServ: AssociateCRUDService,
    private fireBase: AngularFirestore,
    private authServ: AuthenticationService,
    private tokenAndReqServ: TokeAndMovementsService,
    private nzMessageService: NzMessageService
  ) {
    // this.filteredOptions = this.options;

    this.wallets$ = this.assoCRUDServ.getAssociateWallets$();
    this.wallets$.subscribe((newData) => {
      this.options = newData;
      this.filterWallets(newData);
    });
  }

  ngOnInit(): void {
    this.requestForm = this.fb.group({
      wallet: [null, [Validators.required]],
      mount: [null, [Validators.required]],
      token: [null, [Validators.required]],
    });

    let aux = true;
    this.authServ.hasUser().forEach((user) => {
      if (aux) {
        if (user !== null) {
          this.userID = user.uid;
          this.userMail = user.email;
          this.assoCRUDServ.getAssociateWallets(user.uid);
          user.getIdTokenResult().then((idTokenResult) => {
            switch (idTokenResult.claims.role) {
              case 1:
                console.log(`Operativo`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Operativo'];
                break;
              case 2:
                console.log(`Administrativo`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Administrativo'];
                break;
              case 3:
                console.log(`Directivo Relaciones públicas`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Relaciones públicas'];
                break;
              case 4:
                console.log(`Nacional`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Nacional', 'Membresías', 'Residual', 'Desarrollo'];
                break;
              case 5:
                console.log(`Zonal`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Zonal', 'Membresías', 'Residual', 'Desarrollo'];
                break;
              case 6:
                console.log(`marketing`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Marketing', 'Membresías', 'Residual', 'Desarrollo'];
                break;
              default:
                this.userKind = 0;
                // this.options = ['Membresías', 'Residual', 'Desarrollo'];
                break;
            }
          });
        } else {
          console.log('empty user');
        }
        aux = false;
      }
    });
  }

  // onChange(value: string): void {
  //   this.filteredOptions = this.options.filter(
  //     (option) => option.toLowerCase().indexOf(value.toLowerCase()) !== -1
  //   );
  // }

  submitForm(): void {
    for (const i of Object.keys(this.requestForm.controls)) {
      this.requestForm.controls[i].markAsDirty();
      this.requestForm.controls[i].updateValueAndValidity();
    }
    let movementObj = {
      mount: this.requestForm.value['mount'],
      token: this.requestForm.value['token'],
      user1: this.userID,
      user1email: this.userMail,
      user2: '',
      walletID1: this.requestForm.value['wallet'],
      walletID2: '',
    };
    this.tokenAndReqServ.evaluateToken(movementObj, 1).then((res) => {
      if (res === 'Solicitud realizada satisfactoriamente') {
        this.mandrill
          .sendMonetizacion({
            USER: this.userMail,
            VALUE: this.requestForm.controls.mount.value,
            WALLET: this.walletSelectedName,
          })
          .subscribe((res) => console.log(res));
      } else if (
        res === 'Error generando la soli favor intentelo más tarde nuevamente'
      ) {
        this.nzMessageService.create(
          'error',
          `Error generando la solicitud favor intentelo más tarde nuevamente`
        );
      } else if (
        res ===
        'Error en la solicitud el token puede estar vencido o ya fue usado en otra operación, solicite uno nuevamente'
      ) {
        this.nzMessageService.create(
          'error',
          `Error en la solicitud el token puede estar vencido o ya fue usado en otra operación, solicite uno nuevamente`
        );
      }
    });
  }
  reqCode(event) {
    let newCode: Token = new Token();
    newCode.fireID = this.fireBase.createId();
    newCode.date = new Date();
    newCode.status = true;
    newCode.type = 1;
    newCode.userID = this.userID;
    this.tokenAndReqServ.createToken(newCode).then((res) => {
      this.mandrill
        .sendTokeEmail(this.userMail, 'token - ' + this.userMail, {
          token: `${newCode}`,
        })
        .subscribe((res) => {
          this.nzMessageService.create(
            'success',
            `Token generado y envíado al correo ${this.userMail}`
          );
        });
    });
  }
  filterWallets(wallets) {
    this.options = wallets;
    switch (this.userKind) {
      case 1:
        this.options = this.options.filter((w) => w.type == 12);
        this.addSubstract = 0;
        break;
      case 2:
        this.options = this.options.filter((w) => w.type == 13);
        this.addSubstract = 0;
        break;
      case 3:
        this.options = this.options.filter((w) => w.type == 14);
        this.addSubstract = 0;
        break;
      case 4:
        this.options = this.options.filter((w) => {
          if (
            (w.current_balance > 100 && w.type == 3) ||
            (w.current_balance > 100 && w.type == 4) ||
            (w.current_balance > 100 && w.type == 5) ||
            w.type == 6
          ) {
            return w;
          }
        });
        break;
      case 5:
        this.options = this.options.filter((w) => {
          if (
            (w.current_balance > 100 && w.type == 3) ||
            (w.current_balance > 100 && w.type == 4) ||
            (w.current_balance > 100 && w.type == 5) ||
            w.type == 9
          ) {
            return w;
          }
        });
        break;
      case 6:
        this.options = this.options.filter((w) => {
          if (
            (w.current_balance > 100 && w.type == 3) ||
            (w.current_balance > 100 && w.type == 4) ||
            (w.current_balance > 100 && w.type == 5) ||
            w.type == 8
          ) {
            return w;
          }
        });
        break;
      case 0:
        this.options = this.options.filter((w) => {
          if (
            (w.current_balance > 100 && w.type == 3) ||
            (w.current_balance > 100 && w.type == 4) ||
            (w.current_balance > 100 && w.type == 5)
          ) {
            return w;
          }
        });
        break;
      default:
        break;
    }
    if (this.options.length <= 0) {
      console.log(
        'no posee el saldo suficiente en las billeteras para realizar una solicitud'
      );
    } else {
      this.onChanges();
    }
  }

  updateMax(currentValue) {
    let selectBalance = this.options.filter(
      (wallet) => wallet.fireID == currentValue
    );
    this.walletSelectedName = selectBalance[0].name;
    this.maxVariable = selectBalance[0].current_balance - this.addSubstract;
  }
  onChanges(): void {
    this.requestForm.valueChanges.subscribe((val) => {
      this.valuesChanges = true;
    });
  }
}
