import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestPaymentComponent } from './request-payment/request-payment.component';
import { TransferUsersComponent } from './transfer-users/transfer-users.component';


const routes: Routes = [
  {
    path: 'solicitar',
    component: RequestPaymentComponent,
    data: {
      title: 'Solicitar Pago'
    }
  },
  {
    path: 'transferir',
    component: TransferUsersComponent,
    data: {
      title: 'Transferir dinero'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionsRoutingModule { }
