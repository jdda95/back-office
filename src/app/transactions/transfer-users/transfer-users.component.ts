import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
//Services
import { AngularFirestore } from '@angular/fire/firestore';
import { AssociateCRUDService } from 'src/app/shared/services/associate-crud.service';
import { MandrillService } from 'src/app/shared/services/mandrill.service';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { NzMessageService } from 'ng-zorro-antd';
import { TokeAndMovementsService } from 'src/app/shared/services/toke-and-movements.service';
//  Model
import { Wallet } from '../../shared/models/wallet.model';
import { Token } from 'src/app/shared/models/token.model';

@Component({
  selector: 'app-transfer-users',
  templateUrl: './transfer-users.component.html',
  styles: [
    `
      i {
        cursor: pointer;
      }
    `,
  ],
})
export class TransferUsersComponent implements OnInit {
  requestForm: FormGroup;
  tokenVisible = false;
  password: string;

  inputValue: string;
  filteredOptions: Wallet[] = [];
  options: Wallet[] = [];
  wallets$: Observable<Wallet[]>;
  // options = ['Membresias', 'Desarrollo', 'Residual'];
  userKind = 0;
  maxVariable = 0;
  valuesChanges = false;

  userID = '';
  userMail = '';
  userWallets = [];
  // receiver user
  walletSelectedName: string;
  user2ID = '';
  user2Mail = '';
  user2wallets = [];

  addSubstract = 100;

  selectedValue = null;
  listOfOption: Array<{ value: string; text: string }> = [];
  nzFilterOption = () => true;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private mandrill: MandrillService,
    private assoCRUDServ: AssociateCRUDService,
    private authServ: AuthenticationService,
    private fireBase: AngularFirestore,
    private tokenAndReqServ: TokeAndMovementsService,
    private nzMessageService: NzMessageService
  ) {
    this.filteredOptions = this.options;
  }

  ngOnInit(): void {
    this.authServ.hasUserB().then((user) => {
      if (user !== null) {
        this.assoCRUDServ.getAssociateWallets(user.uid);
      }
    });

    this.requestForm = this.fb.group({
      wallet: [null, [Validators.required]],
      mount: [null, [Validators.required]],
      token: [null, [Validators.required]],
      user: [null, [Validators.required]],
    });

    this.wallets$ = this.assoCRUDServ.getAssociateWallets$();
    this.wallets$.subscribe((newData) => {
      this.filterWallets(newData);
    });

    let aux = true;
    this.authServ.hasUser().forEach((user) => {
      if (aux) {
        if (user !== null) {
          this.userID = user.uid;
          this.userMail = user.email;
          this.assoCRUDServ.getAssociateWallets(user.uid);
          user.getIdTokenResult().then((idTokenResult) => {
            switch (idTokenResult.claims.role) {
              case 1:
                console.log(`Operativo`);
                this.userKind = idTokenResult.claims.role;
                this.addSubstract = 0;
                // this.options = ['Dir. Operativo'];
                break;
              case 2:
                console.log(`Administrativo`);
                this.userKind = idTokenResult.claims.role;
                this.addSubstract = 0;
                // this.options = ['Dir. Administrativo'];
                break;
              case 3:
                console.log(`Directivo Relaciones públicas`);
                this.userKind = idTokenResult.claims.role;
                this.addSubstract = 0;
                // this.options = ['Dir. Relaciones públicas'];
                break;
              case 4:
                console.log(`Nacional`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Nacional', 'Membresías', 'Residual', 'Desarrollo'];
                break;
              case 5:
                console.log(`Zonal`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Zonal', 'Membresías', 'Residual', 'Desarrollo'];
                break;
              case 6:
                console.log(`marketing`);
                this.userKind = idTokenResult.claims.role;
                // this.options = ['Dir. Marketing', 'Membresías', 'Residual', 'Desarrollo'];
                break;
              default:
                this.userKind = 0;
                // this.options = ['Membresías', 'Residual', 'Desarrollo'];
                break;
            }
          });
        } else {
          console.log('empty user');
        }
        aux = false;
      }
    });
  }

  onChange(value: string): void {
    this.filteredOptions = this.options.filter(
      (option) => option.name.toLowerCase().indexOf(value.toLowerCase()) !== -1
    );
  }

  submitForm(): void {
    for (const i of Object.keys(this.requestForm.controls)) {
      this.requestForm.controls[i].markAsDirty();
      this.requestForm.controls[i].updateValueAndValidity();
    }
    console.log(this.requestForm.value);
    let wallSelected = this.userWallets.find(
      (wall) => wall['id'] === this.requestForm.value['wallet']
    );
    let walletReceiver = this.user2wallets.find(
      (wall) => wall['type'] === wallSelected['type']
    );
    console.log(walletReceiver);
    let movementObj = {
      mount: this.requestForm.value['mount'],
      token: this.requestForm.value['token'],
      user1: this.userID,
      user1email: this.userMail,
      walletID1: this.requestForm.value['wallet'],
      user2: this.requestForm.value['user'],
      user2email: this.user2Mail,
      walletID2: walletReceiver['id'],
    };
    this.tokenAndReqServ.evaluateToken(movementObj, 2).then((res) => {
      console.log(res);
      if (res === 'Solicitud realizada satisfactoriamente') {
        this.mandrill
          .sendTransaction(
            `${this.user2Mail}`,
            `Nueva transferencia de ${this.userMail}`,
            {
              USERSEND: this.user2Mail,
              VALUE: this.requestForm.controls.mount.value,
              WALLET: this.walletSelectedName,
            }
          )
          .subscribe((res) => {
            if (res[0].status === 'sent') {
              this.nzMessageService.create(
                'success',
                `Transferencia realizada.`
              );
            } else {
              this.nzMessageService.create(
                'error',
                `La transferencia no pudo ser realizada, intentalo más tarde.`
              );
            }
          });
      } else if (
        res === 'Error generando la soli favor intentelo más tarde nuevamente'
      ) {
        this.nzMessageService.create(
          'error',
          `Error generando la soli favor intentelo más tarde nuevamente`
        );
      } else if (
        res ===
        'Error en la solicitud el token puede estar vencido o ya fue usado en otra operación, solicite uno nuevamente'
      ) {
        this.nzMessageService.create(
          'error',
          `Error en la solicitud el token puede estar vencido o ya fue usado en otra operación, solicite uno nuevamente`
        );
      }
    });
  }

  search(value: string): void {
    this.listOfOption = this.listOfOption.filter((usr) => usr.value === '');
    if (value !== this.userMail) {
      let cuUser = this.fireBase
        .collection(`associates`, (ref) => ref.where(`email`, '==', value))
        .get();
      if (cuUser !== undefined) {
        cuUser.forEach((doc) => {
          doc.forEach((user) => {
            // console.log(user.data());
            this.user2wallets = this.user2wallets.filter(
              (wallet) => wallet['type'] === 0
            );
            this.user2ID = user.id;
            this.user2Mail = user.data()['email'];
            this.user2wallets.push({
              type: 3,
              id: user.data()['walletMembership'],
            });
            this.user2wallets.push({
              type: 4,
              id: user.data()['walletResidual'],
            });
            this.user2wallets.push({
              type: 5,
              id: user.data()['walletDevelopment'],
            });
            console.log(this.user2wallets);
            const listOfOption: Array<{ value: string; text: string }> = [];
            listOfOption.push({
              value: user.data()['fireId'],
              text: user.data()['email'],
            });
            this.listOfOption = listOfOption;
          });
        });
      } else {
        console.log('no user find');
      }
    }
  }

  evalamount(event) {
    console.log(event);
  }

  filterWallets(wallets) {
    this.options = this.options.filter((wallet) => wallet.owner === '');
    this.options = wallets;
    switch (this.userKind) {
      case 4:
        this.options = this.options.filter((w) => {
          if (
            (w.current_balance > 100 && w.type == 3) ||
            (w.current_balance > 100 && w.type == 4) ||
            (w.current_balance > 100 && w.type == 5) /*|| w.type == 6*/
          ) {
            this.userWallets.push({ type: w.type, id: w.fireID });
            return w;
          }
        });
        break;
      case 5:
        this.options = this.options.filter((w) => {
          if (
            (w.current_balance > 100 && w.type == 3) ||
            (w.current_balance > 100 && w.type == 4) ||
            (w.current_balance > 100 && w.type == 5) /*|| w.type == 9*/
          ) {
            this.userWallets.push({ type: w.type, id: w.fireID });
            return w;
          }
        });
        break;
      case 6:
        this.options = this.options.filter((w) => {
          if (
            (w.current_balance > 100 && w.type == 3) ||
            (w.current_balance > 100 && w.type == 4) ||
            (w.current_balance > 100 && w.type == 5) /* || w.type == 8*/
          ) {
            this.userWallets.push({ type: w.type, id: w.fireID });
            return w;
          }
        });
        break;
      case 0:
        this.options = this.options.filter((w) => {
          if (
            (w.current_balance > 100 && w.type == 3) ||
            (w.current_balance > 100 && w.type == 4) ||
            (w.current_balance > 100 && w.type == 5)
          ) {
            this.userWallets.push({ type: w.type, id: w.fireID });
            return w;
          }
        });
        break;
      default:
        break;
    }
    if (this.options.length <= 0) {
      console.log(
        'no posee el saldo suficiente en las billeteras para realizar una solicitud'
      );
    } else {
      this.onChanges();
    }
  }

  reqCode(event) {
    let newCode: Token = new Token();
    newCode.fireID = this.fireBase.createId();
    newCode.date = new Date();
    newCode.status = true;
    newCode.type = 2;
    newCode.userID = this.userID;
    this.tokenAndReqServ.createToken(newCode).then((res) => {
      this.mandrill
        .sendTokeEmail(this.userMail, 'Token - ' + this.userMail, {
          token: `${newCode}`,
        })
        .subscribe((res) => {
          console.log(res);
          if (res[0].status === 'sent') {
            this.nzMessageService.create(
              'success',
              `Token generado y envíado al correo ${this.userMail}`
            );
          } else {
            this.nzMessageService.create(
              'error',
              `El TOKEN no pudo ser envíado al correo ${this.userMail}, intentalo más tarde.`
            );
          }
        });
    });
  }

  updateMax(currentValue) {
    let selectBalance = this.options.filter(
      (wallet) => wallet.fireID == currentValue
    );
    this.walletSelectedName = selectBalance[0].name;
    this.maxVariable = selectBalance[0].current_balance - this.addSubstract;
  }

  onChanges(): void {
    this.requestForm.valueChanges.subscribe((val) => {
      this.valuesChanges = true;
    });
  }
}
