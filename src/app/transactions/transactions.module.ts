import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzIconModule } from 'ng-zorro-antd';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';

import { ReactiveFormsModule } from '@angular/forms';
import { RequestPaymentComponent } from './request-payment/request-payment.component';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransferUsersComponent } from './transfer-users/transfer-users.component';

const antdModule = [
  NzFormModule,
  NzInputModule,
  NzButtonModule,
  NzCardModule,
  NzCheckboxModule,
  NzToolTipModule,
  NzSelectModule,
  NzIconModule,
  NzAutocompleteModule,
  NzInputNumberModule,
];

@NgModule({
  declarations: [TransferUsersComponent, RequestPaymentComponent],
  imports: [
    CommonModule,
    TransactionsRoutingModule,
    ReactiveFormsModule,
    ...antdModule,
  ],
})
export class TransactionsModule {}
