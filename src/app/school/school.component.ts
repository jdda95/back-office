import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Observable } from 'rxjs';
//  Services
import { VideosService } from '../shared/services/videos.service';
import { AssociateCRUDService } from '../shared/services/associate-crud.service';
import { AuthenticationService } from '../shared/services/authentication.service';
//  Models
import { Video } from '../shared/models/video.model';
import { Associate } from '../shared/models/associate.model';
@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  styles: [
    `
      .videos button {
        background: transparent;
        border: 0;
        cursor: pointer;
        outline: none;
      }
      #player {
        width: 100%;
        height: 480px;
        pointer-events: none;
      }
      input[type='range'] {
        margin-right: 10px;
      }
    `,
  ],
})
export class SchoolComponent implements OnInit {
  youtubeLink = '';
  isVisibleTop = false;
  isVisibleMiddle = false;
  done = false;
  showVideo = false;
  // VIDEO STATE
  playing = false;
  titleVideo: string;
  muted = false;
  @ViewChild('volume ') volume: ElementRef;
  public innerWidth: any;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = event.target.innerWidth;
  }

  /* 1. Some required variables which will be used by YT API*/
  public YT: any;
  public video: any;
  public player: any;
  public reframed: Boolean = false;

  isRestricted = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
  @HostListener('contextmenu', ['$event'])
  onRightClick(event) {
    event.preventDefault();
  }

  /* 2. Initialize method for YT IFrame API */
  init() {
    // Return if Player is already created
    if (window['YT']) {
      this.startVideo();
      return;
    }

    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    /* 3. startVideo() will create an <iframe> (and YouTube player) after the API code downloads. */
    window['onYouTubeIframeAPIReady'] = () => this.startVideo();
  }
  constructor(
    private videoServ: VideosService,
    private assoServ: AssociateCRUDService,
    private authserv: AuthenticationService
  ) { }

  listVideos: Video[] = [];
  associate: Associate = new Associate();
  associate$: Observable<Associate>;

  ngOnInit(): void {
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    document.body.appendChild(tag);
    this.init();
    this.innerWidth = window.innerWidth;

    this.associate$ = this.assoServ.getAssociate$();
    this.associate$.subscribe((newData) => {
      this.associate = newData;
      this.getVideoListForUser(this.associate);
    });

    let aux = true;
    this.authserv.hasUser().forEach((user) => {
      if (aux) {
        if (user !== null) {
          user.uid;
          if (this.assoServ.associate) {
            this.associate = this.assoServ.associate;
          } else {
            this.assoServ.getAssociateData(user.uid);
          }
        }
      }
    });
  }

  getVideoListForUser(userData: Associate) {
    if (userData) {
      this.videoServ.getListVideos().subscribe((res) => {
        res.docs.forEach((video) => {
          let cuVideo: Video = new Video();
          cuVideo.fireID = video.id;
          Object.keys(video.data()).forEach((key) => {
            cuVideo[key] = video.data()[key];
          });
          this.listVideos = [...this.listVideos, cuVideo];
        });
        this.listVideos = this.listVideos.filter(
          (vid) => vid.position === userData.rankID
        );
        const seen = new Set();
        this.listVideos = this.listVideos.filter((el) => {
          const duplicate = seen.has(el.fireID);
          seen.add(el.fireID);
          return !duplicate;
        });
      });
    }
  }
  showModalMiddle(youtubeLink, videoTitle): void {
    this.titleVideo = videoTitle;
    this.isVisibleMiddle = true;
    let a: Array<string> = this.dividirCadena(youtubeLink, '/');
    this.video = a[a.length - 1];
    if (this.showVideo) this.showVideo = false;
    else {
      this.showVideo = true;
      setTimeout(() => {
        this.init();
      });
    }
  }

  dividirCadena(cadenaADividir, separador) {
    var arrayDeCadenas = cadenaADividir.split(separador);
    return arrayDeCadenas;
  }

  playYTVideo(event) {
    event.target.playVideo();
  }

  handleCancel(): void {
    this.isVisibleMiddle = false;
  }

  startVideo() {
    this.reframed = false;
    this.player = new window['YT'].Player('player', {
      videoId: this.video,
      playerVars: {
        autoplay: 1,
        modestbranding: 0,
        controls: 0,
        disablekb: 0,
        rel: 0,
        showinfo: 0,
        fs: 0,
        playsinline: 0,
        origin: 'http://localhost:4200/',
      },
      events: {
        onStateChange: this.onPlayerStateChange.bind(this),
        onError: this.onPlayerError.bind(this),
        onReady: this.onPlayerReady.bind(this),
      },
    });
  }

  /* 4. It will be called when the Video Player is ready */
  onPlayerReady(event) {
    if (this.isRestricted) {
      event.target.mute();
      event.target.playVideo();
    } else {
      event.target.playVideo();
    }
  }

  /* 5. API will call this function when Player State changes like PLAYING, PAUSED, ENDED */
  onPlayerStateChange(event) {
    switch (event.data) {
      case window['YT'].PlayerState.PLAYING:
        if (this.cleanTime() == 0) {
          this.playing = true;
        } else {
          this.playing = true;
        }
        break;
      case window['YT'].PlayerState.PAUSED:
        if (this.player.getDuration() - this.player.getCurrentTime() != 0) {
          this.playing = false;
        }
        break;
      case window['YT'].PlayerState.ENDED:
        break;
    }
  }

  cleanTime() {
    return Math.round(this.player.getCurrentTime());
  }

  onPlayerError(event) {
    switch (event.data) {
      case 2:
        break;
      case 100:
        break;
      case 101 || 150:
        break;
    }
  }

  playVideo() {
    this.player.playVideo();
  }
  pauseVideo() {
    this.player.pauseVideo();
  }

  updateVolume(): void {
    this.player.setVolume(this.volume.nativeElement.value);
  }
}
