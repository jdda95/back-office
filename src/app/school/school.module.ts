import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SchoolRoutingModule } from './school-routing.module';
import { SchoolComponent } from './school.component';
import { SharedModule } from '../shared/shared.module';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { ComponentsModule } from '../components/components.module';
import { NzGridModule, NzModalModule } from 'ng-zorro-antd';

import { YouTubePlayerModule } from '@angular/youtube-player';

const antdModule = [
  NzButtonModule,
  NzCardModule,
  NzTagModule,
  NzBadgeModule,
  NzProgressModule,
  NzAvatarModule,
  NzTableModule,
  NzModalModule,
  NzGridModule,
];

@NgModule({
  declarations: [SchoolComponent],
  imports: [
    CommonModule,
    SchoolRoutingModule,
    SharedModule,
    antdModule,
    ComponentsModule,
    YouTubePlayerModule,
  ],
})
export class SchoolModule {}
