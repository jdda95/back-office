import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
// Services
import { ContriesService } from '../../shared/services/contries.service';
import { AliesService } from '../../shared/services/alies.service';
// Model
import { Country } from '../../shared/models/country.model';
import { Observable } from 'rxjs';
import { Alie } from 'src/app/shared/models/alie.model';

@Component({
  selector: 'app-upload-allies',
  templateUrl: './upload-allies.component.html',
  styles: [
    `
      nz-select {
        width: 45%;
      }
      .dynamic-delete-button {
        cursor: pointer;
        position: relative;
        top: 4px;
        font-size: 24px;
        color: #999;
        transition: all 0.3s;
      }

      .dynamic-delete-button:hover {
        color: #777;
      }

      .passenger-input {
        width: 45%;
        margin-right: 12px;
      }

      [nz-form] {
        max-width: 1200px;
      }

      .add-button {
        width: 60%;
      }
    `,
  ],
})
export class UploadAlliesComponent implements OnInit {
  selectedValue = null;
  validateForm: FormGroup;
  listOfControl: Array<{
    id: number;
    controlInstance: string;
    country: string;
    rate: string;
  }> = [];
  countries$: Observable<Country[]>;
  countries: Country[];
  // notification variables
  successmsg: boolean = false;
  errormsg: boolean = false;

  constructor(
    private fb: FormBuilder,
    private countriesServ: ContriesService,
    private alieServ: AliesService
  ) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({});
    this.addField();
    this.countries$ = this.countriesServ.getCountries$();
    this.countries$.subscribe((countries) => {
      this.setCountriesList(countries);
    });
    this.setCountriesList();
  }

  addField(e?: MouseEvent): void {
    if (e) {
      e.preventDefault();
    }
    const id =
      this.listOfControl.length > 0
        ? this.listOfControl[this.listOfControl.length - 1].id + 1
        : 0;

    const control = {
      id,
      controlInstance: `name${id}`,
      country: `countryID${id}`,
      rate: `rate${id}`,
    };
    const index = this.listOfControl.push(control);
    // console.log(this.listOfControl[this.listOfControl.length - 1]);
    this.validateForm.addControl(
      this.listOfControl[index - 1].controlInstance,
      new FormControl(null, Validators.required)
    );
    this.validateForm.addControl(
      this.listOfControl[index - 1].country,
      new FormControl(null, Validators.required)
    );
    this.validateForm.addControl(
      this.listOfControl[index - 1].rate,
      new FormControl(null, Validators.required)
    );
  }

  removeField(
    i: { id: number; controlInstance: string; country: string; rate: string },
    e: MouseEvent
  ): void {
    e.preventDefault();
    if (this.listOfControl.length > 1) {
      const index = this.listOfControl.indexOf(i);
      this.listOfControl.splice(index, 1);
      // console.log(this.listOfControl);
      this.validateForm.removeControl(i.controlInstance);
      this.validateForm.removeControl(i.country);
      this.validateForm.removeControl(i.rate);
    }
  }

  submitForm(): void {
    let alies: Alie[] = [];
    let sliptCounter = 0;
    let alieCounter = 0;
    let alie: Alie = new Alie();
    for (const i of Object.keys(this.validateForm.controls)) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (this.validateForm.valid) {
      console.log(this.validateForm.value);
      Object.keys(this.validateForm.value).forEach((key) => {
        switch (key) {
          case `name${alieCounter}`:
            alie.name = this.validateForm.value[key];
            break;
          case `countryID${alieCounter}`:
            alie.countryID = this.validateForm.value[key];
            break;
          case `rate${alieCounter}`:
            alie.rate = this.validateForm.value[key];
            alie.bill = 0;
            break;
          default:
            break;
        }
        if (sliptCounter == 2) {
          sliptCounter = 0;
          alieCounter++;
          alies.push(alie);
          alie = new Alie();
        } else {
          sliptCounter++;
        }
      });
      this.alieServ
        .setAlies(alies)
        .then(() => (this.successmsg = true))
        .catch((err) => (this.errormsg = true));
    }
  }

  setCountriesList(countries?: Country[]) {
    if (this.countriesServ.countries.length > 0) {
      this.countries = this.countriesServ.countries;
    } else {
      this.countriesServ.getContries();
    }
  }
}
