import { Component, OnInit } from '@angular/core';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
  NzTableSortOrder,
} from 'ng-zorro-antd/table';
// Services
import { AdminCrudService } from '../../shared/services/admin-crud.service';
// Model
import { AdminCrud } from '../../shared/models/admin-crud.model';

interface ColumnItem {
  name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn | null;
  filterFn: NzTableFilterFn | null;
  filterMultiple: boolean;
  sortDirections: NzTableSortOrder[];
}

@Component({
  selector: 'app-admin-users-list',
  templateUrl: './admin-users-list.component.html',
  styles: [],
})
export class AdminUsersListComponent implements OnInit {
  listOfData: AdminCrud[] = [];

  constructor(private adminServ: AdminCrudService) {}

  ngOnInit(): void {
    this.getAdminlist();
  }

  getAdminlist() {
    this.listOfData = this.listOfData.filter((d) => d.email == '');
    this.adminServ.getAdministratorsList().subscribe((administrators) => {
      if (administrators.length > 0) {
        administrators.forEach((admin) => {
          let newAdmins = new AdminCrud();
          Object.keys(admin.payload.doc.data()).forEach((key) => {
            newAdmins[key] = admin.payload.doc.data()[key];
          });
          this.listOfData = [...this.listOfData, newAdmins];
          // evaluate if already has object duplicate and eliminate
          const seen = new Set();
          this.listOfData = this.listOfData.filter((el) => {
            const duplicate = seen.has(el.email);
            seen.add(el.email);
            return !duplicate;
          });
        });
      }
    });
    this.adminServ.getMarketingDir().subscribe((mrk) => {
      let newAdmins = new AdminCrud();
      newAdmins.email = mrk.data()['mrkDirectorMail'];
      newAdmins.rankName = mrk.data()['name'];
      this.listOfData = [...this.listOfData, newAdmins];
      const seen = new Set();
      this.listOfData = this.listOfData.filter((el) => {
        const duplicate = seen.has(el.email);
        seen.add(el.email);
        return !duplicate;
      });
    });
  }

  listOfColumns: ColumnItem[] = [
    {
      name: 'Rol',
      sortOrder: null,
      sortFn: (a: AdminCrud, b: AdminCrud) =>
        a.rankName.localeCompare(b.rankName),
      sortDirections: ['ascend', 'descend', null],
      filterMultiple: true,
      filterFn: (name: string, item: AdminCrud) =>
        item.rankName.indexOf(name) !== -1,
    },
    {
      name: 'Correo',
      sortOrder: 'descend',
      sortFn: (a: AdminCrud, b: AdminCrud) => a.email.localeCompare(b.email),
      sortDirections: ['descend', null],
      filterFn: null,
      filterMultiple: true,
    },
  ];
  // listOfData: AdminCrud[] = [
  //   {
  //     position: '1',
  //     mail: 'mail@mail.com',
  //     name: 'Super administrador'
  //   },
  //   {
  //     position: '2',
  //     mail: 'mail@mail.com',
  //     name: 'administrador'
  //   },
  //   {
  //     position: '3',
  //     mail: 'mail@mail.com',
  //     name:'fianaciero'
  //   }
  // ];
}
