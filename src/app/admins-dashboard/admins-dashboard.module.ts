import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { NZ_CONFIG } from 'ng-zorro-antd/core/config';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import {
  NzFormModule,
  NzCardModule,
  NzSwitchModule,
  NzDropDownModule,
  NzRadioGroupComponent,
  NzRadioModule,
} from 'ng-zorro-antd';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzI18nModule } from 'ng-zorro-antd/i18n';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzAlertModule } from 'ng-zorro-antd/alert';

import { IconDefinition } from '@ant-design/icons-angular';
import { LeftOutline, RightOutline } from '@ant-design/icons-angular/icons';

import { AdminsDashboardRoutingModule } from './admins-dashboard-routing.module';
import { AlliesComponent } from './allies/allies.component';
import { InfoGeneralComponent } from './info-general/info-general.component';
import { SchoolVideosComponent } from './school-videos/school-videos.component';
import { UploadAlliesComponent } from './upload-allies/upload-allies.component';
import { UploadVideosComponent } from './upload-videos/upload-videos.component';
import { UsersListComponent } from './users-list/users-list.component';
import { SharedModule } from '../shared/shared.module';
import { CountriesComponent } from './countries/countries.component';
import { AlliesListComponent } from './allies-list/allies-list.component';
import { AdminUsersListComponent } from './admin-users-list/admin-users-list.component';
import { AddAdminUsersComponent } from './add-admin-users/add-admin-users.component';
import { ListCountriesComponent } from './list-countries/list-countries.component';
import { ListCitiesComponent } from './list-cities/list-cities.component';
import { AddCitiesComponent } from './add-cities/add-cities.component';
import { CreateDirectorComponent } from './create-director/create-director.component';
import { ListDirectorsComponent } from './list-directors/list-directors.component';
import { FinanRequestComponent } from './finan-request/finan-request.component';

const icons: IconDefinition[] = [LeftOutline, RightOutline];
const antdModule = [
  NzAffixModule,
  NzAlertModule,
  NzAvatarModule,
  NzBadgeModule,
  NzButtonModule,
  NzCardModule,
  NzCheckboxModule,
  NzDividerModule,
  NzDropDownModule,
  NzFormModule,
  NzGridModule,
  NzI18nModule,
  NzIconModule.forRoot(icons),
  NzInputModule,
  NzInputNumberModule,
  NzMenuModule,
  NzMessageModule,
  NzPopconfirmModule,
  NzPopoverModule,
  NzRadioModule,
  NzSelectModule,
  NzSwitchModule,
  NzTableModule,
  NzToolTipModule,
  NzUploadModule,
];
@NgModule({
  declarations: [
    InfoGeneralComponent,
    SchoolVideosComponent,
    UploadVideosComponent,
    UsersListComponent,
    AlliesComponent,
    UploadAlliesComponent,
    CountriesComponent,
    AlliesListComponent,
    AdminUsersListComponent,
    AddAdminUsersComponent,
    ListCountriesComponent,
    ListCitiesComponent,
    AddCitiesComponent,
    CreateDirectorComponent,
    ListDirectorsComponent,
    FinanRequestComponent,
  ],
  imports: [
    CommonModule,
    AdminsDashboardRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    ...antdModule,
  ],
})
export class AdminsDashboardModule {}
