import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Service
import { ContriesService } from '../../shared/services/contries.service';
import { AngularFirestore } from '@angular/fire/firestore';
// Model
import { Country } from '../../shared/models/country.model';
import { City } from '../../shared/models/city.model';
import { Observable } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd';
@Component({
  selector: 'app-add-cities',
  templateUrl: './add-cities.component.html',
  styles: [
    `
      [nz-form] {
        max-width: 600px;
      }
    `,
  ],
})
export class AddCitiesComponent implements OnInit {
  countries$: Observable<Country[]>;
  countries: Country[] = [];
  country: Country;
  zoneForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private countriesServ: ContriesService,
    private fireBase: AngularFirestore,
    private nzMessageService: NzMessageService
  ) {
    this.getCountries();
  }

  ngOnInit(): void {
    this.zoneForm = this.fb.group({
      name: [null, [Validators.required]],
      // acronym: [null, [Validators.required]],
      country: ['', [Validators.required]],
    });
    this.countries$ = this.countriesServ.getCountries$();
    this.countries$.subscribe((countries) => {
      this.getCountries(countries);
    });
  }

  submitZoneForm(): void {
    let cities: City[] = [];
    let city: City = new City();
    let citycounter = 0;
    let splitCounter = 0;
    for (const i in this.zoneForm.controls) {
      this.zoneForm.controls[i].markAsDirty();
      this.zoneForm.controls[i].updateValueAndValidity();
    }
    if (this.zoneForm.valid) {
      Object.keys(this.zoneForm.value).forEach((key) => {
        switch (key) {
          case `name`:
            city.name = this.zoneForm.value[key];
            // console.log(`key -> ${key} || data -> ${this.zoneForm.value[key]}`);
            break;
          // case `acronym`:
          //   city.id = this.zoneForm.value[key];
          //   // console.log(`key -> ${key} || data -> ${this.zoneForm.value[key]}`);
          //   break;
          case `country`:
            city.fatherID = this.zoneForm.value[key];
            // console.log(`key -> ${key} || data -> ${this.zoneForm.value[key]}`);
            break;
          default:
            break;
        }
        // this condition is respect de number of cases 0,1
        if (splitCounter == 1) {
          splitCounter = 0;
          citycounter++;
          city.id = this.fireBase.createId();
          city.directorUID = '';
          city.directormail = '';
          cities.push(city);
          city = new City();
        } else {
          splitCounter++;
        }
      });
      this.countriesServ
        .createCity(cities)
        .then((res) => {
          console.log(`city created ${city.name}`);
          this.nzMessageService.create('success', `Zona creada`);
        })
        .catch((err) => {
          console.log(`error creating city ${err}`);
          this.nzMessageService.create('error', `Error creando zona`);
        });
    }
  }

  getCountries(countries?: Country[]) {
    if (this.countriesServ.countries.length > 0) {
      this.countries = this.countries.filter((d) => d.id == '');
      this.countries = this.countriesServ.countries;
    } else {
      this.countriesServ.getContries();
    }
  }
  changeState(event: Country) {
    this.country = event;
  }
}
