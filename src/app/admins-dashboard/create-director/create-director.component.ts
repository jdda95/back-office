import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
//Services
import { ContriesService } from '../../shared/services/contries.service';
// import { AssociateCRUDService } from '../../shared/services/associate-crud.service';
import { DirRankService } from '../../shared/services/dir-rank.service';
//Models
import { Country } from '../../shared/models/country.model';
import { City } from '../../shared/models/city.model';
// import { Alie } from '../../shared/models/alie.model';
// import { Associate } from 'src/app/shared/models/associate.model';
import { DirRanks } from '../../shared/models/dir-ranks.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-director',
  templateUrl: './create-director.component.html',
  // styleUrls: ['./create-director.component.css'],
})
export class CreateDirectorComponent implements OnInit {
  directorForm: FormGroup;
  countries$: Observable<Country[]>;
  countries: Country[];
  country: Country;
  cities$: Observable<City[]>;
  cities: City[] = [];

  // directors array object
  directors: DirRanks[] = [];
  // alies: Alie[];
  selectedValue = null;
  //referral ID
  referralID = null;
  currAssoID = null;
  // See status update of referral data observer and keep the update data variable.
  statusReferral$: Observable<number>;
  statusReferral = 0;
  // variables to update referrals_list
  grandPaList: string = null;
  // errorsFirebase
  weakpassword = false;
  constructor(
    private fb: FormBuilder,
    private countriesServ: ContriesService,
    private dirrkServ: DirRankService
  ) {
    this.getCountries();
    this.getCities();
  }

  ngOnInit(): void {
    this.getDirRanks();
    this.directorForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      directorType: ['', [Validators.required]],
      country: [''],
      cityID: [''],
      date: new Date(),
    });

    this.countries$ = this.countriesServ.getCountries$();
    this.countries$.subscribe((countries) => {
      this.getCountries(countries);
    });
    this.cities$ = this.countriesServ.getCities$();
    this.cities$.subscribe((cities) => {
      this.getCities(cities);
    });
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.directorForm.controls.repassword.updateValueAndValidity()
    );
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.directorForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  submitForm(): void {
    for (const i of Object.keys(this.directorForm.controls)) {
      this.directorForm.controls[i].markAsDirty();
      this.directorForm.controls[i].updateValueAndValidity();
    }
    if (this.directorForm.valid) {
      this.dirrkServ.setDirector(this.directorForm.value);
      this.directorForm.reset();
    }
  }

  getCountries(countries?: Country[]) {
    if (this.countriesServ.countries.length > 0) {
      this.countries = this.countriesServ.countries;
      const seen = new Set();
      this.countries = this.countries.filter((el) => {
        const duplicate = seen.has(el.id);
        seen.add(el.id);
        return !duplicate;
      });
    } else {
      // console.log(` /// No country data`);
      this.countriesServ.getContries();
    }
  }
  getCities(cities?: City[]) {
    if (this.countriesServ.cities.length > 0) {
      this.cities = this.countriesServ.cities;
      const seen = new Set();
      this.cities = this.cities.filter((el) => {
        const duplicate = seen.has(el.id);
        seen.add(el.id);
        return !duplicate;
      });
    } else {
      // console.log(`/// No city data`);
      this.countriesServ.getContries();
    }
  }

  changeState(event: Country) {
    this.country = event;
  }

  getDirRanks() {
    this.dirrkServ
      .getRanksList()
      .snapshotChanges()
      .subscribe((data) => {
        this.directors = this.directors.filter((d) => d.name == '');
        data.forEach((rank) => {
          let cuRank: DirRanks = new DirRanks();
          cuRank.fireID = rank.payload.doc.id;
          Object.keys(rank.payload.doc.data()).forEach((key) => {
            cuRank[key] = rank.payload.doc.data()[key];
          });
          this.directors.push(cuRank);
        });
      });
  }
}
