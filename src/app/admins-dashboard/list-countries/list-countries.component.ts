import { Component, OnInit } from '@angular/core';
import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
//Services
import { ContriesService } from '../../shared/services/contries.service';
//Models
import { Country } from '../../shared/models/country.model';
import { Observable } from 'rxjs';

interface DataItem {
  id: string;
  name: string;
}

interface ColumnItem {
  name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn | null;
  listOfFilter: NzTableFilterList;
  sortDirections: NzTableSortOrder[];
}

@Component({
  selector: 'app-list-countries',
  templateUrl: './list-countries.component.html',
  styles: [
  ]
})
export class ListCountriesComponent implements OnInit{

  countries$: Observable<Country[]>;
  listOfData: Country[] = [];

  constructor(private countriesServ: ContriesService){
    this.getCountries();
  }

  ngOnInit():void{
    this.countries$ = this.countriesServ.getCountries$();
        this.countries$.subscribe(countries => {
            this.getCountries(countries);
        });
  }

  getCountries(countries?: Country[]) {
    if (this.countriesServ.countries.length > 0) {
      this.listOfData = this.listOfData.filter(d => d.id == '');
      this.listOfData = this.countriesServ.countries;
    } else {
        this.countriesServ.getContries();
    }
  }

  
  listOfColumns: ColumnItem[] = [
    {
      name: 'Código',
      sortOrder: 'ascend',
      sortFn: (a: DataItem, b: DataItem) => a.id.localeCompare(b.id),
      sortDirections: ['ascend','descend', null],
      listOfFilter: []
    },
    {
      name: 'Nombre',
      sortOrder: null,
      sortFn: (a: DataItem, b: DataItem) => a.name.localeCompare(b.name),
      sortDirections: ['ascend', 'descend', null],
      listOfFilter: []
    }
  ];
  
  // listOfData: DataItem[] = [
  //   {
  //     cod: 'COL',
  //     name: 'Colombia'
  //   },
  //   {
  //     cod: 'PER',
  //     name: 'Perú'
  //   },
  //   {
  //     cod:'ECU',
  //     name: 'Ecuador'
  //   }
  // ];
}
