import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';

// services
import { AuthenticationService } from "../../shared/services/authentication.service";

@Component({
  selector: 'app-add-admin-users',
  templateUrl: './add-admin-users.component.html',
  styles: [
    `
      [nz-form] {
        max-width: 400px;
      }

      button {
        margin-left: 8px;
      }
    `
  ]
})
export class AddAdminUsersComponent {

  validateForm: FormGroup;

  submitForm(value: { position: string; email: string; password: string; rankName?:string; fullName:string; profilePic?:string }): void {
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsDirty();
      this.validateForm.controls[key].updateValueAndValidity();
    }
    switch (value.position) {
      case '1':
        value.rankName = 'Operativo';  
        break;
      case '2':
        value.rankName = 'Administrativo';
        break;
      case '3':
        value.rankName = 'Relaciones públicas';
        break;
      default:
        console.log(`default name`);
        break;
    }
    value.profilePic = "A";
    this.authServ.createAdminUser(value)
    .then(res => {
      console.log(`admin user created and added to administrator colection ${res}`);
    })
    .catch(err => {console.error(`error creating admin user ${err}`)});
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsPristine();
      this.validateForm.controls[key].updateValueAndValidity();
    }
  }

  validateConfirmPassword(): void {
    setTimeout(() => this.validateForm.controls.confirm.updateValueAndValidity());
  }


  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  constructor(private fb: FormBuilder, private authServ:AuthenticationService) {
    this.validateForm = this.fb.group({
      position: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required]],
      confirm: ['', [this.confirmValidator]]
    });
  }

}
