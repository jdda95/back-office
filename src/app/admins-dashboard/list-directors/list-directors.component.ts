import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { Observable } from 'rxjs';
// Services
import { AssociateCRUDService } from '../../shared/services/associate-crud.service';
import { ContriesService } from '../../shared/services/contries.service';
// Models
import { Associate } from '../../shared/models/associate.model';
import { Country } from '../../shared/models/country.model';
import { City } from '../../shared/models/city.model';

@Component({
  selector: 'app-list-directors',
  templateUrl: './list-directors.component.html',
  styles: [
    `
      .delete-ico {
        font-size: 16px;
        color: #f00;
      }
      .delete-ico:hover {
        opacity: 0.5;
      }
    `,
  ],
})
export class ListDirectorsComponent implements OnInit {
  countries$: Observable<Country[]>;
  countries: Country[];
  cities$: Observable<City[]>;
  cities: City[];

  listOfData = [];

  constructor(
    private nzMessageService: NzMessageService,
    private assoServ: AssociateCRUDService,
    private countriesServ: ContriesService
  ) {
    this.getCities();
  }

  ngOnInit(): void {
    this.countries$ = this.countriesServ.getCountries$();
    this.countries$.subscribe((countries) => {
      this.countries = countries;
    });

    this.cities$ = this.countriesServ.getCities$();
    this.cities$.subscribe((cities) => {
      this.cities = cities;
      this.drawTable();
    });
  }

  getCities() {
    this.countriesServ.getContries();
  }

  drawTable() {
    for (let i = 0; i < this.cities.length; i++) {
      if (this.cities[i]['directormail'] !== '') {
        this.listOfData = [
          ...this.listOfData,
          {
            mail: this.cities[i]['directormail'],
            type: 'Zonal',
            country: this.countries.find(
              (country) => country.id === this.cities[i].fatherID
            )['name'],
            zone: this.cities[i].name,
          },
        ];
      }
      const seen = new Set();
      this.listOfData = this.listOfData.filter((el) => {
        const duplicate = seen.has(el.zone);
        seen.add(el.zone);
        return !duplicate;
      });
    }

    for (let i = 0; i < this.countries.length; i++) {
      if (this.countries[i].natDirectorMail !== '') {
        this.listOfData = [
          ...this.listOfData,
          {
            mail: this.countries[i].natDirectorMail,
            type: 'Nacional',
            country: this.countries[i].name,
            zone: '',
          },
        ];
      }
      // if (this.countries[i].mrkDirectorMail !== '') {
      //   this.listOfData = [
      //     ...this.listOfData,
      //     {
      //       mail: this.countries[i].mrkDirectorMail,
      //       type: 'Marketing',
      //       country: this.countries[i].name,
      //       zone: '',
      //     },
      //   ];
      // }
      const seen = new Set();
      this.listOfData = this.listOfData.filter((el) => {
        const duplicate = seen.has(el.mail);
        seen.add(el.mail);
        return !duplicate;
      });
    }
  }

  cancel(): void {
    return;
  }
  confirm(aliesID: string): void {
    this.nzMessageService.info('Eliminando...');
  }
}
