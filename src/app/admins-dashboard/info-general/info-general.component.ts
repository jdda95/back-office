import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ThemeConstantService } from 'src/app/shared/services/theme-constant.service';
import { NzMessageService } from 'ng-zorro-antd/message';
// Services
import { MembershipsService } from '../../shared/services/memberships.service';
import { GeneralInfoService } from '../../shared/services/general-info.service';
import { AliesService } from '../../shared/services/alies.service';
// Models
import { Membership } from 'src/app/shared/models/membership.model';
import { Wallet } from '../../shared/models/wallet.model';

@Component({
  selector: 'app-info-general',
  templateUrl: './info-general.component.html',
  styles: [
    `
      .ant-advanced-search-form {
        padding: 24px;
        background: #fbfbfb;
        border: 1px solid #d9d9d9;
        border-radius: 6px;
      }
      .search-result-list {
        margin-top: 16px;
        border: 1px dashed #e9e9e9;
        border-radius: 6px;
        background-color: #fafafa;
        min-height: 200px;
        text-align: center;
        padding-top: 80px;
      }
      [nz-form-label] {
        overflow: visible;
      }
      button {
        margin-left: 8px;
      }
      .collapse {
        margin-left: 8px;
        font-size: 12px;
      }
      .search-area {
        text-align: right;
      }
    `,
  ],
})
export class InfoGeneralComponent implements OnInit {
  themeColors = this.colorConfig.get().colors;
  orange = this.themeColors.orange;
  validateForm: FormGroup;
  validateForm2: FormGroup;
  controlArray: Array<{ index: number }> = [];
  memberships: Array<Membership> = [];
  valuesChanges = false;
  budgets = [];
  wallets = [];

  resetForm(): void {
    this.validateForm.reset();
  }
  cancel(): void {
    this.nzMessageService.warning('click cancel');
  }

  confirm(idAlly): void {
    let fieldUp = { bill: 0 };
    this.allyServ
      .updateAlly(idAlly, fieldUp)
      .then((res) => {
        this.nzMessageService.success('Retiro del monto realizado correctamente');
      })
      .catch((err) => {
        console.log(`Error retirando monto de ${idAlly}`, err);
      });
  }

  constructor(
    private fb: FormBuilder,
    private colorConfig: ThemeConstantService,
    private nzMessageService: NzMessageService,
    private membServ: MembershipsService,
    private infoServ: GeneralInfoService,
    private allyServ: AliesService
  ) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({});
    this.validateForm2 = this.fb.group({});
    this.getMemberships();
    this.getGeneralInfo();
    this.getAlies();
  }
  valueChange(controlName) {
    this.validateForm.valueChanges.subscribe((selectedValue) => {
      console.log('New Value: ', selectedValue);
      console.log('Old Value: ', this.validateForm.value[controlName]);
    });
  }
  onChanges(): void {
    this.validateForm.valueChanges.subscribe((val) => {
      this.valuesChanges = true;
    });
    this.validateForm2.valueChanges.subscribe((val) => {
      this.valuesChanges = true;
    });
  }

  getMemberships() {
    this.membServ
      .getMemberships()
      .get()
      .subscribe(
        (res) => {
          res.docs.forEach((memb) => {
            let cuMemb = new Membership();
            cuMemb.fireID = memb.id;
            Object.keys(memb.data()).forEach((key) => {
              cuMemb[key] = memb.data()[key];
            });
            this.memberships = [...this.memberships, cuMemb];
          });
        },
        (err) => {
          console.error(err);
        },
        () => {
          for (let i = 0; i < this.memberships.length; i++) {
            this.controlArray.push({ index: i });
            this.validateForm.addControl(
              `${this.memberships[i].fireID}`,
              new FormControl('')
            );
            this.validateForm.controls[
              `${this.memberships[i].fireID}`
            ].setValue(this.memberships[i].price);
          }
          this.onChanges();
        }
      );
  }

  getGeneralInfo() {
    this.infoServ
      .getInfo()
      .get()
      .subscribe(
        (res) => {
          res.docs.forEach((info) => {
            Object.keys(info.data()).forEach((key) => {
              if (key == 'linkBudget') {
                let curField = {
                  id: `l-${info.id}`,
                  name: 'Carga incial vinculaciones',
                  amount: info.data()[key],
                  realID: info.id,
                  field: key,
                };
                this.budgets = [...this.budgets, curField];
              }
              if (key == 'matrixBudget') {
                let curField = {
                  id: `m-${info.id}`,
                  name: 'Carga incial Matriz',
                  amount: info.data()[key],
                  realID: info.id,
                  field: key,
                };
                this.budgets = [...this.budgets, curField];
              }
            });
          });
        },
        (err) => console.error(err),
        () => {
          for (let i = 0; i < this.budgets.length; i++) {
            this.controlArray.push({ index: i });
            this.validateForm2.addControl(
              this.budgets[i].id,
              new FormControl('')
            );
            this.validateForm2.controls[this.budgets[i].id].setValue(
              this.budgets[i].amount
            );
          }
          this.onChanges();
        }
      );
  }

  sendForm() {
    this.memberships.forEach((value) => {
      // console.log(`value array ->${value.price} || form ->${this.validateForm.value[value.fireID]}`,value.price != this.validateForm.value[value.fireID]);
      if (value.price != this.validateForm.value[value.fireID]) {
        this.membServ
          .setMemeberships(value.fireID, {
            price: this.validateForm.value[value.fireID],
          })
          .then((res) => {
            console.log(`memb update succefully`);
          })
          .catch((err) => {
            console.error(`error updating price membership`, err);
          });
      }
    });
  }
  sendForm2() {
    this.budgets.forEach((value, i) => {
      // console.log(`value array ->${value['amount']} || form ->${this.validateForm2.value[value['id']]}`, value['amount'] != this.validateForm2.value[value['id']]);
      if (value['amount'] != this.validateForm2.value[value['id']]) {
        let fieldUp = value['field'];
        this.infoServ
          .setInfo(value['realID'], {
            [fieldUp]: this.validateForm2.value[value['id']],
          })
          .then((res) => {
            console.log(`genegar info update succefully`);
          })
          .catch((err) => {
            console.error(`error updating info data`, err);
          });
      }
    });
  }

  getAlies() {
    this.allyServ
      .getAlies()
      .snapshotChanges()
      .subscribe((alies) => {
        alies.forEach((ally) => {
          let cuAlly: Wallet = new Wallet();
          cuAlly.owner = ally.payload.doc.id;
          cuAlly.current_balance = ally.payload.doc.data()['bill'];
          cuAlly.name = ally.payload.doc.data()['name'];
          this.wallets = [...this.wallets, cuAlly];
          // evaluate if already has this alie
          const seen = new Set();
          this.wallets = this.wallets.filter((el) => {
            const duplicate = seen.has(el.name);
            seen.add(el.name);
            return !duplicate;
          });
        });
      });
  }
}
