import { Component, OnInit } from '@angular/core';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
  NzTableSortOrder,
} from 'ng-zorro-antd/table';

//Services
import { ContriesService } from '../../shared/services/contries.service';
//Models
import { Country } from '../../shared/models/country.model';
import { City } from '../../shared/models/city.model';
import { Observable } from 'rxjs';

interface DataItem {
  id: string;
  name: string;
  fatherID: string;
}

interface ColumnItem {
  name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn | null;
  listOfFilter: NzTableFilterList | null;
  filterFn: NzTableFilterFn | null;
  filterMultiple: boolean;
  sortDirections: NzTableSortOrder[];
}
@Component({
  selector: 'app-list-cities',
  templateUrl: './list-cities.component.html',
  styles: [],
})
export class ListCitiesComponent implements OnInit {
  countries$: Observable<Country[]>;
  countries: Country[];
  cities$: Observable<City[]>;
  cities: City[];

  listOfData: City[] = [];
  auxFilter: boolean = true;

  constructor(private countriesServ: ContriesService) {
    // this.getCountries();
    this.getCities();
  }

  ngOnInit(): void {
    // this.countries$ = this.countriesServ.getCountries$();
    // this.countries$.subscribe(countries => {
    //     this.getCountries(countries);
    // });
    this.cities$ = this.countriesServ.getCities$();
    this.cities$.subscribe((cities) => {
      this.getCities(cities);
    });
  }

  getCities(cities?: City[]) {
    // console.log(`draw cities`);
    // this.listOfData = this.listOfData.filter(d => d.id == '');
    if (this.countriesServ.cities.length > 0) {
      // console.log(`already exist ${this.countriesServ.cities.length}`);
      // add countries to the filter box
      if (this.auxFilter) {
        for (let i = 0; i < this.countriesServ.countries.length; i++) {
          let element = this.countriesServ.countries[i].name;
          this.listOfColumns[1]['listOfFilter'] = [
            ...this.listOfColumns[1]['listOfFilter'],
            { text: element, value: element },
          ];
          // this.listOfColumns[2]['listOfFilter'] = [...this.listOfColumns[2]['listOfFilter'],{text: element, value: element}];
        }
        this.auxFilter = false;
      }
      // clean and add data to datalist object
      // this.listOfData = this.listOfData.filter(d => d.id == '');
      this.listOfData = this.countriesServ.cities;
      const seen = new Set();
      this.listOfData = this.listOfData.filter((el) => {
        const duplicate = seen.has(el.id);
        seen.add(el.id);
        return !duplicate;
      });
    } else {
      console.log('not data avaible request');
      this.countriesServ.getContries();
    }
    // replase fatherID - cod  from country with the name
    Object.keys(this.listOfData).forEach((key) => {
      Object.keys(this.listOfData[key]).forEach((key2) => {
        if (key2 == 'fatherID') {
          Object.keys(this.countriesServ.countries).forEach((countrykey) => {
            Object.keys(this.countriesServ.countries[countrykey]).forEach(
              (countrykey2) => {
                if (
                  countrykey2 == 'id' &&
                  this.listOfData[key][key2] ==
                    this.countriesServ.countries[countrykey][countrykey2]
                ) {
                  this.listOfData[key][key2] = this.countriesServ.countries[
                    countrykey
                  ]['name'];
                }
              }
            );
          });
        }
      });
    });
  }

  listOfColumns: ColumnItem[] = [
    // {
    //   name: 'Código',
    //   sortOrder: null,
    //   sortFn: (a: DataItem, b: DataItem) => a.id.localeCompare(b.id),
    //   sortDirections: ['ascend', 'descend', null],
    //   filterMultiple: false,
    //   filterFn: null,
    //   listOfFilter: []
    // },
    {
      name: 'Nombre',
      sortOrder: null,
      sortFn: (a: DataItem, b: DataItem) => a.name.localeCompare(b.name),
      sortDirections: ['ascend', 'descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: false,
    },
    {
      name: 'País',
      sortOrder: 'descend',
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) =>
        a.fatherID.localeCompare(b.fatherID),
      filterMultiple: false,
      listOfFilter: [],
      filterFn: (country: string, item: DataItem) =>
        item.fatherID.indexOf(country) !== -1,
    },
  ];
  // listOfData: DataItem[] = [
  //   {
  //     id: 'med',
  //     name: 'Medellin',
  //     country: 'Colombia'
  //   },
  //   {
  //     id: 'bog',
  //     name: 'Bogotá',
  //     country: 'Colombia'
  //   },
  //   {
  //     id: 'lim',
  //     name: 'Lima',
  //     country: 'Perú'
  //   },
  //   {
  //     id: 'col',
  //     name: 'Colón',
  //     country: 'Panamá'
  //   }
  // ];
}
