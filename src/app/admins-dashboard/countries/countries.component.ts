import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
// Services
import { ContriesService } from '../../shared/services/contries.service';
// Model
import { Country } from '../../shared/models/country.model';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styles: [
    `
      .dynamic-delete-button {
        cursor: pointer;
        position: relative;
        top: 4px;
        font-size: 24px;
        color: #999;
        transition: all 0.3s;
      }

      .dynamic-delete-button:hover {
        color: #777;
      }

      .passenger-input {
        width: 45%;
        margin-right: 12px;
      }

      [nz-form] {
        max-width: 1200px;
      }

      .add-button {
        width: 45%;
      }
    `,
  ],
})
export class CountriesComponent implements OnInit {
  validateForm: FormGroup;
  listOfControl: Array<{ id: number; code: string; name: string }> = [];
  // notification variables
  successmsg: boolean = false;
  errormsg: boolean = false;

  addField(e?: MouseEvent): void {
    if (e) {
      e.preventDefault();
    }
    const id =
      this.listOfControl.length > 0
        ? this.listOfControl[this.listOfControl.length - 1].id + 1
        : 0;

    const control = {
      id,
      code: `code${id}`,
      name: `name${id}`,
    };
    const index = this.listOfControl.push(control);
    // console.log(this.listOfControl[this.listOfControl.length - 1]);
    this.validateForm.addControl(
      this.listOfControl[index - 1].code,
      new FormControl(null, [Validators.required, Validators.maxLength(3)])
    );
    this.validateForm.addControl(
      this.listOfControl[index - 1].name,
      new FormControl(null, Validators.required)
    );
  }

  removeField(
    i: { id: number; code: string; name: string },
    e: MouseEvent
  ): void {
    e.preventDefault();
    if (this.listOfControl.length > 1) {
      const index = this.listOfControl.indexOf(i);
      this.listOfControl.splice(index, 1);
      console.log(this.listOfControl);
      this.validateForm.removeControl(i.code);
      this.validateForm.removeControl(i.name);
    }
  }

  submitForm(): void {
    let contries: Country[] = [];
    let country: Country = new Country();
    let countryCounter = 0;
    let sliptCounter = 0;
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (this.validateForm.valid) {
      Object.keys(this.validateForm.value).forEach((key) => {
        switch (key) {
          case `code${countryCounter}`:
            country.id = this.validateForm.value[key];
            break;
          case `name${countryCounter}`:
            country.name = this.validateForm.value[key];
            break;
          default:
            break;
        }
        if (sliptCounter == 1) {
          sliptCounter = 0;
          countryCounter++;
          country.natDirectorUID = '';
          country.natDirectorMail = '';
          country.mrkDirectorUID = '';
          country.mrkDirectorMail = '';
          contries.push(country);
          country = new Country();
        } else {
          sliptCounter++;
        }
      });
      this.counServ
        .createCountry(contries)
        .then((res) => {
          console.log(`countries created`);
          this.successmsg = true;
          this.nzMessageService.create('success', `Paises creados`);
        })
        .catch((err) => {
          console.log(`error creating countries ${err}`);
          this.errormsg = true;
          this.nzMessageService.create('error', `Error creando paises`);
        });
    }
  }

  constructor(private fb: FormBuilder, private counServ: ContriesService,  private nzMessageService: NzMessageService) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({});
    this.addField();
  }
}
