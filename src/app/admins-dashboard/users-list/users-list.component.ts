import { Component, OnInit } from '@angular/core';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
  NzTableSortOrder,
} from 'ng-zorro-antd/table';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Users } from 'src/app/shared/models/user.model';
import { Observable } from 'rxjs';
// Services
import { AssociateCRUDService } from '../../shared/services/associate-crud.service';
import { MembershipsService } from 'src/app/shared/services/memberships.service';
import { RankService } from '../../shared/services/rank.service';
import { ExportExcelService } from 'src/app/shared/services/export-excel.service';
import { AliesService } from '../../shared/services/alies.service';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { ContriesService } from '../../shared/services/contries.service';
// Model
import { Associate } from '../../shared/models/associate.model';
import { Membership } from 'src/app/shared/models/membership.model';
import { Rank } from '../../shared/models/rank.model';
import { Alie } from '../../shared/models/alie.model';
import { MandrillService } from 'src/app/shared/services/mandrill.service';
import { Country } from 'src/app/shared/models/country.model';
interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder;
  sortFn?: NzTableSortFn;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
  filterMultiple?: boolean;
  sortDirections?: NzTableSortOrder[];
}

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styles: [
    `
      .d-flex.align-items-center h6 {
        white-space: nowrap;
        width: 60%;
        overflow: hidden;
        text-overflow: ellipsis;
      }
      .delete-ico {
        font-size: 16px;
        color: #f00;
      }
      .delete-ico:hover {
        opacity: 0.5;
      }
      .ant-select {
        width: 100%;
      }
      .search-box {
        padding: 8px;
      }

      .search-box input {
        width: 188px;
        margin-bottom: 8px;
        display: block;
      }

      .search-box button {
        width: 90px;
      }

      .search-button {
        margin-right: 8px;
      }
    `,
  ],
})
export class UsersListComponent implements OnInit {
  listOfColumns: ColumnItem[] = [
    {
      name: 'Nombre',
    },
    {
      name: 'Usuario',
    },
    {
      name: 'Correo',
    },
    {
      name: 'Membresía',
    },
    {
      name: ' Escuela',
    },
    {
      name: 'Teléfono',
    },
    {
      name: 'Cargo',
    },
    {
      name: 'Activo',
    },
    {
      name: '',
    },
  ];
  associateList: Associate[] = [];
  membershipsList: Membership[];
  rankList: Rank[];
  aliesList: Alie[];
  selectedMembership: string;
  defaultSelectorValue = '';

  dataForExcel = [];

  listOfDisplayData = [...this.associateList];
  searchValue = '';
  searchEmail = '';
  visible = false;
  visibleEmail = false;
  // National Director variables
  countryCode = '';
  directorData: Associate = new Associate();
  countries: Country[] = [];
  countries$: Observable<Country[]>;
  isDirector: boolean = false;

  constructor(
    private nzMessageService: NzMessageService,
    private assoServ: AssociateCRUDService,
    private memberServ: MembershipsService,
    public ete: ExportExcelService,
    private rankServ: RankService,
    private mandrill: MandrillService,
    private aliesServ: AliesService,
    private authServ: AuthenticationService,
    private countryServ: ContriesService
  ) {}

  ngOnInit(): void {
    // this.route.params.subscribe(({ id }) => (this.idContent = id));

    this.countries$ = this.countryServ.getCountries$();
    this.countries$.subscribe((data) => {
      this.countries = data;
    });

    this.associateList = this.associateList.filter((d) => d.fireId == '');
    this.listOfDisplayData = this.listOfDisplayData.filter(
      (d) => d.fireId == ''
    );

    this.getMemberships();

    this.authServ.hasUserB().then((user) => {
      if (user !== null) {
        if (this.assoServ.associate === undefined) {
          this.assoServ
            .getAssociateData(user.uid)
            .get()
            .forEach((res) => {
              user.getIdTokenResult().then((idTokenResult) => {
                switch (idTokenResult.claims.role) {
                  case 4:
                    this.isDirector = true;
                    this.getUserList(
                      res.get('countryID'),
                      idTokenResult.claims.role
                    );
                    break;
                  case 5:
                    this.isDirector = true;
                    this.getUserList(
                      res.get('cityID'),
                      idTokenResult.claims.role
                    );
                    break;
                  default:
                    this.getUserList();
                    break;
                }
              });
            });
        } else {
          this.assoServ
            .getAssociateData(user.uid)
            .get()
            .forEach((res) => {
              user.getIdTokenResult().then((idTokenResult) => {
                switch (idTokenResult.claims.role) {
                  case 4:
                    this.isDirector = true;
                    this.getUserList(
                      res.get('countryID'),
                      idTokenResult.claims.role
                    );
                    break;
                  case 5:
                    this.isDirector = true;
                    this.getUserList(
                      res.get('cityID'),
                      idTokenResult.claims.role
                    );
                    break;
                  default:
                    this.getUserList();
                    break;
                }
              });
            });
        }
      }
    });
  }

  async getUserList(filter?, role?) {
    // await this.getRanks();
    await this.getAlies();
    this.assoServ
      .getAssociateList()
      .snapshotChanges()
      .subscribe((associates) => {
        associates.forEach((associate) => {
          const cuAssociate = new Associate();
          cuAssociate.fireId = associate.payload.doc.id;
          Object.keys(associate.payload.doc.data()).forEach((key) => {
            switch (key) {
              case 'alieID':
                this.aliesList.forEach((ally) => {
                  if (ally.firebaseID == associate.payload.doc.data()[key]) {
                    cuAssociate.alieID = ally.name;
                  }
                });
                break;
              default:
                cuAssociate[key] = associate.payload.doc.data()[key];
                break;
            }
          });
          this.associateList = [...this.associateList, cuAssociate];
          // evaluate if already has object duplicate
          const seen = new Set();
          this.associateList = this.associateList.filter((el) => {
            const duplicate = seen.has(el.fireId);
            seen.add(el.fireId);
            return !duplicate;
          });
          if (filter !== undefined && role === 4) {
            this.associateList = this.associateList.filter(
              (asso) => asso.countryID == filter
            );
          }
          if (filter !== undefined && role === 5) {
            this.associateList = this.associateList.filter(
              (asso) => asso.cityID === filter
            );
          }
          this.listOfDisplayData = [...this.associateList];
        });
        this.getMemberships();
      });
  }

  getMemberships() {
    this.membershipsList = [];
    this.memberServ
      .getMemberships()
      .snapshotChanges()
      .subscribe((data) => {
        data.forEach((membership) => {
          // this.membershipsList = this.membershipsList.filter(d => d.name == '');
          const cMemb: Membership = new Membership();
          cMemb.fireID = membership.payload.doc.id;
          cMemb.name = membership.payload.doc.data()['name'];
          this.membershipsList.push(cMemb);
        });
        const seen = new Set();
        this.membershipsList = this.membershipsList.filter((el) => {
          const duplicate = seen.has(el.fireID);
          seen.add(el.fireID);
          return !duplicate;
        });
      });
  }

  async getRanks() {
    this.rankList = [];
    this.rankServ
      .getRanks()
      .snapshotChanges()
      .subscribe((data) => {
        data.forEach((rank) => {
          const cuRank: Rank = new Rank();
          Object.keys(rank.payload.doc.data()).forEach((key) => {
            cuRank[key] = rank.payload.doc.data()[key];
          });
          this.rankList.push(cuRank);
        });
      });
  }

  getAlies(): Promise<void> {
    this.aliesList = [];
    this.aliesServ
      .getAlies()
      .snapshotChanges()
      .subscribe((data) => {
        data.forEach((ally) => {
          const cuAlly: Alie = new Alie();
          cuAlly.firebaseID = ally.payload.doc.id;
          Object.keys(ally.payload.doc.data()).forEach((key) => {
            cuAlly[key] = ally.payload.doc.data()[key];
          });
          this.aliesList.push(cuAlly);
        });
      });
    return;
  }

  cancel(): void {
    this.nzMessageService.info('click cancel');
  }

  confirm(): void {
    this.nzMessageService.info('Usuario Eliminado');
  }

  updateStatus(
    fireId: string,
    statusCK: boolean,
    curMemb: string,
    fullname: string,
    email: string,
    complexref: string
  ) {
    // console.log(`data from list users ${fireId} - ${statusCK} - ${curMemb}`);
    this.assoServ
      .updateAssociateData(
        fireId,
        { status: statusCK, membershipID: curMemb },
        { status: statusCK, ref: complexref }
      )
      .then((result) => {
        console.info(
          `activación de usuario satisfactoria, inicia proceso de disperción de pagos`
        );
        if (statusCK === true) {
          // SEND NOTIFICATION
          this.mandrill
            .sendLoginMail(email, fullname, {})
            .subscribe((res) => console.log(res));
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }

  reset(): void {
    this.searchValue = '';
    this.search();
  }
  resetEmail(): void {
    this.searchEmail = '';
    this.searchEmailVal();
  }

  search(): void {
    this.visible = false;
    this.listOfDisplayData = this.associateList.filter((item: Associate) => {
      return item.fullName.toLocaleLowerCase().indexOf(this.searchValue) !== -1;
    });
  }
  searchEmailVal(): void {
    this.visibleEmail = false;
    this.listOfDisplayData = this.associateList.filter((item: Associate) => {
      return item.email.toLocaleLowerCase().indexOf(this.searchEmail) !== -1;
    });
  }
  exportToExcel() {
    let headers;
    this.listOfDisplayData.forEach((row: any) => {
      let {
        fullName,
        countryID,
        address,
        nickName,
        rankName,
        dni,
        email,
        status,
        membershipID,
      } = row;
      let singleUser = {
        Nombre: fullName,
        País: countryID,
        Dirección: address,
        Usuario: nickName,
        Correo: email,
        Rango: rankName,
        DNI: dni,
        Estado: status,
        Membresía: membershipID,
      };
      this.dataForExcel.push(Object.values(singleUser));
      headers = Object.keys(singleUser);
    });

    let reportData = {
      title: '24/7 Bussines Center - Usuarios',
      data: this.dataForExcel,
      headers,
    };

    this.ete.exportExcel(reportData);
  }

  openMembSelector(event: boolean, status) {
    if (event == true) {
      console.log(
        `Esta seguro de querer modificar la membresía, al realizarlo esto efectuara la disperción y no podrá revertirse`
      );
      if (status == false) {
        console.log(
          `El usuario a modificar actualmente esta desactivado, al cambiar la membresía será activado`
        );
      }
    }
  }

  changeMemb(dataMemb: any, fireId: string, complexref: string) {
    // console.log(`change slection to ${dataMemb}`);
    this.assoServ
      .updateAssociateData(
        fireId,
        { status: true, membershipID: dataMemb },
        { status: true, ref: complexref }
      )
      .then((result) => {
        console.info(
          `cambio de membresía satisfactorio, inicia proceso de disperción de pagos`
        );
        location.reload();
      })
      .catch((err) => {
        console.error();
      });
  }
}
