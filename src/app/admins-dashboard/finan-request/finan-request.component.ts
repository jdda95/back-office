import { Component, OnInit } from '@angular/core';
// Services
import { TokeAndMovementsService } from '../../shared/services/toke-and-movements.service';
// Model
import { Movement } from '../../shared/models/movement.model';
import { MandrillService } from 'src/app/shared/services/mandrill.service';

@Component({
  selector: 'app-finan-request',
  templateUrl: './finan-request.component.html',
  styles: [
    `
      .ant-popover-message-title {
        max-width: 360px;
        text-align: justify;
      }
    `,
  ],
})
export class FinanRequestComponent implements OnInit {
  listOfData: Movement[] = [];
  // listOfData = [
  //   {
  //     key: '1',
  //     name: 'John Brown',
  //     age: 32,
  //     address: 'New York No. 1 Lake Park',
  //     date: new Date().getTime(),
  //   },
  //   {
  //     key: '2',
  //     name: 'Jim Green',
  //     age: 42,
  //     address: 'London No. 1 Lake Park',
  //     date: new Date().getTime(),
  //   },
  //   {
  //     key: '3',
  //     name: 'Joe Black',
  //     age: 32,
  //     address: 'Sidney No. 1 Lake Park',
  //     date: new Date().getTime(),
  //   },
  // ];
  constructor(
    private moveServ: TokeAndMovementsService,
    private mandrill: MandrillService
  ) {}

  ngOnInit(): void {
    this.reqMovements();
  }

  reqMovements() {
    this.moveServ.getAllMovementsFilter('status', 1).subscribe((res) => {
      if (res !== undefined) {
        res.docs.forEach((doc) => {
          let cuMove: Movement = new Movement();
          cuMove.fireID = doc.id;
          Object.keys(doc.data()).forEach((key) => {
            cuMove[key] = doc.data()[key];
          });
          this.listOfData = [...this.listOfData, cuMove];
        });
      }
    });
  }
  confirmSi(moveId, amount, walletID1, newStatus, email) {
    let moveData = {
      type: 1,
      id: moveId,
      amount: amount,
      walletID1: walletID1,
      newStatus: newStatus,
    };
    this.moveServ.changeStatusMovement(moveData).then((res) => {
      if (res['status'] === 1) {
        this.listOfData = this.listOfData.filter(
          (mov) => mov.fireID !== moveId
        );
        console.log(res['msg']);
        this.mandrill.moneyAccept(email, 'Solicitud').subscribe((res) => {
          console.log(res);
        });
      } else if (res['status'] === 2) {
        console.log(`${res['msg']} - Error: ${res['err']}`);
      }
    });
  }
  cancelSi() {
    console.log('cancel');
  }
  confirmNo(moveId, amount, walletID1, newStatus, email) {
    let moveData = {
      type: 2,
      id: moveId,
      amount: amount,
      walletID1: walletID1,
      newStatus: newStatus,
    };
    this.moveServ.changeStatusMovement(moveData).then((res) => {
      if (res['status'] === 1) {
        this.listOfData = this.listOfData.filter(
          (mov) => mov.fireID !== moveId
        );
        console.log(res['msg']);
        this.mandrill.moneyDenied(email, 'Solicitud').subscribe((res) => {
          console.log(res);
        });
      } else if (res['status'] === 2) {
        console.log(`${res['msg']} - Error: ${res['err']}`);
      }
    });
  }
  cancelNo() {
    console.log('cancel');
  }
}
