import { Component, OnInit } from '@angular/core';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
  NzTableSortOrder,
} from 'ng-zorro-antd/table';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Observable, Observer, Subscriber, Subscription } from 'rxjs';
//  Services
import { VideosService } from "../../shared/services/videos.service";
import { AssociateCRUDService } from "../../shared/services/associate-crud.service";
import { AuthenticationService } from "../../shared/services/authentication.service";
import { RankService } from "../../shared/services/rank.service";
//  Models
import { Video } from "../../shared/models/video.model";
import { Rank } from "../../shared/models/rank.model";

interface VideoItem {
  name: string;
  description: string;
  link: string;
  image: string;
  position: string;
}

interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder;
  sortFn?: NzTableSortFn;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
  filterMultiple?: boolean;
  sortDirections?: NzTableSortOrder[];
}

@Component({
  selector: 'app-school-videos',
  templateUrl: './school-videos.component.html',
  styles: [
    `
    .delete-ico {
      font-size: 16px;
      color: #f00;
    }
    .delete-ico:hover {
      opacity: 0.5;
    }
  `,
  ]
})
export class SchoolVideosComponent implements OnInit {
  listOfColumns: ColumnItem[] = [
    {
      name: 'Título',
    },
    {
      name: 'Desc.',
    },
    {
      name: 'Link',
    },
    {
      name: 'Cargo',
    },
    {
      name: '',
    },
  ];
  videoList: Array<any> = [];
  editId: string | null = null;
  settingList: Subscription;
  constructor(private nzMessageService: NzMessageService, private videoServ: VideosService, private assoServ: AssociateCRUDService, private authserv: AuthenticationService, private rkServ: RankService) { }

  listVideos: Video[] = [];
  listRanks: Rank[] = [];

  async ngOnInit(): Promise<void> {
    await this.getRanksList();
    await this.getVideoListForUser();
  }
  async getRanksList() {
    this.rkServ.getRanks().get().subscribe(res => {
      res.docs.forEach(rk => {
        let cuRk: Rank = new Rank();
        cuRk.fireID = rk.id;
        Object.keys(rk.data()).forEach(key => {
          cuRk[key] = rk.data()[key];
        });
        this.listRanks = [...this.listRanks, cuRk];
      });
    });
  }
  async getVideoListForUser() {

    this.videoServ.getListVideos().subscribe(res => {
      res.docs.forEach(video => {
        let cuVideo: Video = new Video();
        cuVideo.fireID = video.id;
        Object.keys(video.data()).forEach(key => {
          if (key === 'position') {
            let realName = this.listRanks.find(rk => rk.fireID === video.data()[key]);
            cuVideo[key] = realName.name;
          } else {
            cuVideo[key] = video.data()[key];
          }
        });
        this.listVideos = [...this.listVideos, cuVideo];
        console.log(this.listVideos);
      });
      const seen = new Set();
      this.listVideos = this.listVideos.filter(el => {
        const duplicate = seen.has(el.fireID);
        seen.add(el.fireID);
        return !duplicate;
      });
    });
  }

  cancel(): void {
    return;
  }

  confirm(videoID: string): void {
    this.nzMessageService.info(`Eliminando...`);
    this.videoServ.deleteVideo(videoID).then(res => {
      // console.log('Video eliminado satisfactoriamente');
      this.nzMessageService.success('Video Eliminado');
      this.listVideos = this.listVideos.filter(vid => vid.fireID !== videoID);
    }).catch(err => {
      console.error(`Error eliminando el video, intentelo más tarde nuevamente`, err);
      this.nzMessageService.error('Error eliminando el video, intentelo más tarde nuevamente', err);
    });
  }

  // deleteAlie(aliesID: string) {
  //   this.nzMessageService.info('Aliado Eliminado');
  // }

}
