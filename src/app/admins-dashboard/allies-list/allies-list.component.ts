import { Component, OnInit } from '@angular/core';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
  NzTableSortOrder,
} from 'ng-zorro-antd/table';
import { NzMessageService } from 'ng-zorro-antd/message';
// Services
import { AliesService } from '../../shared/services/alies.service';
import { ContriesService } from '../../shared/services/contries.service';
// Model
import { Alie } from '../../shared/models/alie.model';
import { Country } from '../../shared/models/country.model';
import { Observable, Observer, Subscriber, Subscription } from 'rxjs';

interface AllieItem {
  name: string;
  country: string;
  rate: number;
}

interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder;
  sortFn?: NzTableSortFn;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
  filterMultiple?: boolean;
  sortDirections?: NzTableSortOrder[];
}

@Component({
  selector: 'app-allies-list',
  templateUrl: './allies-list.component.html',
  styles: [
    `
      .delete-ico {
        font-size: 16px;
        color: #f00;
      }
      .delete-ico:hover {
        opacity: 0.5;
      }
    `,
  ],
})
export class AlliesListComponent implements OnInit {
  listOfColumns: ColumnItem[] = [
    {
      name: 'Aliado',
    },
    {
      name: 'País',
    },
    {
      name: 'Tarifa',
    },
    {
      name: 'Saldo a favor',
    },
    {
      name: 'Estado',
    },
  ];
  editId: string | null = null;
  aliesList: Alie[] = [];
  countries: Country[] = [];
  countries$: Observable<Country[]>;
  settingList: Subscription;

  constructor(
    private nzMessageService: NzMessageService,
    private aliesServ: AliesService,
    private countryServ: ContriesService
  ) { }

  ngOnInit(): void {
    this.countries$ = this.countryServ.getCountries$();
    this.countries$.subscribe((data) => {
      this.setListCountries(data);
    });
    this.setListCountries();
    this.setListalies();
  }

  startEdit(id: string): void {
    this.editId = id;
  }

  stopEdit(): void {
    this.editId = null;
  }

  cancel(): void {
    return;
  }

  confirm(aliesID: string): void {
    this.nzMessageService.info('Eliminando...');
    this.deleteAlie(aliesID);
  }

  setListCountries(countries?: Country[]) {
    if (this.countryServ.countries.length === 0) {
      this.countryServ.getContries();
    } else {
      this.countries = this.countryServ.countries;
    }
  }

  setListalies() {
    this.settingList = this.aliesServ
      .getAlies()
      .snapshotChanges()
      .subscribe((data) => {
        data.forEach((dalie) => {
          let alie: Alie = new Alie();
          alie.firebaseID = dalie.payload.doc.id;

          Object.keys(dalie.payload.doc.data()).forEach((key) => {
            if (key === 'countryID') {
              this.countries.forEach((country) => {
                if (dalie.payload.doc.data()[key] === country.id) {
                  alie.countryName = country.name;
                }
              });
            } else {
              alie[key] = dalie.payload.doc.data()[key];
            }
          });
          this.aliesList = [...this.aliesList, alie];
          const seen = new Set();
          this.aliesList = this.aliesList.filter(el => {
            const duplicate = seen.has(el.firebaseID);
            seen.add(el.firebaseID);
            return !duplicate;
          });
        });
      });
  }

  deleteAlie(aliesID: string) {
    this.settingList.unsubscribe();
    this.aliesServ
      .deleteAlie(aliesID)
      .then(() => {
        this.nzMessageService.info('Aliado Eliminado');
        let newArray = this.aliesList.filter(
          (item) => item.firebaseID !== aliesID
        );
        this.aliesList = [...newArray];
      })
      .catch((err) => {
        this.nzMessageService.info(
          'Error eliminando el aliado intentalo de nuevo'
        );
        console.log('error deleting alie', err);
      });
  }

  updateStatus(firebaseID, status) {
    this.aliesServ.updateAlly(firebaseID, { status }).then(res => {
      this.nzMessageService.success('Aliado Actualizado');
    }).catch(err => {
      this.nzMessageService.error('Error al actualizar el aliado, inténtelo más tarde');
    });
  }
}
