import { Component, OnInit } from '@angular/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Observable, Observer } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';
// Services
import { RankService } from '../../shared/services/rank.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { VideosService } from '../../shared/services/videos.service';
import { finalize } from 'rxjs/operators';
// models
import { Rank } from '../../shared/models/rank.model';

@Component({
  selector: 'app-upload-videos',
  templateUrl: './upload-videos.component.html',
  styles: [
    `
      [nz-form] {
        max-width: 600px;
      }

      button {
        margin-left: 8px;
      }
    `,
  ],
})
export class UploadVideosComponent implements OnInit {
  validateForm: FormGroup;
  videoID = '';
  finalVideoURL = '';
  loading = '';
  downloadURL$: Observable<string>;
  mainFile: NzUploadFile;
  ranks: Rank[] = [];
  avatarUrl?: string;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private rkServ: RankService,
    private fireBaseSto: AngularFireStorage,
    private fireServ: AngularFirestore,
    private videoServ: VideosService
  ) {
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      link: ['', [Validators.required]],
      position: ['', [Validators.required]],
    });
  }
  ngOnInit(): void {
    this.rkServ
      .getRanks()
      .get()
      .subscribe((res) => {
        res.docs.forEach((doc) => {
          let cuRank: Rank = new Rank();
          cuRank.fireID = doc.id;
          Object.keys(doc.data()).forEach((key) => {
            cuRank[key] = doc.data()[key];
          });
          this.ranks = [...this.ranks, cuRank];
        });
      });
  }

  private getBase64(img: File, callback: (img: string) => void): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result!.toString()));
    reader.readAsDataURL(img);
  }

  submitForm(value: {
    title: string;
    description: string;
    link: string;
    image: string;
    rank: string;
  }): void {
    console.log(value);
    this.uploadImage(value);
  }

  beforeUpload = (file: NzUploadFile) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng =
        file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        this.msg.error('Solo puedes subir archivos JPG o PNG!');
        observer.complete();
        return;
      }
      const isLt1M = file.size / 1024 / 1024 < 2;
      if (!isLt1M) {
        this.msg.error('La imagen debe pesar menos de 1MB!');
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt1M);
      observer.complete();
      this.mainFile = file;
      console.log(file);
    });
  };

  uploadImage(formObject) {
    const file = this.mainFile;
    const filePath = `video_thumbs/${this.fireServ.createId()}`;
    const fileRef = this.fireBaseSto.ref(filePath);
    const task = this.fireBaseSto.upload(filePath, file);
    return task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL$ = fileRef.getDownloadURL();
          this.downloadURL$.subscribe((url) => {
            if (url) {
              this.loading = '';
              this.uploadVideo(formObject, url);
            }
          });
        })
      )
      .subscribe((url) => {
        if (url) {
          this.loading = 'Cargando...';
        }
      });
  }
  uploadVideo(formObject, urlThumb) {
    formObject['image'] = urlThumb;
    this.videoServ
      .setVideo(formObject)
      .then((res) => {
        console.log(`video create succesfully`);
      })
      .catch((err) => {
        console.error(`error creating video`, err);
      });
  }
}
