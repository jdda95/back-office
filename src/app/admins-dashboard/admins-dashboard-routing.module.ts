import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoGeneralComponent } from './info-general/info-general.component';
import { UploadVideosComponent } from './upload-videos/upload-videos.component';
import { SchoolVideosComponent } from './school-videos/school-videos.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UploadAlliesComponent } from './upload-allies/upload-allies.component';
import { CountriesComponent } from './countries/countries.component';
import { AlliesListComponent } from './allies-list/allies-list.component';
import { AdminUsersListComponent } from './admin-users-list/admin-users-list.component';
import { AddAdminUsersComponent } from './add-admin-users/add-admin-users.component';
import { ListCountriesComponent } from './list-countries/list-countries.component';
import { AddCitiesComponent } from './add-cities/add-cities.component';
import { ListCitiesComponent } from './list-cities/list-cities.component';
import { CreateDirectorComponent } from './create-director/create-director.component';
import { ListDirectorsComponent } from './list-directors/list-directors.component';
import { FinanRequestComponent } from './finan-request/finan-request.component';

const routes: Routes = [
  {
    path: 'info-general',
    component: InfoGeneralComponent,
    data: {
      title: 'Información General',
      path: 'admin/info-general',
    },
  },
  {
    path: 'videos',
    component: SchoolVideosComponent,
    data: {
      title: 'Videos escuela de empresarios',
      path: 'admin/videos',
    },
  },
  {
    path: 'videos/agregar',
    component: UploadVideosComponent,
    data: {
      title: 'Subir video',
      path: 'admin/videos/agregar',
    },
  },
  {
    path: 'usuarios',
    component: UsersListComponent,
    data: {
      title: 'Usuarios',
      path: 'admin/usuarios',
    },
  },
  {
    path: 'aliados/agregar',
    component: UploadAlliesComponent,
    data: {
      title: 'Agregar Aliados',
      path: 'admin/aliados/agregar',
    },
  },
  {
    path: 'aliados',
    component: AlliesListComponent,
    data: {
      title: 'Aliados',
      path: 'admin/aliados',
    },
  },
  {
    path: 'paises/agregar',
    component: CountriesComponent,
    data: {
      title: 'Agregar Países',
      path: 'admin/paises/agregar',
    },
  },
  {
    path: 'paises',
    component: ListCountriesComponent,
    data: {
      title: 'Países',
      path: 'admin/paises',
    },
  },
  {
    path: 'zonas/agregar',
    component: AddCitiesComponent,
    data: {
      title: 'Agregar zonas',
      path: 'admin/zonas/agregar',
    },
  },
  {
    path: 'zonas',
    component: ListCitiesComponent,
    data: {
      title: 'Zonas',
      path: 'admin/zonas',
    },
  },
  {
    path: 'usuarios-administrativos',
    component: AdminUsersListComponent,
    data: {
      title: 'Usuarios Administrativos',
      path: 'admin/usuarios-administrativos',
    },
  },
  {
    path: 'usuarios-administrativos/agregar',
    component: AddAdminUsersComponent,
    data: {
      title: 'Usuarios Administrativos',
      path: 'admin/usuarios-administrativos/agregar',
    },
  },
  {
    path: 'directores',
    component: ListDirectorsComponent,
    data: {
      title: 'Lista directores',
      path: 'admin/directores',
    },
  },
  {
    path: 'directores/agregar',
    component: CreateDirectorComponent,
    data: {
      title: 'Crear director',
      path: 'admin/directores/agregar',
    },
  },
  {
    path: 'transferencias/solicitudes',
    component: FinanRequestComponent,
    data: {
      title: 'Solicitudes Usuarios',
      path: 'admin/transferencias/solicitudes',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminsDashboardRoutingModule {}
