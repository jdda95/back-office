import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
//Services
import { ContriesService } from '../../shared/services/contries.service';
import { WalletService } from '../../shared/services/wallet.service';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { AliesService } from '../../shared/services/alies.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { ReferralListService } from '../../shared/services/referral-list.service';
import { AssociateCRUDService } from '../../shared/services/associate-crud.service';
import { GeneralInfoService } from '../../shared/services/general-info.service';
//Models
import { Country } from '../../shared/models/country.model';
import { City } from '../../shared/models/city.model';
import { Alie } from '../../shared/models/alie.model';
import { Associate } from 'src/app/shared/models/associate.model';
import { Observable } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd';
import { MandrillService } from 'src/app/shared/services/mandrill.service';

@Component({
  templateUrl: './sign-up-1.component.html',
  styles: [
    `
      .banner_bg {
        position: relative;
      }
      .banner_bg::after {
        content: '';
        background-color: rgba(0, 0, 0, 0.8);
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
      .banner_bg > * {
        position: relative;
        z-index: 2;
      }
    `,
  ],
})
export class SignUp1Component implements OnInit {
  signUpForm: FormGroup;
  countries$: Observable<Country[]>;
  countries: Country[];
  country: Country;
  cities$: Observable<City[]>;
  cities: City[] = [];
  alies: Alie[];
  selectedValue = null;
  //referral ID
  referralID = null;
  currAssoID = null;
  statusNow = false;
  // See status update of referral data observer and keep the update data variable.
  statusReferral$: Observable<number>;
  statusReferral = 0;
  // variables to update referrals_list
  grandPaList: string = null;
  // errorsFirebase
  weakpassword = false;
  // Form Sended
  regSend = false;
  // set start value links wallets
  linksValue = 0;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private countriesServ: ContriesService,
    private alieServ: AliesService,
    private authServ: AuthenticationService,
    private fireBase: AngularFirestore,
    private walletServ: WalletService,
    private reflistServ: ReferralListService,
    private assoServ: AssociateCRUDService,
    private nzMessageService: NzMessageService,
    private mandrill: MandrillService,
    private infoServ: GeneralInfoService
  ) {
    this.getCountries();
    this.getCities();
    this.getAlies();
    route.params.subscribe((c) => {
      if (c.id !== undefined) {
        this.referralID = c.id;
        this.getFatherData(this.referralID);
        console.log(c.status);
        if (c.status !== undefined) {
          this.statusNow = true;
          console.log(this.statusNow);
        }
      }
    });
    this.infoServ
      .getInfo()
      .get()
      .subscribe((data) => {
        data.docs.forEach((doc) => {
          this.linksValue = doc.data()['linkBudget'];
        });
      });
  }

  ngOnInit(): void {
    this.signUpForm = this.fb.group({
      nickName: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      password: ['', [Validators.required]],
      repassword: ['', [Validators.required, this.confirmationValidator]],
      dni: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]],
      address: ['', [Validators.required]],
      country: ['', [Validators.required]],
      cityID: ['', [Validators.required]],
      alieID: ['', [Validators.required]],
      parent: [this.referralID],
      date: new Date(),
    });

    this.statusReferral$ = this.reflistServ.getStatusChanges$();
    this.statusReferral$.subscribe((cStatus) => {
      this.toMembSelector(cStatus);
    });

    this.countries$ = this.countriesServ.getCountries$();
    this.countries$.subscribe((countries) => {
      this.getCountries(countries);
    });
    this.cities$ = this.countriesServ.getCities$();
    this.cities$.subscribe((cities) => {
      this.getCities(cities);
    });
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.signUpForm.controls.repassword.updateValueAndValidity()
    );
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.signUpForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  submitForm(): void {
    this.regSend = true;
    for (const i of Object.keys(this.signUpForm.controls)) {
      this.signUpForm.controls[i].markAsDirty();
      this.signUpForm.controls[i].updateValueAndValidity();
    }
    this.authServ
      .createUser(this.signUpForm.value, this.statusNow)
      .then((res) => {
        this.currAssoID = res.fireId;
        this.createWallets(res);
        this.mandrill.sendFinanMail().subscribe();
      })
      .catch((err) => {
        this.regSend = false;
        if (err.code === 'auth/weak-password') {
          this.nzMessageService.create(
            'error',
            'Contraseña muy debil. Intenta con otra contraseña.'
          );
        } else if (err.code === 'auth/email-already-in-use') {
          this.nzMessageService.create(
            'error',
            `<b>${this.signUpForm.controls.email.value}</b> ya se encuentra en uso. Intenta con otro correo electrónico.`
          );
        } else if (err.code === 'auth/internal-error') {
          this.nzMessageService.create(
            'error',
            'Error inesperado del sistema. Intentalo nuevamente más tarde.'
          );
        } else if (err.code === 'auth/invalid-email') {
          this.nzMessageService.create('error', 'El correo no es válido.');
        }
      });
  }

  getCountries(countries?: Country[]) {
    if (this.countriesServ.countries.length > 0) {
      this.countries = this.countriesServ.countries;
      const seen = new Set();
      this.cities = this.cities.filter((el) => {
        const duplicate = seen.has(el.id);
        seen.add(el.id);
        return !duplicate;
      });
    } else {
      // console.log(` /// No country data`);
      this.countriesServ.getContries();
    }
    this.infoServ
      .getInfo()
      .get()
      .subscribe((data) => {
        data.docs.forEach((doc) => {
          this.linksValue = doc.data()['linkBudget'];
        });
      });
  }

  getCities(cities?: City[]) {
    if (this.countriesServ.cities.length > 0) {
      this.cities = this.countriesServ.cities;
      const seen = new Set();
      this.cities = this.cities.filter((el) => {
        const duplicate = seen.has(el.id);
        seen.add(el.id);
        return !duplicate;
      });
    } else {
      // console.log(`/// No city data`);
      this.countriesServ.getContries();
    }
  }

  changeState(event: Country) {
    this.country = event;
  }
  getAlies() {
    this.alieServ
      .getAlies()
      .snapshotChanges()
      .subscribe((data) => {
        this.alies = [];
        data.forEach((alie) => {
          let cAlie: Alie = new Alie();
          cAlie.firebaseID = alie.payload.doc.id;
          Object.keys(alie.payload.doc.data()).forEach((key) => {
            cAlie[key] = alie.payload.doc.data()[key];
          });
          this.alies.push(cAlie);
        });
      });
  }
  createWallets(associate: Associate) {
    let wallets: Object[] = [];
    let wLinks = {
      stringID: 'walletLinks',
      docID: this.fireBase.createId(),
      owner: associate['fireId'],
      type: 1,
      name: 'Vinculaciones',
      max_balance: this.linksValue,
      current_balance: this.linksValue,
      movementsList: '',
      date: new Date(),
    };
    wallets.push(wLinks);

    let w247 = {
      stringID: 'wallet247',
      docID: this.fireBase.createId(),
      owner: associate['fireId'],
      type: 2,
      name: 'Billetera 24/7',
      max_balance: 0,
      current_balance: 0,
      movementsList: '',
      date: new Date(),
    };
    wallets.push(w247);

    let wMemebership = {
      stringID: 'walletMembership',
      docID: this.fireBase.createId(),
      owner: associate['fireId'],
      type: 3,
      name: 'Membresía',
      max_balance: 0,
      current_balance: 0,
      movementsList: '',
      date: new Date(),
    };
    wallets.push(wMemebership);

    let wResidual = {
      stringID: 'walletResidual',
      docID: this.fireBase.createId(),
      owner: associate['fireId'],
      type: 4,
      name: 'Residual',
      max_balance: 0,
      current_balance: 0,
      movementsList: '',
      date: new Date(),
    };
    wallets.push(wResidual);
    let wDevelompment = {
      stringID: 'walletDevelopment',
      docID: this.fireBase.createId(),
      owner: associate['fireId'],
      type: 5,
      name: 'Desarrollo',
      max_balance: 0,
      current_balance: 0,
      movementsList: '',
      date: new Date(),
    };
    wallets.push(wDevelompment);

    this.walletServ
      .createAssociateWallets(wallets)
      .then((res) => {
        this.setAssociate(wallets, associate);
      })
      .catch((err) => {
        console.error('Erros seting wallets', err);
      });
  }
  setAssociate(walletsData: Object[], associate: Object) {
    associate['rankID'] = 'rk_asso';
    associate['rankName'] = 'Afiliado';
    associate['status'] = false;
    walletsData.forEach((wallet) => {
      associate[wallet['stringID']] = wallet['docID'];
    });
    associate['referrals_list'] = this.fireBase.createId();
    //    associate to generic object to insert
    let finalAssociate = new Associate().deserialize(associate);
    this.authServ
      .createAssociate(finalAssociate)
      .then((res) => {
        // create simple referral object
        if (this.referralID !== null) {
          let newassociateData = {
            // 0-> no referrals yet, 1->has referrals
            assoID: this.currAssoID,
            haschilds: 0,
            id_referral: this.referralID,
            name: associate['fullName'],
            nickname: associate['nickName'],
            phone: associate['phone'],
            profilePic: 'assets/images/avatars/generic.png',
            rank_name: 'Afiliado',
            rank_ID: 'rk_asso',
            referrals_list: associate['referrals_list'],
            status: false,
            date: new Date(),
          };
          this.updateReferralList(newassociateData);
        } else {
          this.router.navigate(['/seleccionar-membresia', this.currAssoID]);
        }
      })
      .catch((err) => {
        console.error(`Error creating associate - ${err}`);
      });
  }
  // add the creating son to referral father
  updateReferralList(newassociateData: Object) {
    this.reflistServ.setReferralList(newassociateData);
  }
  toMembSelector(status: number) {
    if (status === 1 && this.grandPaList === null) {
      this.router.navigate([
        '/seleccionar-membresia',
        this.currAssoID,
        this.referralID,
      ]);
    } else if (status === 1 && this.grandPaList !== null) {
      this.updateGrandPaList(this.grandPaList, this.referralID);
    } else if (status === 2) {
      console.error('Error from component');
    } else if (status == 3 && this.currAssoID !== null) {
      this.router.navigate([
        '/seleccionar-membresia',
        this.currAssoID,
        this.referralID,
      ]);
    }
  }
  // retrieve father or  referral data
  getFatherData(fatherID: string) {
    this.assoServ
      .getAssociateData(fatherID)
      .snapshotChanges()
      .subscribe((father) => {
        if (father.payload.data()['parent'] !== null) {
          this.getGrandpaData(father.payload.data()['parent']);
        }
      });
  }
  // retreive grandpa referrals_list to simplfye three draw on dashboard
  getGrandpaData(grandID: string) {
    this.assoServ
      .getAssociateData(grandID)
      .snapshotChanges()
      .subscribe((grandPa) => {
        this.grandPaList = grandPa.payload.data()['referrals_list'];
      });
  }
  //  update granpa son's to has childs
  updateGrandPaList(listID: string, sonID: string) {
    this.reflistServ.updateReferralList(listID, sonID);
  }
}
