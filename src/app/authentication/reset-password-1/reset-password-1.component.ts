import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  templateUrl: './reset-password-1.component.html',
})
export class ResetPasswordComponent {
  resetPasswordForm: FormGroup;
  resetPassActive = true;
  constructor(private fb: FormBuilder, private afAuth: AngularFireAuth) {}

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit(): void {
    this.resetPasswordForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  submitForm(): void {
    // tslint:disable-next-line: forin
    for (const i in this.resetPasswordForm.controls) {
      this.resetPasswordForm.controls[i].markAsDirty();
      this.resetPasswordForm.controls[i].updateValueAndValidity();
    }
    if (this.resetPasswordForm.valid) {
      this.resetPasswordInit(this.resetPasswordForm.controls.email.value)
        .then((resp) => {
          console.log(resp);

          console.log('sent!');
          this.resetPassActive = !this.resetPassActive;
        })
        .catch((error) => console.log('failed to send', error));
    }
  }

  resetPasswordInit(email: string) {
    return this.afAuth.sendPasswordResetEmail(email);
  }
}
