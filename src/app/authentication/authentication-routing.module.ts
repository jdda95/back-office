import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Login1Component } from './login-1/login-1.component';
import { Login3Component } from './login-3/login-3.component';
import { SignUp1Component } from './sign-up-1/sign-up-1.component';
import { ResetPasswordComponent } from './reset-password-1/reset-password-1.component';

const routes: Routes = [
  {
    path: 'login',
    component: Login1Component,
    data: {
      title: 'Login 1',
    },
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent,
    data: {
      title: 'Recuperar contraseña',
    },
  },
  {
    path: 'login-3',
    component: Login3Component,
    data: {
      title: 'Login 3',
    },
  },
  {
    path: 'registrate/:id',
    component: SignUp1Component,
    data: {
      title: 'Registro referido',
    },
  },
  {
    path: 'registrate/:id/:status',
    component: SignUp1Component,
    data: {
      title: 'Registro referido',
    },
  },
  {
    path: 'registrate',
    component: SignUp1Component,
    data: {
      title: 'Registro',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
