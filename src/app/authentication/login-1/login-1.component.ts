import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
//Services
import { AuthenticationService } from '../../shared/services/authentication.service';
import { AssociateCRUDService } from '../../shared/services/associate-crud.service';

@Component({
  templateUrl: './login-1.component.html',
  styleUrls: ['./login-1.component.scss'],
})
export class Login1Component implements OnInit {
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authServ: AuthenticationService,
    private router: Router,
    private nzMessageService: NzMessageService,
    private assoServ: AssociateCRUDService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }

  submitForm(): void {
    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty();
      this.loginForm.controls[i].updateValueAndValidity();
    }
    this.authServ
      .login(this.loginForm.value)
      .then((res) => {
        this.changeRoute(res);
      })
      .catch((err) => {
        console.log(err);
        if (err.code === 'auth/wrong-password') {
          this.nzMessageService.create(
            'error',
            `Correo o contraseña incorrectos.`
          );
        } else if (err.code === 'auth/user-not-found') {
          this.nzMessageService.create(
            'error',
            `El usuario con el correo <b>${this.loginForm.controls.email.value}</b> no se encuentra registrado.`
          );
        } else if (err.code === 'auth/invalid-email') {
          this.nzMessageService.create(
            'error',
            `El correo ingresado no es válido.`
          );
        }

        console.error('Error Login', err);
      });
  }

  changeRoute(res) {
    let profile = '';
    res.user.getIdTokenResult().then((idTokenResult) => {
      switch (idTokenResult.claims.role) {
        case 1:
          console.log(`Operativo`);
          profile = 'op';
          this.router.navigate([`dashboard-admin/${profile}`]);
          break;
        case 2:
          console.log(`Administrativo`);
          profile = 'adm';
          this.router.navigate([`dashboard-admin/${profile}`]);
          break;
        case 3:
          console.log(`Relaciones públicas`);
          profile = 'rel';
          this.router.navigate([`dashboard-admin/${profile}`]);
          break;
        case 4:
          console.log(`Nacional`);
          this.evaluateStatus(res);
          // profile = 'nal';
          // this.router.navigate([`dashboard`]);
          break;
        case 5:
          console.log(`Zonal`);
          this.evaluateStatus(res);
          // profile = 'zon';
          // this.router.navigate([`dashboard`]);
          break;
        case 6:
          console.log(`marketing`);
          this.evaluateStatus(res);
          // profile = 'mrk';
          // this.router.navigate([`dashboard`]);
          break;
        default:
          this.evaluateStatus(res);
          // console.log(`none claims login normal user`);
          // this.router.navigate([`dashboard`]);
          break;
      }
    });
  }
  evaluateStatus(res) {
    this.assoServ
      .getAssociateData(res.user.uid)
      .get()
      .forEach((asso) => {
        if (asso.get('status') == true) {
          this.router.navigate([`dashboard`]);
        } else {
          console.log(
            `el usuario con correo ${res.user.email} no se encuentra activo`
          );
        }
      });
  }
}
