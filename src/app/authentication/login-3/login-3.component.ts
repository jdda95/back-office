import { Component } from '@angular/core'
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';


@Component({
    templateUrl: './login-3.component.html'
})

export class Login3Component {
    loginForm: FormGroup;

    submitForm(): void {
        // tslint:disable-next-line: forin
        for (const i in this.loginForm.controls) {
            this.loginForm.controls[ i ].markAsDirty();
            this.loginForm.controls[ i ].updateValueAndValidity();
        }
    }

    constructor(private fb: FormBuilder) {
    }

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit(): void {
        this.loginForm = this.fb.group({
            userName: [ null, [ Validators.required ] ],
            password: [ null, [ Validators.required ] ]
        });
    }
}