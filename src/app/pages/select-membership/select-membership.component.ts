import { Component, Input } from '@angular/core';
import { ThemeConstantService } from 'src/app/shared/services/theme-constant.service';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { Router, ActivatedRoute } from '@angular/router';
// FireBase Service and Services
import { AngularFirestore } from '@angular/fire/firestore';
import { MembershipsService } from '../../shared/services/memberships.service';
//Model
import { Membership } from '../../shared/models/membership.model';

@Component({
  selector: 'app-select-membership',
  templateUrl: './select-membership.component.html',
  styles: [],
})
export class SelectMembershipComponent {
  // THEME COLORS
  themeColors = this.colorConfig.get().colors;
  blue = this.themeColors.blue;
  gray = this.themeColors.grayLight;
  membershipsList: Membership[];
  isVisible = false;
  isOkLoading = false;
  // Associate and Referral ID
  associateID = '';
  referralID = '';

  selectedMembership = false;
  selectedMembershipID = '';

  constructor(
    private colorConfig: ThemeConstantService,
    private modal: NzModalService,
    private router: Router,
    private route: ActivatedRoute,
    private fireBase: AngularFirestore,
    private memberServ: MembershipsService
  ) {
    route.params.subscribe((c) => {
      if (c.myid !== undefined) {
        this.associateID = c.myid;
      }
      if (c.refid !== undefined) {
        this.referralID = c.refid;
      }
    });
    this.getMemberships();
  }

  getMemberships() {
    this.memberServ
      .getMemberships()
      .snapshotChanges()
      .subscribe((data) => {
        this.membershipsList = [];
        data.forEach((membership) => {
          const cMemb: Membership = new Membership();
          cMemb.fireID = membership.payload.doc.id;
          cMemb.private = membership.payload.doc.data()['private'];
          cMemb.name = membership.payload.doc.data()['name'];
          cMemb.price = membership.payload.doc.data()['price'];
          cMemb.image = membership.payload.doc.data()['image'];
          cMemb.message = membership.payload.doc.data()['message'];
          this.membershipsList.push(cMemb);
        });
      });
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isOkLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isOkLoading = false;
    }, 3000);
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  createModal(title: string, message: string, fireId: string): void {
    this.modal.create({
      nzTitle: title,
      nzContent: NzModalCustomComponent,
      nzGetContainer: () => document.body,
      nzComponentParams: {
        title,
        message,
      },
      nzClosable: false,
      nzOnOk: () =>
        new Promise((resolve) => {
          this.selectedMembership = true;
          this.selectedMembershipID = fireId;

          this.memberServ
            .updateUserMemebership(this.associateID, this.selectedMembershipID)
            .then((res) => {
              this.router.navigate(['/dashboard']);
            })
            .catch((err) => {
              console.error('Error updating membership', err);
            });
          setTimeout(resolve, 1000);
        }),
    });
  }
  createModalActivation(): void {
    this.modal.create({
      nzTitle: 'Bienvenido a 24/7',
      nzContent: NzModalCustomComponent,
      nzGetContainer: () => document.body,
      nzComponentParams: {
        message: 'Tu usuario está en tramite de aprobación.',
      },
      nzClosable: false,
      nzOnOk: () =>
        new Promise((resolve) => {
          window.location.href =
            'https://www.empresarios24-7.com/office/auth/login';
          setTimeout(resolve, 1000);
        }),
    });
  }
}

@Component({
  selector: 'app-modal-template',
  template: `
    <div>
      <p>
        <span>{{ message }}</span>
      </p>
    </div>
  `,
})
export class NzModalCustomComponent {
  @Input() title: string;
  @Input() message: string;

  constructor(private modal: NzModalRef) {}

  destroyModal(): void {
    this.modal.destroy({ data: 'this the result data' });
  }

  handleOk(): void {
    this.modal.triggerOk;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
  }
}
