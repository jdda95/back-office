import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { SelectMembershipComponent } from './select-membership/select-membership.component';

const routes: Routes = [
  {
    path: 'contacto',
    component: ContactComponent,
    data: {
      title: 'Contáctanos',
    },
  },
  {
    path: 'seleccionar-membresia/:myid',
    component: SelectMembershipComponent,
    data: {
      title: 'Seleccionar Membresía',
    },
  },
  {
    path: 'seleccionar-membresia/:myid/:refid',
    component: SelectMembershipComponent,
    data: {
      title: 'Seleccionar Membresía Con El que Vincularás a tu Afiliado',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
