import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MandrillService } from 'src/app/shared/services/mandrill.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { Router } from '@angular/router';
import { ContriesService } from 'src/app/shared/services/contries.service';
import { Country } from 'src/app/shared/models/country.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styles: [
    `
      .banner_bg {
        position: relative;
      }
      .banner_bg::after {
        content: '';
        background-color: rgba(0, 0, 0, 0.8);
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
      .banner_bg > * {
        position: relative;
        z-index: 2;
      }
    `,
  ],
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  countries: Country[];
  CUcountryEmail: string;
  selectedValue = null;
  contactSend = false;
  countries$: Observable<Country[]>;

  constructor(
    private fb: FormBuilder,
    private authServ: AuthenticationService,
    private router: Router,
    private countriesServ: ContriesService,
    private mandrill: MandrillService
  ) {}

  ngOnInit(): void {
    this.getCountries();
    this.countries$ = this.countriesServ.getCountries$();
    this.countries$.subscribe((countries) => {
      this.getCountries(countries);
    });
    this.authServ.hasUserB().then((user) => {
      if (user !== null) {
        this.changeRoute(user);
      }
    });
    this.contactForm = this.fb.group({
      fullname: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      country: ['', [Validators.required]],
      message: [''],
    });
  }

  changeRoute(user) {
    let profile = '';
    user.getIdTokenResult().then((idTokenResult) => {
      switch (idTokenResult.claims.role) {
        case 1:
          profile = 'op';
          this.router.navigate([`dashboard-admin/${profile}`]);
          break;
        case 2:
          profile = 'adm';
          this.router.navigate([`dashboard-admin/${profile}`]);
          break;
        case 3:
          profile = 'rel';
          this.router.navigate([`dashboard-admin/${profile}`]);
          break;
        case 4:
          profile = 'nal';
          this.router.navigate([`dashboard`]);
          break;
        case 5:
          profile = 'zon';
          this.router.navigate([`dashboard`]);
          break;
        case 6:
          profile = 'mrk';
          this.router.navigate([`dashboard`]);
          break;
        default:
          this.router.navigate([`dashboard`]);
          break;
      }
    });
  }

  getCountries(countries?: Country[]) {
    if (this.countriesServ.countries.length > 0) {
      this.countries = this.countriesServ.countries;
    } else {
      this.countriesServ.getContries();
    }
  }

  changeValue($event) {
    let countrySet = this.countries.find((country) => country.name == $event);
    if (
      countrySet.mrkDirectorMail === '' ||
      countrySet.mrkDirectorMail === undefined
    ) {
      this.CUcountryEmail = 'ceo@empresarios24-7.com';
    } else {
      this.CUcountryEmail = countrySet.mrkDirectorMail;
    }
  }

  submitForm(): void {
    this.contactSend = true;
    for (const i of Object.keys(this.contactForm.controls)) {
      this.contactForm.controls[i].markAsDirty();
      this.contactForm.controls[i].updateValueAndValidity();
    }
    if (this.contactForm.valid) {
      // SEND CONTACT
      this.mandrill
        .sendContactMessage(
          this.contactForm.controls.email.value,
          this.contactForm.controls.fullname.value,
          {
            NAME: this.contactForm.controls.fullname.value,
            COUNTRY: this.contactForm.controls.country.value,
          }
        )
        .subscribe((res) => {
          console.log(res[0].status);
          if (res[0].status === 'sent') {
            this.mandrill
              .sendDirectorNatNotification(
                this.CUcountryEmail,
                'Director Nacional',
                {
                  NAME: this.contactForm.controls.fullname.value,
                  PHONE: this.contactForm.controls.phone.value,
                  EMAIL: this.contactForm.controls.email.value,
                }
              )
              .subscribe((res) => console.log(res));
            this.contactForm.reset();
          }
        });
    } else {
      this.contactSend = false;
    }
  }
}
