import { AfterViewChecked, Component, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ThemeConstantService } from 'src/app/shared/services/theme-constant.service';
//Services
import { AuthenticationService } from '../shared/services/authentication.service';
import { AssociateCRUDService } from '../shared/services/associate-crud.service';
import { TokeAndMovementsService } from '../shared/services/toke-and-movements.service';
// Models
import { Wallet } from '../shared/models/wallet.model';
import { Movement } from '../shared/models/movement.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-wallets',
  templateUrl: './wallets.component.html',
  styles: [],
})
export class WalletsComponent implements OnInit, OnChanges, AfterViewChecked {
  themeColors = this.colorConfig.get().colors;
  orange = this.themeColors.orange;

  wallet: Wallet = new Wallet();
  wallets: Wallet[] = [];

  wallets$: Observable<Wallet[]>;
  currentUser: string;

  movementsList: Movement[] = [];
  listIsSet = false;
  loading = true;
  idWallet: any;

  constructor(
    private colorConfig: ThemeConstantService,
    private route: ActivatedRoute,
    private assoCRUDServ: AssociateCRUDService,
    private authServ: AuthenticationService,
    private tokeAndMoveServ: TokeAndMovementsService
  ) {}

  ngOnInit(): void {
    this.listIsSet = false;
    let aux = true;
    this.authServ.hasUser().forEach((user) => {
      if (aux) {
        if (user !== null) {
          // change on production use only for localhost
          this.currentUser = user.uid;
          // this.currentUser = user.fireId;
          this.createArrayWallets(this.currentUser);
        } else {
          console.log('empty user');
        }
        aux = false;
      }
    });
    this.wallets$ = this.assoCRUDServ.getAssociateWallets$();
    this.wallets$.subscribe((newData) => {
      this.wallets = newData;
      console.log(this.wallets);

      this.createArrayWallets();
    });
  }

  createArrayWallets(userID?: string) {
    if (this.assoCRUDServ.wallets.length <= 0) {
      // console.log('not wallets');
      this.assoCRUDServ.getAssociateWallets(userID);
      this.route.params.subscribe((c) => {
        this.wallet = this.getWallet(c.id);
        this.idWallet = c.id;
      });
    } else {
      // console.log('already wallets');
      this.wallets = this.assoCRUDServ.wallets;
      this.route.params.subscribe((c) => {
        this.wallet = this.getWallet(c.id);
        this.idWallet = c.id;
      });
    }
    if (this.wallet !== undefined && this.listIsSet === false) {
      this.tokeAndMoveServ.getMovementList(this.wallet.fireID).then((res) => {
        this.movementsList = res;
        this.loading = false;
      });
      this.listIsSet = true;
      this.loading = false;
    }
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    this.route.params.subscribe((c) => {
      if (this.idWallet !== c.id) {
        this.loading = true;
        this.wallet = this.getWallet(c.id);
        this.listIsSet = false;
        let aux = true;
        this.authServ.hasUser().forEach((user) => {
          if (aux) {
            if (user !== null) {
              // change on production use only for localhost
              this.currentUser = user.uid;
              // this.currentUser = user.fireId;
              this.createArrayWallets(this.currentUser);
            } else {
              console.log('empty user');
            }
            aux = false;
          }
        });
      }
    });
  }

  ngOnChanges(): void {
    console.log('Cambio');
  }

  getWallet(id: number): Wallet {
    let wallet;
    this.wallets.filter((x) => {
      if (x.type == id) {
        wallet = x;
        console.log(wallet);
      }
    });
    return wallet;
  }

  getMovements(walletID) {
    this.tokeAndMoveServ.getMovementList(walletID);
  }
}
