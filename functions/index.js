// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require("firebase-functions");

const companyWalletConst = "Z38c35fpGkDHymC8yri7";
const BalanceCeoConst = "q3lQBztROf3LEPhRQbRU";
const finanBalanceConst = "ZHqeL3gr9sCKhAOdSdIH";
const PRBalanceConst = "ZKV3Wvrp8LnCAoxUpDZm";

// The Firebase Admin SDK to access Cloud Firestore.
const admin = require("firebase-admin");
const { timeStamp } = require("console");
admin.initializeApp();
exports.getUpdates = functions.firestore
  .document("/referrals_list/{documentId}/my_referr/{documentId2}")
  .onCreate(async (snapshot, context) => {
    getReferralsCount(context.params.documentId);
  });
//update admin claims
exports.setAdminClaimsUp = functions.firestore
  .document(`administrators/{documentId}`)
  .onUpdate(async (snapshot, context) => {
    Object.keys(snapshot.after.data()).forEach((key) => {
      // functions.logger.log(`data fields ${snapshot.data()[key]}`);
      if (key === "position") {
        // functions.logger.log(`user ID ${context.params.documentId}`);
        admin
          .auth()
          .setCustomUserClaims(context.params.documentId, {
            role: snapshot.after.data()[key],
          })
          .then(() => {
            functions.logger.log(
              `${snapshot.before.data()[key]} claim set to ${
                context.params.documentId
              }`
            );
            return "ok";
          })
          .catch((err) => {
            functions.logger.error(`error setting admin claims ${err}`);
            throw err;
          });
      }
    });
  });
// set claims to nat and mrk directos
exports.setNatMrkDirClaims = functions.firestore
  .document(`countries/{countryID}`)
  .onUpdate((snapshot, context) => {
    // National
    if (
      snapshot.before.data()["natDirectorUID"] === null ||
      (snapshot.before.data()["natDirectorUID"] === "" &&
        snapshot.after.data()["natDirectorUID"] !== "")
    ) {
      // assign claim to new director
      admin
        .auth()
        .setCustomUserClaims(snapshot.after.data()["natDirectorUID"], {
          role: 4,
        })
        .then(() => {
          functions.logger.log(
            `${
              snapshot.after.data()["natDirectorUID"]
            } as national director role:4 assigned `
          );
          return true;
        })
        .catch((err) => {
          functions.logger.error(
            `UId-> ${
              snapshot.after.data()["natDirectorUID"]
            } - error setting role:4 claim -> ${err}`
          );
          throw err;
        });
    } else if (
      snapshot.before.data()["natDirectorUID"] !== "" &&
      snapshot.after.data()["natDirectorUID"] !== "" &&
      snapshot.before.data()["natDirectorUID"] !==
        snapshot.after.data()["natDirectorUID"]
    ) {
      // remove claim to before director
      admin
        .auth()
        .setCustomUserClaims(snapshot.before.data()["natDirectorUID"], {
          role: null,
        })
        .then(() => {
          // assign claim to new director
          admin
            .auth()
            .setCustomUserClaims(snapshot.after.data()["natDirectorUID"], {
              role: 4,
            })
            .then(() => {
              functions.logger.log(
                `${snapshot.after.data()["natDirectorUID"]} role:4 assigned `
              );
              return true;
            })
            .catch((err) => {
              functions.logger.error(
                `error setting role:4 claim to new director er: ${err}`
              );
              return false;
            });
          return "ok";
        })
        .catch((err) => {
          functions.logger.error(
            `error deleting national role:4 claim er: ${err}`
          );
          throw err;
        });
    }

    // Markenting
    if (
      snapshot.before.data()["mrkDirectorUID"] === null ||
      (snapshot.before.data()["mrkDirectorUID"] === "" &&
        snapshot.after.data()["mrkDirectorUID"] !== "")
    ) {
      admin
        .auth()
        .setCustomUserClaims(snapshot.after.data()["mrkDirectorUID"], {
          role: 6,
        })
        .then(() => {
          functions.logger.log(
            `${
              snapshot.after.data()["mrkDirectorUID"]
            } as marketing director role:6 assigned `
          );
          return true;
        })
        .catch((err) => {
          functions.logger.error(
            `error setting marketing role:6 claim er: ${err}`
          );
          return false;
        });
    } else if (
      snapshot.before.data()["mrkDirectorUID"] !== "" &&
      snapshot.after.data()["mrkDirectorUID"] !== "" &&
      snapshot.before.data()["mrkDirectorUID"] !==
        snapshot.after.data()["mrkDirectorUID"]
    ) {
      // remove claim to before director
      admin
        .auth()
        .setCustomUserClaims(snapshot.before.data()["mrkDirectorUID"], {
          role: null,
        })
        .then(() => {
          // assign claim to new director
          // eslint-disable-next-line promise/no-nesting
          admin
            .auth()
            .setCustomUserClaims(snapshot.after.data()["mrkDirectorUID"], {
              role: 6,
            })
            .then(() => {
              functions.logger.log(
                `${snapshot.after.data()["mrkDirectorUID"]} role:6 assigned `
              );
              return true;
            })
            .catch((err) => {
              functions.logger.error(
                `error setting role:6 claim to new marketing director er: ${err}`
              );
              return false;
            });
          return "ok";
        })
        .catch((err) => {
          functions.logger.error(
            `error deleting marketing role:6 claim er: ${err}`
          );
          return false;
        });
    }
  });
// set claims to administratos
exports.setAdminClaims = functions.firestore
  .document(`administrators/{documentId}`)
  .onCreate(async (snapshot, context) => {
    Object.keys(snapshot.data()).forEach((key) => {
      // functions.logger.log(`data fields ${snapshot.data()[key]}`);
      if (key === "position") {
        // functions.logger.log(`user ID ${context.params.documentId}`);
        admin
          .auth()
          .setCustomUserClaims(context.params.documentId, {
            role: snapshot.data()[key],
          })
          .then(() => {
            functions.logger.log(
              `${snapshot.data()[key]} claim set to ${
                context.params.documentId
              }`
            );
            return "ok";
          })
          .catch((err) => {
            functions.logger.error(`error setting admin claims ${err}`);
            throw err;
          });
      }
    });
  });
exports.setZoneDirClaims = functions.firestore
  .document("countries/{countryID}/cities/{cityID}")
  .onUpdate((snapshot, context) => {
    // National
    if (
      snapshot.before.data()["directorUID"] === null ||
      (snapshot.before.data()["directorUID"] === "" &&
        snapshot.after.data()["directorUID"] !== "")
    ) {
      // assign claim to new director
      admin
        .auth()
        .setCustomUserClaims(snapshot.after.data()["directorUID"], { role: 5 })
        .then(() => {
          functions.logger.log(
            `${
              snapshot.after.data()["directorUID"]
            } as zonal director role:5 assigned `
          );
          return "ok";
        })
        .catch((err) => {
          functions.logger.error(`error setting role:5 claim er: ${err}`);
          throw err;
        });
    } else if (
      snapshot.before.data()["directorUID"] !== "" &&
      snapshot.after.data()["directorUID"] !== "" &&
      snapshot.before.data()["directorUID"] !==
        snapshot.after.data()["directorUID"]
    ) {
      // remove claim to before director
      admin
        .auth()
        .setCustomUserClaims(snapshot.before.data()["directorUID"], {
          role: null,
        })
        .then(() => {
          // assign claim to new director
          // eslint-disable-next-line promise/no-nesting
          admin
            .auth()
            .setCustomUserClaims(snapshot.after.data()["directorUID"], {
              role: 5,
            })
            .then(() => {
              functions.logger.log(
                `${snapshot.after.data()["directorUID"]} role:5 assigned `
              );
              return "ok";
            })
            .catch((err) => {
              functions.logger.error(
                `error setting role:5 claim to new zone director er: ${err}`
              );
              throw err;
            });
          return "ok";
        })
        .catch((err) => {
          functions.logger.error(
            `error deleting zonal role:5 claim er: ${err}`
          );
          throw err;
        });
    }
  });
// run dispersion
exports.newassodispersion = functions.firestore
  .document(`associates/{documeintId}`)
  .onUpdate(async (snapshot, context) => {
    let propagate = 0;
    let runDispersion = false;
    functions.logger.log(`
    ID user ${context.params.documeintId} just update.
    `);
    Object.keys(snapshot.before.data()).forEach((key) => {
      let before = snapshot.before.data()[key];
      let after = snapshot.after.data()[key];

      if (key === "in_ref_list") {
        before = snapshot.before.data()[key]["path"];
        after = snapshot.after.data()[key]["path"];
      }
      if (key === "date") {
        before = snapshot.before.data()[key]["_seconds"];
        after = snapshot.after.data()[key]["_seconds"];
      }
      // evaluate to run dispersion only if has this cases
      if (before !== after && (key === "status" || key === "membershipID")) {
        runDispersion = true;
      }
    });

    // evaluate id user was activate and membership selected
    if (
      snapshot.before.data()[`status`] !== snapshot.after.data()[`status`] &&
      snapshot.after.data()["status"] === true &&
      snapshot.after.data()[`membershipID`] === "mb_star" &&
      runDispersion === true
    ) {
      propagate = 1;
      functions.logger.log(
        `Star status b-> ${snapshot.before.data()[`status`]} || a-> ${
          snapshot.after.data()[`status`]
        } || new memb-> ${snapshot.after.data()[`membershipID`]}`
      );
      functions.logger.log(
        `start evaluation to promote father || ref_List child -> ${
          snapshot.after.data()[`in_ref_list`]["path"]
        }`
      );
    } else if (
      snapshot.after.data()["status"] === true &&
      (snapshot.before.data()[`membershipID`] !==
        snapshot.after.data()[`membershipID`] ||
        snapshot.before.data()[`membershipID`] ===
          snapshot.after.data()[`membershipID`]) &&
      snapshot.after.data()[`membershipID`] !== "mb_star" &&
      runDispersion === true
    ) {
      propagate = 2;
      functions.logger.log(
        `Business status b-> ${snapshot.before.data()[`status`]} || a-> ${
          snapshot.after.data()[`status`]
        } || b memb-> ${snapshot.before.data()[`membershipID`]} a memb-> ${
          snapshot.after.data()[`membershipID`]
        }`
      );
      functions.logger.log(
        `start evaluation to promote father || ref_List child -> ${
          snapshot.after.data()[`membershipID`]["path"]
        }`
      );
    }
    // switch to select kind of dispersion
    switch (propagate) {
      case 1:
        await starMembDis(snapshot, context)
          .then(async (result) => {
            if (snapshot.after.data()["in_ref_list"] !== undefined) {
              functions.logger.log(
                `start evaluate promotion from change status`
              );
              await getReferralsCount(
                String(snapshot.after.data()["in_ref_list"]["path"]).split(
                  "/"
                )[1]
              );
            } else {
              functions.logger.warn(
                `the user ${
                  snapshot.after.data()["email"]
                } comes without data on in_ref_list`
              );
            }

            return functions.logger.log(`star dispersion OK `);
          })
          .catch((err) => {
            functions.logger.error(`error star dispersion ${err}`);
            throw err;
          });
        break;
      case 2:
        await bussMembDis(snapshot, context)
          .then(async (result) => {
            functions.logger.log(`dispersion from business onwards OK `);
            if (snapshot.after.data()["in_ref_list"] !== undefined) {
              functions.logger.log(
                `start evaluate promotion from change status or membership`
              );
              await getReferralsCount(
                String(snapshot.after.data()["in_ref_list"]["path"]).split(
                  "/"
                )[1]
              );
            } else {
              functions.logger.warn(
                `the user ${
                  snapshot.after.data()["email"]
                } comes without data  on in_ref_list`
              );
            }
            return functions.logger.log(`buss onward dispersion OK `);
          })
          .catch((err) => {
            functions.logger.error(
              `error in dispersion from business onwards ${err}`
            );
            throw err;
          });
        break;
      default:
        functions.logger.log(`change other field`);
        break;
    }
  });
// wrap to work on emulator enviroment collect money for schools change for pub sub
exports.scheduleAliesCollect = functions.pubsub
  .schedule("0 0 1 * *")
  .timeZone("America/Bogota")
  .onRun(async (context) => {
    let aliesList = [];
    let ally = {};
    let families = [];
    let rundispersion = false;

    // get alies data
    const alies = await getAlies();
    alies.forEach((cuAlly) => {
      if (cuAlly.data()["status"]) {
        ally = {
          name: cuAlly.data()["name"],
          id: cuAlly.id,
          rate: cuAlly.data()["rate"],
          count: 0,
        };
        aliesList.push(ally);
      }
    });
    //  getting associates
    const asso = await admin.firestore().collection("associates").get();

    asso.forEach(async (cuAsso) => {
      //  set fee depending of allly
      let fee = 0;
      aliesList.forEach((val, i) => {
        if (val["id"] === cuAsso.data()["alieID"]) {
          fee = val["rate"];
        }
      });

      if (cuAsso.data()["status"] === true) {
        //evaluate if current associate has money to pay
        const getMembWall = await admin
          .firestore()
          .doc(`wallets/${cuAsso.data()["walletMembership"]}`)
          .get();
        const MembBalance = getMembWall.data()["current_balance"];
        const getResWall = await admin
          .firestore()
          .doc(`wallets/${cuAsso.data()["walletResidual"]}`)
          .get();
        const ResBlance = getResWall.data()["current_balance"];
        const getDevWall = await admin
          .firestore()
          .doc(`wallets/${cuAsso.data()["walletDevelopment"]}`)
          .get();
        const DevBalance = getDevWall.data()["current_balance"];

        if (MembBalance >= fee) {
          console.log(`pay with memberships`);
          rundispersion = true;
          // reduce wallet ballance
          await admin
            .firestore()
            .doc(`wallets/${getMembWall.id}`)
            .update({
              current_balance: getMembWall.data()["current_balance"] - fee,
            });
        } else if (ResBlance >= fee) {
          console.log(`pay with residual`);
          rundispersion = true;
          // reduce wallet ballance
          await admin
            .firestore()
            .doc(`wallets/${getResWall.id}`)
            .update({
              current_balance: getResWall.data()["current_balance"] - fee,
            });
        } else if (DevBalance >= fee) {
          console.log(`pay with development`);
          rundispersion = true;
          // reduce wallet ballance
          await admin
            .firestore()
            .doc(`wallets/${getDevWall.id}`)
            .update({
              current_balance: getDevWall.data()["current_balance"] - fee,
            });
        } else {
          console.log(`don't have money set note`);
          if (cuAsso.data()["noPay"] < 3) {
            const upNopay = await admin
              .firestore()
              .doc(`associates/${cuAsso.id}`)
              .update({ noPay: cuAsso.data()["noPay"] + 1 });
          } else {
            const myBatch = admin.firestore().batch();
            myBatch.update(admin.firestore().doc(`associates/${cuAsso.id}`), {
              status: false,
            });
            if (
              cuAsso.data()["in_ref_list"] !== undefined &&
              cuAsso.data()["in_ref_list"] !== ""
            ) {
              myBatch.update(
                admin.firestore().doc(cuAsso.data()["in_ref_list"].path),
                { status: false }
              );
            }
            await myBatch
              .commit()
              .then((res) => {
                functions.logger.info(
                  `user succesfully desactivated -> ${cuAsso.id}`
                );
                return `user succesfully desactivated -> ${cuAsso.id}`;
              })
              .catch((err) => {
                functions.logger.error(
                  `error disactivating user -> ${cuAsso.id}`,
                  err
                );
                throw err;
              });
            // const upNopay = await admin
            //   .firestore()
            //   .doc(`associates/${cuAsso.id}`)
            //   .update({ status: false });
          }
        }

        if (rundispersion) {
          // get the family of each associate with his repective comission
          let family = await getFamilyForAliesDis(cuAsso, aliesList);
          families.push(family);

          Object.keys(cuAsso.data()).forEach((key) => {
            if (key === "alieID") {
              // set number of associates by school to collect money
              for (let i = 0; i < aliesList.length; i++) {
                if (aliesList[i]["id"] === cuAsso.data()[key]) {
                  aliesList[i]["count"] = aliesList[i]["count"] + 1;
                }
              }
            }
          });
          // if I get all families with his comissions update wallets
          // functions.logger.log({
          //   isSame: families.length === asso.size,
          //   assoSize: asso.size,
          //   familiesLength: families.length,
          // });
          if (families.length === asso.size) {
            printFamily(families);
            // update balance in all alies wallets
            aliesList.forEach(async (ally) => {
              console.log(
                `ally -> ${ally["id"]}`,
                ally["rate"] * 0.35 * ally["count"]
              );
              await updateAllyBalance(
                ally["id"],
                ally["rate"] * 0.35 * ally["count"]
              );
            });
          }else{
            console.log(`No son iguales las familias a los asociados, no se cargo. families.length: ${families.length} | asso.size: ${asso.size}`);
          }
          console.log(`associates ok`);
        }
      } else {
        functions.logger.log(`user no active ${cuAsso.id} to do dispersion`);
      }
    });

    // await functions.pubsub.schedule('* * * * *')
    // .timeZone('America/Bogota')
    // .onRun((context) => {
    //   functions.logger.log('This will be run every 1 minute!');
    //   return null;
    // });
  });
// FUNCION PRUEBA FAMILY
exports.getFamilynAssoSize = functions.firestore
  .document("/prueba/{documentId}")
  .onCreate(async (snapshot, context) => {
    let families = [];
    let aliesList = [];
     // get alies data
     const alies = await getAlies();
     alies.forEach((cuAlly) => {
       if (cuAlly.data()["status"]) {
         ally = {
           name: cuAlly.data()["name"],
           id: cuAlly.id,
           rate: cuAlly.data()["rate"],
           count: 0,
         };
         aliesList.push(ally);
       }
     });
    const asso = await admin.firestore().collection("associates").get();
    asso.forEach(async (cuAsso) => {
      aliesList.forEach((val, i) => {
             if (val["id"] === cuAsso.data()["alieID"]) {
               fee = val["rate"];
             }
           });
          })
          let family = await getFamilyForAliesDis(cuAsso, aliesList);
          families.push(family);
          if (families.length == asso.size) {
            printFamily(families);
            // update balance in all alies wallets
            aliesList.forEach(async (ally) => {
              console.log(`ally -> ${ally["id"]}`, ally["rate"] * 0.35 * ally["count"]);
              await updateAllyBalance(ally["id"], ally["rate"] * 0.35 * ally["count"]);
            });
          } else {
            console.log(
              `No son iguales las familias a los asociados, no se cargo. families.length: ${families.length} | asso.size: ${asso.size}`
            );
            functions.logger.log(families);
            functions.logger.log(asso);
          }
 });
function evaluatePromotion(referrerID, counters) {
  // get current rank to evaluate what it's and the next to be promote
  const gerReferralData = admin
    .firestore()
    .doc(`associates/${referrerID}`)
    .get()
    .then((referralData) => {
      let currentRank = "";
      let currentStatus = false;
      let currentMemb = "";
      let devWallet = "";
      Object.keys(referralData.data()).forEach((key) => {
        if (key === "rankID") {
          currentRank = referralData.data()[key];
        }
        if (key === "status") {
          currentStatus = referralData.data()[key];
        }
        if (key === "membershipID") {
          currentMemb = referralData.data()[key];
        }
        if (key === "walletDevelopment") {
          devWallet = referralData.data()[key];
        }
      });

      // console.log(`cur_rank ->${currentRank} || status ->${currentStatus} || membership -> ${currentMemb}`);
      let crosscurrent = false;
      let indexrank = 0;
      let lastCouldBe = 0;
      for (let i = 0; i < counters.length; i++) {
        if (counters[i] !== undefined) {
          //  get current rank to evaluate childs with posterior ranks and if they colaborate to increase my rank
          if (currentRank === counters[i]["curr_rk"]) {
            crosscurrent = true;
            indexrank = i;
          }
          if (crosscurrent) {
            for (let j = i; j < counters.length; j++) {
              if (counters[j].count >= 5) {
                lastCouldBe = j + 1;
              }
            }
          }
        } else {
          functions.logger.log(`no associate in i -> ${i} slot`);
        }
      }
      if (lastCouldBe > indexrank) {
        if (
          currentStatus &&
          counters[lastCouldBe]["memb_req"] === currentMemb
        ) {
          functions.logger.log(
            `promote to  -> ${counters[lastCouldBe]["name"]}, rk-> ${counters[lastCouldBe]["curr_rk"]} || status-> ${currentStatus} and curMemb -> ${currentMemb}`
          );
          functions.logger.log(`referrID ${referrerID}`);
          promoteAssociate(
            referrerID,
            counters[lastCouldBe]["curr_rk"],
            counters[lastCouldBe]["name"],
            currentMemb,
            devWallet,
            counters[lastCouldBe]["com_dev"]
          );
        } else {
          // use money from child's star membership to update to business membership
          if (currentMemb === "mb_star") {
            console.log(
              `update to business memebership from star with childs money comdev-> ${counters[lastCouldBe]["com_dev"]}`
            );
            promoteAssociate(
              referrerID,
              counters[lastCouldBe]["curr_rk"],
              counters[lastCouldBe]["name"],
              counters[lastCouldBe]["memb_req"],
              devWallet,
              counters[lastCouldBe]["com_dev"]
            );
          } else {
            functions.logger.log(
              `could be promote to  -> ${counters[lastCouldBe]["name"]}, rk-> ${counters[lastCouldBe]["curr_rk"]}, but miss status-> ${currentStatus} or curMemb -> ${currentMemb} and need ${counters[lastCouldBe]["memb_req"]} memb`
            );
          }
        }
      } else {
        functions.logger.log(`not enought to be promote`);
      }
      return "ok";
    })
    .catch((err) => {
      functions.logger.error(
        `Error getting referral data ID -> ${referrerID} error-> ${err}`
      );
      throw err;
    });
}
function promoteAssociate(
  assoID,
  rank_ID,
  rank_name,
  membId,
  walletId,
  commission
) {
  const updateRef = admin
    .firestore()
    .doc(`/associates/${assoID}`)
    .update({
      rankID: rank_ID,
      rankName: rank_name,
      status: true,
      membershipID: membId,
    })
    .then((res) => {
      functions.logger.log(`complex asso rank updated`);
      updateSimpleasso(assoID, rank_ID, rank_name);
      // update develompment wallet
      updateWallet(walletId, commission, 5);
      functions.logger.log(`ok promoting complex associate`);
      return true;
    })
    .catch((err) => {
      functions.logger.error(
        `error updating complex asso document ${referrerID} error-> ${err}`
      );
      throw err;
    });
}
function updateSimpleasso(referrerID, rank_ID, rank_name, walletId) {
  // update simple associate data
  getReferralListDoc = admin
    .firestore()
    .collectionGroup("my_referr")
    .where(`assoID`, `==`, referrerID)
    .get()
    .then((querySnapshot) => {
      // functions.logger.log(`number of documents ${querySnapshot.size} with ${referrerID}`);
      querySnapshot.docs.forEach((simpleAsso) => {
        const updateSimpleAsso = admin
          .firestore()
          .doc(simpleAsso.ref.path)
          .update({ rank_ID: rank_ID, rank_name: rank_name, status: true })
          .then((res) => {
            functions.logger.log(
              `simple asso updated succesfully ${simpleAsso.ref.path} with ${res.writeTime.seconds}`
            );
            return true;
          })
          .catch((err) => {
            functions.logger.error(
              `error updating document ${simpleAsso.ref} error-> ${err}`
            );
            throw err;
          });
      });
      functions.logger.log(`ok getting simple associate data`);
      return true;
    })
    .catch((err) => {
      throw err;
    });
}
// run dispersion when activate star membership
async function starMembDis(snapshot, context) {
  try {
    let parenId = "";
    let membershipID = "";
    let countryID = "";
    let cityID = "";
    let linkBudget = 0;
    let matrixBudget = 0;
    Object.keys(snapshot.after.data()).forEach((key) => {
      switch (key) {
        case "parent":
          parenId = snapshot.after.data()[key];
          break;
        case "membershipID":
          membershipID = snapshot.after.data()[key];
          break;
        case "countryID":
          countryID = snapshot.after.data()[key];
          break;
        // case "cityID":
        //     cityID = snapshot.after.data()[key];
        //     break;
        default:
          break;
      }
    });
    // get memberships
    const membs = await getMemberships();
    membs.forEach((memb) => {
      if (memb.id === membershipID) {
        membershipPrice = memb.data()["price"];
      }
    });
    // get general info
    const info = await getGeneralInfo();
    info.forEach((doc) => {
      linkBudget = doc.get(`linkBudget`);
      matrixBudget = doc.get(`matrixBudget`);
    });

    // evaluate if it's the correct memb
    if (membershipID === "mb_star") {
      // evaluate if has father
      if (parenId !== "" && parenId !== null) {
        await hasParent(parenId)
          .then(async (result) => {
            if (result !== undefined) {
              // evaluate what do with 30 USD
              if (result["membershipID"] === "mb_star") {
                functions.logger.log(
                  `father is start set money save to his business memb`
                );
              } else if (
                result["membershipID"] !== "" &&
                result["membershipID"] !== "mb_star"
              ) {
                functions.logger.log(
                  `father is ${result["membershipID"]} set money to  24/7 admin`
                );
                // set company 24/7 comision
                await update247Balance(membershipPrice * 0.3);
              } else if (result["membershipID"] === "") {
                functions.logger.log(
                  `father don't have active memb ${result["membershipID"]}`
                );
              }
              // discount from father`s links wallet
              functions.logger.log(
                `father's links wallet ${result[`walletLinks`]}`
              );
              await updateWallet(
                result[`walletLinks`],
                -linkBudget / 5,
                1
              ).then((resultmemb) => {
                functions.logger.log(
                  `discount links wallet succesfully ${resultmemb.toDate()}`
                );
                return "ok";
              });
              // add comision to father`s 24/7 wallet
              functions.logger.log(
                ` father's wallet 24/7 ${result[`wallet247`]}`
              );
              await updateWallet(
                result[`wallet247`],
                membershipPrice * 0.2,
                2
              ).then((resultmemb) => {
                functions.logger.log(
                  `24/7 added comision  succesfully ${resultmemb.toDate()}`
                );
                return "ok";
              });
            }
            return "ok";
          })
          .catch((err) => {
            functions.logger.error(`${err} ==> getting ancestor`);
            throw err;
          });
      } else {
        functions.logger.log(
          `${snapshot.after.data()["fullName"]} user not have father`
        );
      }
      // set national matrix dir comision
      await updateNalDirMatrixBalance(countryID, membershipPrice * 0.1);
      // set company 24/7 comision
      await update247Balance(membershipPrice * 0.1);
    } else {
      functions.logger.log(`user not have ${membershipID} membership`);
    }
  } catch (error) {
    functions.logger.error(
      `starMembDis error dispersion with star memb err-> ${error}`
    );
    throw error;
  }
}
// run dispersion when activate membership from business
async function bussMembDis(snapshot, context) {
  try {
    let parenId = "";
    let membershipID = "";
    let membershipPrice = 0;
    let countryID = "";
    let cityID = "";
    let ally = "";
    let percents = [0.2, 0.1, 0.05, 0.03, 0.02];
    let familyDisper = [];
    let surplus = 0;
    Object.keys(snapshot.after.data()).forEach((key) => {
      switch (key) {
        case "parent":
          parenId = snapshot.after.data()[key];
          break;
        case "membershipID":
          membershipID = snapshot.after.data()[key];
          break;
        case "countryID":
          countryID = snapshot.after.data()[key];
          break;
        case "cityID":
          cityID = snapshot.after.data()[key];
          break;
        case "alieID":
          ally = snapshot.after.data()[key];
          break;
        default:
          break;
      }
    });

    // get memberships
    const membs = await getMemberships();
    membs.forEach((memb) => {
      if (memb.id === membershipID) {
        membershipPrice = memb.data()["price"];
      }
    });
    // evaluate if has father
    if (parenId !== "" && parenId !== null) {
      let currentAnces = parenId;
      await hasParent(parenId)
        .then(async (result1) => {
          if (result1 !== undefined) {
            familyDisper = [
              ...familyDisper,
              {
                id: parenId,
                commission: membershipPrice * percents[0],
                cuMemb: result1["membershipID"],
                wallMemb: result1["walletMembership"],
                status: result1["status"],
              },
            ];
            currentAnces = result1["parent"];
            // does my father have father?
            if (currentAnces !== "" && currentAnces !== null) {
              for (let i = 1; i <= 4; i++) {
                if (currentAnces !== "" && currentAnces !== null) {
                  await hasParent(currentAnces).then((result2) => {
                    if (result2 !== undefined) {
                      familyDisper = [
                        ...familyDisper,
                        {
                          id: currentAnces,
                          commission: membershipPrice * percents[i],
                          cuMemb: result2["membershipID"],
                          wallMemb: result2["walletMembership"],
                          status: result2["status"],
                        },
                      ];
                      currentAnces = result2["parent"];
                    }
                    return "ok";
                  });
                } else {
                  functions.logger.log(
                    `there is no familiar on the level -> ${i}`
                  );
                  surplus = surplus + membershipPrice * percents[i];
                }
              }
            } else {
              functions.logger.log(`I don't have a grandfather`);
              surplus = surplus + membershipPrice * 0.2;
            }

            // ya tengo la dispersión familiar
            Object.values(familyDisper).forEach(async (value) => {
              functions.logger.log(
                `partner to dispert id -> ${value.id} || comision  USD -> ${value.commission} || membresía -> ${value.cuMemb}`
              );
              // if(value.cuMemb === 'mb_star'){
              //   surplus = surplus+value.commission
              //   functions.logger.log(`surplus -> ${surplus}`);
              // }else{
              //   functions.logger.log(`commission the associate -> ${value.id} in wallet -> ${value.wallMemb}`);
              //   await updateWallet(value.wallMemb,value.commission,3);
              // }
              // evaluate if user is active, if not active send to company
              // if (value.status === false) {
              //   surplus = surplus + value.commission;
              //   functions.logger.log(
              //     `surplus -> ${surplus} to company because user it's not active`
              //   );
              // } else if (value.status === true) {
              //   functions.logger.log(
              //     `commission the associate -> ${value.id} is active send to wallet -> ${value.wallMemb}`
              //   );
              //   await updateWallet(value.wallMemb, value.commission, 3);
              // }
              functions.logger.log(
                `commission the associate -> ${value.id} in wallet -> ${value.wallMemb}`
              );
              await updateWallet(value.wallMemb, value.commission, 3);
            });
          }
          return "ok";
        })
        .catch((err) => {
          functions.logger.error(`Error getting father ancestor -> ${err}`);
          throw err;
        });
    } else {
      functions.logger.log(
        `${snapshot.after.data()["fullName"]} user not have father`
      );
      // No tengo familiares el 40% va a la empresa
      surplus = surplus + membershipPrice * 0.4;
    }
    // com 24/7 company
    functions.logger.log(
      `commission 24/7 comp -> ${
        surplus + membershipPrice * 0.44
      } || sur -> ${surplus} || member ->${membershipPrice}`
    );
    await update247Balance(surplus + membershipPrice * 0.45);
    // com national director
    await updateNalDirBalance(countryID, membershipPrice * 0.02);
    // com zonal director
    await updateZnDirBalance(countryID, cityID, membershipPrice * 0.02);
    // com Mrk director
    await updateMkDirBalance(countryID, membershipPrice * 0.03);
    // com CEO
    await updateCEOBalance(membershipPrice * 0.03);
    // com Financial
    await updateFinanBalance(membershipPrice * 0.03);
    // com PR
    await updatePRBalance(membershipPrice * 0.02);
    // com ally
    // await updateAllyBalance(ally,membershipPrice*0.35);
  } catch (error) {
    functions.logger.error(
      `bussMembDis error dispersion with star memb err-> ${error}`
    );
    throw error;
  }
}
async function getMemberships() {
  try {
    const res = await admin.firestore().collection(`memberships`).get();
    const membs = res.docs;
    return membs;
  } catch (err) {
    functions.logger.error(`error getting memberships ${error}`);
    throw error;
  }
}
async function getAlies() {
  try {
    const res = await admin.firestore().collection(`alies`).get();
    const alies = res.docs;
    return alies;
  } catch (err) {
    functions.logger.error(`error getting memberships ${error}`);
    throw error;
  }
}
async function getGeneralInfo() {
  try {
    const res = await admin.firestore().collection(`information`).get();
    const membs = res.docs;
    return membs;
  } catch (err) {
    functions.logger.error(`error getting memberships ${error}`);
    throw error;
  }
}
async function hasParent(fatherID) {
  try {
    const res = await admin.firestore().doc(`associates/${fatherID}`).get();
    if (res.data()["status"] === true) {
      return (objectData = res.data());
    } else {
      return (objectData = undefined);
    }
  } catch (err) {
    functions.logger.error(`error getting father ${fatherID}`);
    throw err;
  }
}
async function updateWallet(walletID, increment, type) {
  try {
    let linkBudget;
    let matrixBudget;
    // get general info
    const info = await getGeneralInfo();
    info.forEach((doc) => {
      linkBudget = doc.get(`linkBudget`);
      matrixBudget = doc.get(`matrixBudget`);
    });
    const res = await admin.firestore().doc(`wallets/${walletID}`).get();
    console.log(
      `ACTUALIZANDO BILLETERA -> wallet ->${walletID} || current balance -> ${
        res.data()[`current_balance`]
      }`
    );
    let newBalance = res.data()[`current_balance`] + increment;
    switch (type) {
      case 1:
        if (newBalance === 0 || newBalance <= 0) {
          newBalance = linkBudget;
          functions.logger.log(`charge budget links wallet ${walletID}`);
        }
        break;
      case 7:
        if (newBalance === matrixBudget) {
          functions.logger.log(
            `complete balance to pay Matrix budget ${walletID}`
          );
        } else if (newBalance > matrixBudget) {
          newBalance = matrixBudget;
          // ************************************************************************************
          // ---------------------- cambiar cuando se migre a producción ------------------------
          // ************************************************************************************
          // add money to company's wallet
          await updateWallet(companyWalletConst, increment, 10);
        } else if (newBalance < matrixBudget) {
          functions.logger.log(
            `add to matrix wallet ${walletID} comision-> ${increment} || new balance->${newBalance}`
          );
        }
        break;
      default:
        break;
    }
    const res2 = await admin
      .firestore()
      .doc(`wallets/${walletID}`)
      .update({ current_balance: Number(newBalance) });
    console.log(
      `///////// wallet ->${walletID} || current -> ${
        res.data()[`current_balance`]
      }|| new balance -> ${newBalance}`
    );
    return res2.writeTime;
  } catch (error) {
    functions.logger.error(`error updating wallet function ${error}`);
    throw error;
  }
}
async function updateNalDirMatrixBalance(countryID, valueCom) {
  try {
    const res = await admin.firestore().doc(`countries/${countryID}`).get();
    const dirID = res.data()[`natDirectorUID`];
    if (dirID !== "") {
      const res2 = await admin.firestore().doc(`associates/${dirID}`).get();
      const dirNalMatWallet = res2.data()[`walletMatDir`];
      const res3 = await updateWallet(dirNalMatWallet, valueCom, 7);
      return res3;
    } else {
      await update247Balance(valueCom);
    }
  } catch (error) {
    if (res !== undefined) {
      functions.logger.error(
        `error updating national mrk dir (get country throw)  ${
          res.data()[`name`]
        } - error ${error}`
      );
    }
    if (res2 !== undefined) {
      functions.logger.error(
        `error updating national mrk dir (get associate throw) ${
          res2.data()[`fullName`]
        } - error ${error}`
      );
    }
    if (res3 !== undefined) {
      functions.logger.error(
        `error updating wallet mrk dir ${res3} - error ${error}`
      );
    }
    throw error;
  }
}
async function updateNalDirBalance(countryID, valueCom) {
  try {
    const res = await admin.firestore().doc(`countries/${countryID}`).get();
    const dirID = res.data()[`natDirectorUID`];
    if (dirID !== "") {
      const res2 = await admin.firestore().doc(`associates/${dirID}`).get();
      const dirNalWallet = res2.data()[`walletNalDir`];
      const res3 = await updateWallet(dirNalWallet, valueCom, 10);
      return res3;
    } else {
      await update247Balance(valueCom);
    }
  } catch (error) {
    if (res !== undefined) {
      functions.logger.error(
        `error updating national dir (get country throw)  ${
          res.data()[`name`]
        } - error ${error}`
      );
    }
    if (res2 !== undefined) {
      functions.logger.error(
        `error updating national dir (get associate throw) ${
          res2.data()[`fullName`]
        } - error ${error}`
      );
    }
    if (res3 !== undefined) {
      functions.logger.error(
        `error updating wallet national dir ${res3} - error ${error}`
      );
    }
    throw error;
  }
}
async function updateMkDirBalance(countryID, valueCom) {
  try {
    // const res = await admin.firestore().doc(`countries/${countryID}`).get();
    const res = await admin
      .firestore()
      .doc(`/ranks_dir/cFi4HSMwobtUChFso00n`)
      .get();
    functions.logger.error(`RANKSDIR MKDIR -> ${res}`);
    const dirID = res.data()[`mrkDirectorUID`];
    if (dirID) {
      const res2 = await admin.firestore().doc(`associates/${dirID}`).get();
      const dirMkWallet = res2.data()[`walletMkDir`];
      const res3 = await updateWallet(dirMkWallet, valueCom, 8);
      return res3;
    } else {
      await update247Balance(valueCom);
    }
  } catch (error) {
    if (res !== undefined) {
      functions.logger.error(
        `error updating marketing dir (get country throw)  ${
          res.data()[`name`]
        } - error ${error}`
      );
    }
    if (res2 !== undefined) {
      functions.logger.error(
        `error updating marketing dir (get associate throw) ${
          res2.data()[`fullName`]
        } - error ${error}`
      );
    }
    if (res3 !== undefined) {
      functions.logger.error(
        `error updating wallet marketing dir ${res3} - error ${error}`
      );
    }
    throw error;
  }
}
async function updateZnDirBalance(countryID, zoneID, valueCom) {
  try {
    const res = await admin
      .firestore()
      .doc(`countries/${countryID}/cities/${zoneID}`)
      .get();
    const dirID = res.data()[`directorUID`];
    if (dirID !== "") {
      const res2 = await admin.firestore().doc(`associates/${dirID}`).get();
      const dirZnWallet = res2.data()[`walletZnDir`];
      const res3 = await updateWallet(dirZnWallet, valueCom, 9);
      return res3;
    } else {
      await update247Balance(valueCom);
    }
  } catch (error) {
    if (res !== undefined) {
      functions.logger.error(
        `error updating national zone (get country throw)  ${
          res.data()[`name`]
        } - error ${error}`
      );
    }
    if (res2 !== undefined) {
      functions.logger.error(
        `error updating national zone (get associate throw) ${
          res2.data()[`fullName`]
        } - error ${error}`
      );
    }
    if (res3 !== undefined) {
      functions.logger.error(
        `error updating wallet national zone ${res3} - error ${error}`
      );
    }
    throw error;
  }
}
async function update247Balance(valueCom) {
  try {
    // ************************************************************************************
    // ---------------------- cambiar cuando se migre a producción -------------------------
    // ************************************************************************************
    // functions.logger.log(`wallet company 24/7 ->companyWalletConst|| comision-> ${valueCom}`);
    const res = await updateWallet(companyWalletConst, valueCom, 10);
    return res;
  } catch (error) {
    functions.logger.error(`error updating 24/7 company wallet ${error}`);
    throw error;
  }
}
async function updateCEOBalance(valueCom) {
  try {
    // ************************************************************************************
    // ---------------------- cambiar cuando se migre a producción -------------------------
    // ************************************************************************************
    const res = await updateWallet(BalanceCeoConst, valueCom, 12);
    return res;
  } catch (error) {
    functions.logger.error(`error updating 24/7 company wallet ${error}`);
    throw error;
  }
}
async function updateFinanBalance(valueCom) {
  try {
    // ************************************************************************************
    // ---------------------- cambiar cuando se migre a producción -------------------------
    // ************************************************************************************
    const res = await updateWallet(finanBalanceConst, valueCom, 13);
    return res;
  } catch (error) {
    functions.logger.error(`error updating 24/7 company wallet ${error}`);
    throw error;
  }
}
async function updatePRBalance(valueCom) {
  try {
    const res = await updateWallet(PRBalanceConst, valueCom, 14);
    return res;
  } catch (error) {
    functions.logger.error(`error updating 24/7 company wallet ${error}`);
    throw error;
  }
}
async function updateAllyBalance(allyID, valueCom) {
  try {
    const res = await admin.firestore().doc(`alies/${allyID}`).get();
    const newBill = res.data()[`bill`] + valueCom;
    const res2 = await admin
      .firestore()
      .doc(`alies/${allyID}`)
      .update({ bill: newBill })
      .then((resultu) => {
        console.log(
          `///////// Alie wallet ->${res.data()[`name`]} || current -> ${
            res.data()[`bill`]
          }|| new balance -> ${newBill}`
        );
        return resultu.writeTime;
      })
      .catch((err) => {
        return err;
      });
  } catch (error) {
    if (res !== undefined) {
      functions.logger.error(
        `error updating  ally  (get ally throw)  ${
          res.data()[`name`]
        } - error ${error}`
      );
    }
    if (res2 !== undefined) {
      functions.logger.error(
        `error updating  ally  (update ally throw) ${
          res2.data()[`fullName`]
        } - error ${error}`
      );
    }
    throw error;
  }
}
function printFamily(families) {
  //  merge all families in one object
  let integrado = families.reduce((a, b) => {
    return a.concat(b);
  });
  // create an array with all users and they total comission
  let seen = new Map();
  let groupCommissions = integrado.filter((position) => {
    const duplicate = seen.has(position["id"]);
    if (duplicate) {
      let obj = seen.get(position["id"]);
      Object.keys(obj).forEach((key) => {
        if (key === "commission") {
          obj[key] = obj[key] + position["commission"];
        }
      });
    } else {
      seen.set(position["id"], position);
    }
    return !duplicate;
  });
  // array with all users and they commission
  let assoCom = groupCommissions.filter((pos) => pos["isAsso"] === 0);
  // array with all company commissions
  let companyCom = groupCommissions.filter((pos) => pos["isAsso"] === 1);
  // array with all nationals dir commissions
  let nationalsCom = groupCommissions.filter((pos) => pos["isAsso"] === 3);
  // array with all zones dir commissions
  let zonesdirCom = groupCommissions.filter((pos) => pos["isAsso"] === 4);
  // array with all marketing commissions
  let marketingCom = groupCommissions.filter((pos) => pos["isAsso"] === 5);
  // array with all financial commissions
  let finanCom = groupCommissions.filter((pos) => pos["isAsso"] === 6);
  // array with all ceo commissions
  let ceoCom = groupCommissions.filter((pos) => pos["isAsso"] === 7);
  // array with all ceo commissions
  let prCom = groupCommissions.filter((pos) => pos["isAsso"] === 8);
  console.log(`---------------------------`);
  console.log(nationalsCom);
  console.log(`---------------------------`);
  console.log(zonesdirCom);
  console.log(`---------------------------`);
  console.log(marketingCom);
  console.log(`---------------------------`);
  console.log(finanCom);
  console.log(`---------------------------`);
  console.log(ceoCom);
  console.log(`---------------------------`);
  console.log(prCom);

  // update every associate or acumulate extra commission for company
  let surplus = 0;
  for (let i = 0; i < assoCom.length; i++) {
    if (assoCom[i]["cuMemb"] === "mb_star") {
      surplus = surplus + assoCom[i]["commission"];
    } else {
      // update every user
      console.log(`asso comission ->${assoCom[i]["commission"]}`);
      updateWallet(assoCom[i]["wallResid"], assoCom[i]["commission"], 3);
    }
  }
  console.log(
    `final company comission ->${surplus + companyCom[0]["commission"]}`
  );
  for (let i = 0; i < companyCom.length; i++) {
    update247Balance(surplus + companyCom[i]["commission"]);
  }
  // com national director
  for (let i = 0; i < nationalsCom.length; i++) {
    updateNalDirBalance(
      nationalsCom[i]["country"],
      nationalsCom[i]["commission"]
    );
  }
  // com zonal director
  for (let i = 0; i < zonesdirCom.length; i++) {
    updateZnDirBalance(
      zonesdirCom[i]["country"],
      zonesdirCom[i]["id"],
      zonesdirCom[i]["commission"]
    );
  }
  // com Mrk director
  for (let i = 0; i < marketingCom.length; i++) {
    updateMkDirBalance(
      marketingCom[i]["country"],
      marketingCom[i]["commission"]
    );
  }
  // com CEO
  for (let i = 0; i < ceoCom.length; i++) {
    updateCEOBalance(ceoCom[i]["commission"]);
  }
  // com Financial
  for (let i = 0; i < finanCom.length; i++) {
    updateFinanBalance(finanCom[i]["commission"]);
  }
  // com PR
  for (let i = 0; i < prCom.length; i++) {
    updatePRBalance(prCom[i]["commission"]);
  }
  // com ally already set when iterate our alies out of this function
}
async function getFamilyForAliesDis(snapshot, aliesList) {
  try {
    let parenId = "";
    let membershipID = "";
    let allyRate = 0;
    let allyName = "";
    let countryID = "";
    let cityID = "";
    let ally = "";
    let percents = [0.2, 0.1, 0.05, 0.03, 0.02];
    let familyDisper = [];
    let surplus = 0;
    Object.keys(snapshot.data()).forEach((key) => {
      switch (key) {
        case "parent":
          parenId = snapshot.data()[key];
          break;
        case "membershipID":
          membershipID = snapshot.data()[key];
          break;
        case "countryID":
          countryID = snapshot.data()[key];
          break;
        case "cityID":
          cityID = snapshot.data()[key];
          break;
        case "alieID":
          ally = snapshot.data()[key];
          break;
        default:
          break;
      }
    });
    // prepare other commission
    for (let i = 0; i < aliesList.length; i++) {
      if (aliesList[i]["id"] === ally) {
        allyRate = aliesList[i]["rate"];
        // prepare 24/7 commission
        familyDisper = [
          ...familyDisper,
          {
            name: "24/7",
            id: "24-7",
            commission: aliesList[i]["rate"] * 0.09,
            isAsso: 1,
          },
        ];
        // prepare national commission
        familyDisper = [
          ...familyDisper,
          {
            name: "national dir of:",
            id: `nat-${countryID}`,
            commission: aliesList[i]["rate"] * 0.02,
            isAsso: 3,
            country: countryID,
          },
        ];
        // prepare zone commission
        familyDisper = [
          ...familyDisper,
          {
            name: "zone dir of:",
            id: cityID,
            commission: aliesList[i]["rate"] * 0.02,
            isAsso: 4,
            country: countryID,
          },
        ];
        // prepare mrk commission
        familyDisper = [
          ...familyDisper,
          {
            name: "marketing:",
            id: `mrk-${countryID}`,
            commission: aliesList[i]["rate"] * 0.03,
            isAsso: 5,
            country: countryID,
          },
        ];
        // prepare financial commission
        familyDisper = [
          ...familyDisper,
          {
            name: "financial:",
            id: "fin",
            commission: aliesList[i]["rate"] * 0.03,
            isAsso: 6,
          },
        ];
        // prepare ceo commission
        familyDisper = [
          ...familyDisper,
          {
            name: "admin:",
            id: "ceo",
            commission: aliesList[i]["rate"] * 0.03,
            isAsso: 7,
          },
        ];
        // prepare PR commission
        familyDisper = [
          ...familyDisper,
          {
            name: "public relationships:",
            id: "pr",
            commission: aliesList[i]["rate"] * 0.03,
            isAsso: 8,
          },
        ];
      }
    }
    // evaluate if has father
    if (parenId !== "" && parenId !== null) {
      let currentAnces = parenId;
      await hasParent(parenId)
        .then(async (result1) => {
          if (result1 !== undefined) {
            familyDisper = [
              ...familyDisper,
              {
                name: result1["fullName"],
                id: parenId,
                commission: allyRate * percents[0],
                cuMemb: result1["membershipID"],
                wallResid: result1["walletResidual"],
                isAsso: 0,
              },
            ];
            currentAnces = result1["parent"];
            // does my father have father?
            if (currentAnces !== "" && currentAnces !== null) {
              for (let i = 1; i <= 4; i++) {
                if (currentAnces !== "" && currentAnces !== null) {
                  await hasParent(currentAnces).then((result2) => {
                    if (result2 !== undefined) {
                      familyDisper = [
                        ...familyDisper,
                        {
                          name: result2["fullName"],
                          id: currentAnces,
                          commission: allyRate * percents[i],
                          cuMemb: result2["membershipID"],
                          wallResid: result2["walletResidual"],
                          isAsso: 0,
                        },
                      ];
                      currentAnces = result2["parent"];
                    }
                    return "ok";
                  });
                } else {
                  // console.log(`------------- ${snapshot.data()['fullName']} user not have ancestor in level ->${i} || percent -> ${percents[i]}`);
                  familyDisper = [
                    ...familyDisper,
                    {
                      name: "no commission",
                      id: "24-7",
                      commission: allyRate * percents[i],
                      isAsso: 1,
                    },
                  ];
                }
              }
            } else {
              // console.log(`------------- ${snapshot.data()['fullName']} user not have grandfather`);
              familyDisper = [
                ...familyDisper,
                {
                  name: "no commission",
                  id: "24-7",
                  commission: allyRate * 0.2,
                  isAsso: 1,
                },
              ];
            }
          }
          return "ok";
        })
        .catch((err) => {
          functions.logger.error(`Error getting father ancestor -> ${err}`);
        });
      return familyDisper;
    } else {
      // console.log(`------------- ${snapshot.data()['fullName']} user not have father`);
      familyDisper = [
        ...familyDisper,
        {
          name: "no commission",
          id: "24-7",
          commission: allyRate * 0.4,
          isAsso: 1,
        },
      ];
      return familyDisper;
    }
  } catch (error) {
    functions.logger.error(`error dispersion with school err-> ${error}`);
    throw err;
  }
}
async function getReferralsCount(idReferralList) {
  let counters = [];
  let index = 0;
  let referrerID = "";
  someActive = false;
  const ranks = await admin
    .firestore()
    .collection(`ranks`)
    .orderBy(`pos`)
    .get()
    .then((data) => {
      data.docs.forEach((doc) => {
        counters.push({
          count: 0,
          curr_rk: doc.id,
          memb_req: doc.data()["memb_req"],
          name: doc.data()["name"],
          min: doc.data()["min_referrals"],
          com_dev: doc.data()["com_dev"],
        });
        index++;
      });
      return counters;
    })
    .catch((err) => {
      functions.logger.error(
        `Error getting ranks on referral_list update, ${err}`
      );
      throw err;
    });

  const data = await admin
    .firestore()
    .collection(`/referrals_list/${idReferralList}/my_referr/`)
    .get()
    .then((data) => {
      data.docs.forEach((doc) => {
        functions.logger.log(`evaluate referral list ${idReferralList}`);
        if (doc.get("status")) {
          functions.logger.log(
            `${doc.get("name")} status is -> ${doc.get(
              "status"
            )} with rank -> ${doc.get("rank_ID")}`
          );
          // get father to update
          referrerID = doc.get("id_referral");
          someActive = true;
          // set level child
          switch (doc.get("rank_ID")) {
            case "rk_asso":
              counters[0]["count"] = counters[0]["count"] + 1;
              break;
            case "rk_con":
              counters[0]["count"] = counters[0]["count"] + 1;
              counters[1]["count"] = counters[1]["count"] + 1;
              break;
            case "rk_dir":
              // add to sum ranks before could help to be promote
              counters[0]["count"] = counters[0]["count"] + 1;
              counters[1]["count"] = counters[1]["count"] + 1;
              counters[2]["count"] = counters[2]["count"] + 1;
              counters[3]["count"] = counters[3]["count"] + 1;
              counters[4]["count"] = counters[4]["count"] + 1;
              counters[5]["count"] = counters[5]["count"] + 1;
              break;
            case "rk_ger":
              counters[0]["count"] = counters[0]["count"] + 1;
              counters[1]["count"] = counters[1]["count"] + 1;
              counters[2]["count"] = counters[2]["count"] + 1;
              counters[3]["count"] = counters[3]["count"] + 1;
              counters[4]["count"] = counters[4]["count"] + 1;
              break;
            case "rk_sub":
              counters[0]["count"] = counters[0]["count"] + 1;
              counters[1]["count"] = counters[1]["count"] + 1;
              counters[2]["count"] = counters[2]["count"] + 1;
              counters[3]["count"] = counters[3]["count"] + 1;
              break;
            case "rk_sup":
              counters[0]["count"] = counters[0]["count"] + 1;
              counters[1]["count"] = counters[1]["count"] + 1;
              counters[2]["count"] = counters[2]["count"] + 1;
              break;
          }
        } else {
          functions.logger.log(
            `user ${doc.get("name")} has ${doc.get(
              "status"
            )} status on ${idReferralList} list for evaluate promotion so don't count to father's promotion`
          );
        }
      });
      if (someActive) {
        // evaluate promotion
        evaluatePromotion(referrerID, counters);
      }
      return true;
    })
    .catch((err) => {
      functions.logger.error(`error getting referrals list err:${err}`);
      throw err;
    });
}
